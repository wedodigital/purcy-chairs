# Global TS definitions

## Packages

Global packages definitions that don't need to be shipped with lerna packages itself.
Eg. jsx-control-statement since you should polyfill TS for them in your own package.

- used by TSC checks

## System

TS Definitions for storybook, logger and other `around` core v2 tools

- not used by TSC checks
