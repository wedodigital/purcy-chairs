const path = require('path')

module.exports = {
  siteMetadata: {
    title: 'purcy-chairs',
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'The Purcy Chair',
        short_name: 'The Purcy Chair',
        start_url: '/',
        display: 'minimal-ui',
        icon: 'src/assets/favicon/favicon.png', // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-alias-imports`,
      options: {
        alias: {
          '@assets': path.resolve(__dirname, 'src/assets'),
          '@images': path.resolve(__dirname, 'src/images'),
          '@components': path.resolve(__dirname, 'src/components'),
          '@context': path.resolve(__dirname, 'src/context'),
          '@helpers': path.resolve(__dirname, 'src/helpers'),
          '@pages': path.resolve(__dirname, 'src/pages'),
          '@sections': path.resolve(__dirname, 'src/sections'),
          '@styles': path.resolve(__dirname, 'src/styles'),
          '@themes': path.resolve(__dirname, 'src/themes'),
          '@templates': path.resolve(__dirname, 'src/templates'),
          '@utils': path.resolve(__dirname, 'src/utils'),
        },
      },
    },
    `gatsby-plugin-tsconfig-paths`,
    {
      resolve: `gatsby-plugin-facebook-pixel`,
      options: {
        pixelId: '290592402589950',
      },
    },
    {
      resolve: `gatsby-source-wordpress`,
      options: {
        /*
         * The full URL of the WordPress site's GraphQL API.
         * Example : 'https://www.example-site.com/graphql'
         */
        url: `https://cms.purcychair.com/graphql`,
        type: {
          MediaItem: {
            localFile: {
              requestConcurrency: 50
            }
          }
        },
        schema: {
          timeout: 120000,
          perPage: 5,
          requestConcurrency: 2,
          previewRequestConcurrency: 2,
        },
      },
    },
    'gatsby-plugin-styled-components',
    'gatsby-plugin-sharp',
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /assets/, // See below to configure properly
        },
      },
    },
    'gatsby-transformer-sharp',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: './src/images/',
      },
      __key: 'images',
    },
    {
      resolve: 'gatsby-plugin-load-script',
      options: {
        src: 'https://x.klarnacdn.net/kp/lib/v1/api.js',
      },
    },
    {
    resolve: 'gatsby-plugin-google-tagmanager',
    options: {
      id: 'GTM-PQTXV6L',
    },
  },
    
  ],
};
