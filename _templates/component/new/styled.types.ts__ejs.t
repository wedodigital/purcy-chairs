---
to: src/components/<%= name %>/styles/<%= name %>.style.types.ts
---
import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface Styled<%= name %>Props {
  theme: Theme;
}
