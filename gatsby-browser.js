import React from 'react'
import PageLayout from './src/templates/PageLayout'

export const wrapPageElement = ({ element, props }) => <PageLayout {...props}>{element}</PageLayout>
