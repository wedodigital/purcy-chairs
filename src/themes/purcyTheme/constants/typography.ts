const typography = {
  paragraph: {
    1: {
      fontSizeMobile: '12px',
      lineHeightMobile: '16px',
      fontSize: '12px',
      lineHeight: '16px',
    },
    2: {
      fontSizeMobile: '14px',
      lineHeightMobile: '18px',
      fontSize: '14px',
      lineHeight: '18px',
    },
    3: {
      fontSizeMobile: '16px',
      lineHeightMobile: '24px',
      fontSize: '16px',
      lineHeight: '24px',
    },
  },
  heading: {
    1: {
      fontSizeMobile: '18px',
      lineHeightMobile: '26px',
      fontSize: '20px',
      lineHeight: '28px',
    },
    2: {
      fontSizeMobile: '24px',
      lineHeightMobile: '32px',
      fontSize: '32px',
      lineHeight: '44px',
    },
    3: {
      fontSizeMobile: '24px',
      lineHeightMobile: '38px',
      fontSize: '36px',
      lineHeight: '58px',
    },
    4: {
      fontSizeMobile: '32px',
      lineHeightMobile: '40px',
      fontSize: '48px',
      lineHeight: '56px',
    },
    5: {
      fontSizeMobile: '36px',
      lineHeightMobile: '44px',
      fontSize: '74px',
      lineHeight: '88px',
    },
    6: {
      fontSizeMobile: '40px',
      lineHeightMobile: '48px',
      fontSize: '80px',
      lineHeight: '88px',
    },
    7: {
      fontSizeMobile: '48px',
      lineHeightMobile: '70px',
      fontSize: '152px',
      lineHeight: '150px',
    },
  },
  fontWeight: {
    1: 100,
    2: 400,
    3: 600,
  },
  fontFamily: 'aktiv-grotesk, sans-serif',
}

export default typography
