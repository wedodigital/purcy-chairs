export type colorsTypeKeys =
  | 'primary'
  | 'secondary'
  | 'tertiary'
  | 'quartenary'

export type Colours = {
  [key in colorsTypeKeys]: string;
};
