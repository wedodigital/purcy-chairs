import { Colours } from './colours.types'

const colours: Colours = {
  primary: '#2d2c2d',
  secondary: '#e0dee0',
  tertiary: '#ffffff',
  quartenary: '#a7a8ab',
}

export default colours
