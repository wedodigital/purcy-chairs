import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledCollapsibleContainerProps {
  theme: Theme
  isOpen: boolean
}
