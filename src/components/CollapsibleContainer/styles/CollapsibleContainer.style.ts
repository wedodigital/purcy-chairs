import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledCollapsibleContainerProps } from './CollapsibleContainer.style.types'

export const CollapsibleContainer = styled.div((props: Pick<StyledCollapsibleContainerProps, 'theme'>): FlattenSimpleInterpolation => css`
  border-bottom: 1px solid ${props.theme.colours.primary};
  padding: ${props.theme.spacing.fixed[2]}px;

  &:first-child {
    border-top: 1px solid ${props.theme.colours.primary};
  }
`)

export const CollapsibleContainerTitle = styled.div((): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
`)

export const CollapsibleContainerContent = styled.div((props: Pick<StyledCollapsibleContainerProps, 'theme'>): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[4]}px 0;

  ${props.theme.mixins.respondTo.md(css`
    padding: ${props.theme.spacing.fixed[4]}px;
  `)}
`)
