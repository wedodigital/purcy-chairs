import React, { PureComponent, ReactNode } from 'react'
import AnimateHeight from 'react-animate-height'

import Heading from '@components/Heading'
import Toggle from '@components/Toggle'

import * as Styled from './styles/CollapsibleContainer.style'

import { CollapsibleContainerProps, CollapsibleContainerState } from './CollapsibleContainer.types'

export default class CollapsibleContainer extends PureComponent<CollapsibleContainerProps, CollapsibleContainerState> {
  public state: CollapsibleContainerState = {
    isOpen: false,
  }

  toggleCollapsibleContainer(): void {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render(): ReactNode {
    return (
      <Styled.CollapsibleContainer>
        <Styled.CollapsibleContainerTitle>
          <Heading size={2} weight={2} noMargin text={this.props.title} />
          <Toggle onClick={() => this.toggleCollapsibleContainer()} isOpen={this.state.isOpen} />
        </Styled.CollapsibleContainerTitle>
        <AnimateHeight
          duration={ 500 }
          height={this.state.isOpen ? 'auto' : 0}
        >
          <Styled.CollapsibleContainerContent>
            {this.props.children}
          </Styled.CollapsibleContainerContent>
        </AnimateHeight>
      </Styled.CollapsibleContainer>
    )
  }
}
