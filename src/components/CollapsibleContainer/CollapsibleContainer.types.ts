import { ReactNode } from 'react'

export interface CollapsibleContainerProps {
  title: string
  children: ReactNode
}

export interface CollapsibleContainerState {
  isOpen: boolean
}
