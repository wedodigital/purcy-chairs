import React, { FC, ReactElement } from "react"
import { Helmet } from "react-helmet"
import { HeadTagsProps } from "./HeadTags.types";

import Script from 'next/script'
 const HeadTags: FC<HeadTagsProps> = ({ seo }: HeadTagsProps): ReactElement => {
   return (
     <Helmet>
       <If condition={seo.title}>
         <title>{seo.title}</title>
       </If>
       <If condition={seo.metaDesc}>
         <meta name="description" content={seo.metaDesc} />
       </If>
       <If condition={seo.canonical}>
         <link rel="canonical" href={seo.canonical} />
       </If>
       <If condition={seo.opengraphType}>
         <meta property="og:type" content={seo.opengraphType} />
       </If>
      
       <If condition={seo.opengraphImage}>
         <meta name="twitter:card" content={"summary"} />
       </If>
       <If condition={seo.schema}>
         <script type="application/ld+json">
           {JSON.stringify(seo.schema?.raw)}
         </script>
       </If>
       <meta
         name="facebook-domain-verification"
         content="gdb856loed4nix31xnvwnk6oa9mk9r"
       />
     <meta name="google-site-verification" content="zTvh1AkYhRMt1lZQSIvzQTCS-58IK15MYmtCGXxUqK8" />
     <script
       strategy="lazyOnload"
        src={`https://www.googletagmanager.com/gtag/js?id=UA-190508461-1`}
      />
      <script strategy="lazyOnload">
        {`
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-190508461-1', {
        page_path: window.location.pathname,
       });
    `}
      </script>
     
     </Helmet>
   );
 };
 export default HeadTags;