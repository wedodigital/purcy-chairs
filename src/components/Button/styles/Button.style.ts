import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledButtonProps } from './Button.style.types'

export const Button = styled.div((props: StyledButtonProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[1]}px ${props.theme.spacing.fixed[2]}px;
  background: ${props.theme.colours.primary};
  color: ${props.theme.colours.tertiary};
  display: inline-block;
  text-decoration: none;
  font-weight: 700;
  border-radius: 40px;
  border: 2px solid ${props.theme.colours.primary};
  cursor: pointer;
  font-size: ${props.theme.typography.paragraph[3].fontSize};
  line-height: ${props.theme.typography.paragraph[3].lineHeight};
  font-family: ${props.theme.typography.fontFamily};
  text-align: center;
  transition: 0.4s all ease;

  ${props.inverse && css`
    background: ${props.theme.colours.tertiary};
    color: ${props.theme.colours.primary};
    border-color: ${props.theme.colours.tertiary};
  `}

  ${props.appearance === 'secondary' && css`
    background: none;
    color: ${props.theme.colours.primary};

    &:hover {
      background: ${props.theme.colours.primary};
      color: ${props.theme.colours.tertiary};
    }

    ${props.inverse && css`
      color: ${props.theme.colours.tertiary};
      border-color: ${props.theme.colours.tertiary};

      &:hover {
        background: ${props.theme.colours.tertiary};
        color: ${props.theme.colours.primary};
      }
    `}
  `}

  ${props.size === 1 && css`
    font-size: ${props.theme.typography.paragraph[2].fontSize};
    line-height: ${props.theme.typography.paragraph[2].lineHeight};
    padding: ${props.theme.spacing.fixed[1] / 2}px ${props.theme.spacing.fixed[2]}px;
  `}

  ${props.size === 3 && css`
    line-height: ${props.theme.typography.heading[1].lineHeight};
    padding: ${props.theme.spacing.fixed[1]}px ${props.theme.spacing.fixed[4]}px;

    ${props.theme.mixins.respondTo.md(css`
      padding: ${props.theme.spacing.fixed[1]}px ${props.theme.spacing.fixed[8]}px;
    `)}
  `}
`)
