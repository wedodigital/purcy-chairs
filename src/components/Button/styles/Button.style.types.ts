import { Theme } from '@themes/purcyTheme/purcyTheme.types'
import { ButtonProps } from '../Button.types'

export interface StyledButtonProps {
  theme: Theme
  appearance: ButtonProps['appearance']
  inverse: boolean
  size: ButtonProps['size']
}
