import { MouseEvent } from 'react'

export type ButtonEvent =
  | MouseEvent<HTMLButtonElement, MouseEvent>
  | MouseEvent<HTMLAnchorElement, MouseEvent>

export interface ButtonProps {
  to?: string
  onInteraction?: (event: ButtonEvent) => void
  href?: string
  appearance?: 'primary' | 'secondary'
  text?: string
  inverse?: boolean
  size?: 1 | 2 | 3
  type?: 'submit' | 'button' | 'reset'
}
