export interface HeaderProps {
  appearance?: 'primary' | 'secondary' | 'tertiary'
  zIndex?: number
  fixed?: boolean
  formEndpoint?: string
  showNav?: boolean
}
