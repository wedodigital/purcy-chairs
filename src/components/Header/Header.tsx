import React, { FC, ReactElement, useContext, useState } from 'react'
import { CSSTransition } from 'react-transition-group'
import { useStaticQuery, graphql, Link } from 'gatsby'

import ShoppingBag from '@assets/shopping-bag.svg'
import Menu from '@assets/menu.svg'

import Button from '@components/Button'
import Container from '@components/Container'
import Logo from '@components/Logo'
import Modal from '@components/Modal'
import EmailSignup from '@components/EmailSignup'
import Navigation from '@components/Navigation'
import MobileNav from '@components/MobileNav'
import Overlay from '@components/Overlay'
import MatchMedia from '@utils/MatchMedia'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

import AnnouncementBar from './components/AnnouncementBar'

import { HeaderProps } from './Header.types'

import * as Styled from './styles/Header.styles'

const Header: FC<HeaderProps> = ({ appearance = 'primary', showNav = true }: HeaderProps): ReactElement => {
  const context = useContext(PageContext) as PageContextType
  const [showMobileNav, setShowMobileNav] = useState(false)

  const headerContent = useStaticQuery(graphql`
    query HeaderContent {
      wpPage(databaseId: {eq: 2}) {
        homepage {
          announcementBar
        }
      }
      allProducts: allWpProduct(sort: {fields: databaseId, order: ASC}) {
        nodes {
          slug
          name
          databaseId
          ... on WpSimpleProduct {
            name
            galleryImages {
              nodes {
                sourceUrl
              }
            }
          }
          ... on WpVariableProduct {
            name
            galleryImages {
              nodes {
                sourceUrl
              }
            }
            variations {
              nodes {
                image {
                  sourceUrl
                }
              }
            }
          }
        }
      }
      purcyChair: allWpProduct(filter: {databaseId: {eq: 11}}) {
        nodes {
          name
          databaseId
          galleryImages {
            nodes {
              sourceUrl
            }
          }
          ... on WpVariableProduct {
            variations {
              nodes {
                databaseId
              }
            }
            featuredImage {
              node {
                sourceUrl
              }
            }
          }
        }
      }
    }
  `)

  const navigation = [
    {
      title: 'Our Story',
      url: '/our-story',
    },
    {
      title: 'Shop',
      url: '/shop',
      children: headerContent.allProducts.nodes.map((product) => product)
    },
    {
      title: 'Support',
      url: '/support',
    },
  ]

  return (
    <>
      <AnnouncementBar text={context.journeyType === 'capture' ? 'Get 20% off Purcy Chair' : headerContent.wpPage.homepage.announcementBar} />
      <Styled.Header appearance={appearance}>
        <Container width='wide'>
          <MatchMedia breakpoint='sm'>
            <If condition={showNav}>
              <Styled.IconButton as={Link} to='/basket'>
                <ShoppingBag />
              </Styled.IconButton>
            </If>
            <Styled.LogoBlock>
              <Logo />
            </Styled.LogoBlock>
            <If condition={showNav}>
              <Styled.IconButton as='button' onClick={() => setShowMobileNav(true)} to='/basket'>
                <Menu />
              </Styled.IconButton>
            </If>
          </MatchMedia>
          <MatchMedia breakpoint='md' andAbove>
            <If condition={showNav}>
              <Styled.ContentBlock>
                <Navigation appearance={appearance} navigation={navigation} />
              </Styled.ContentBlock>
            </If>
            <Styled.LogoBlock>
              <Logo inverse={appearance === 'secondary'} />
            </Styled.LogoBlock>
            <If condition={showNav}>
              <Styled.ContentBlock>
                <If condition={context.journeyType !== 'capture'}>
                  <Button to='/basket' text='View Basket' appearance='secondary' inverse={appearance === 'secondary'} />
                </If>
                <Button onInteraction={() => context.toggleModal()} text={context.journeyType === 'capture' ? 'Sign-up for 20% Off' : 'Buy Now With PayL8r'} inverse={appearance === 'secondary'} />
              </Styled.ContentBlock>
            </If>
          </MatchMedia>
        </Container>
      </Styled.Header>
        <CSSTransition
          in={showMobileNav}
          timeout={500}
          classNames='modal'
          unmountOnExit
        >
          <Overlay onClick={() => setShowMobileNav(false)} />
        </CSSTransition>
        <CSSTransition
          in={showMobileNav}
          timeout={500}
          classNames='mobile-nav'
          unmountOnExit
        >
          <MobileNav
            menu={navigation}
            toggleMobileNav={setShowMobileNav}
            // formEndpoint={formEndpoint}
          />
        </CSSTransition>
      <CSSTransition
        in={context.showModal}
        timeout={500}
        classNames='modal'
        unmountOnExit
      >
        <Modal>
          <Choose>
            <When condition={context.journeyType === 'capture'}>
              <EmailSignup columns={1} />
            </When>
            <Otherwise>
              <iframe src='https://2purcychair.payl8r.com/checkout' frameBorder='0' width='100%' height='600'></iframe>
            </Otherwise>
          </Choose>
        </Modal>
      </CSSTransition>
    </>
  )
}

export default Header
