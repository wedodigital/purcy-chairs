import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledAnnouncementBarProps {
  theme: Theme
}
