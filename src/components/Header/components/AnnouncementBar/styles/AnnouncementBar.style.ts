import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledAnnouncementBarProps } from './AnnouncementBar.types'

export const AnnouncementBar = styled.div((props: StyledAnnouncementBarProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.primary};
  padding: ${props.theme.spacing.fixed[2]}px 0;
  font-weight: 700;
  color: ${props.theme.colours.tertiary};
  text-align: center;
  font-size: ${props.theme.typography.paragraph[2].fontSize};
  line-height: ${props.theme.typography.paragraph[2].lineHeight};
  position: relative;
  z-index: 50;
`)
