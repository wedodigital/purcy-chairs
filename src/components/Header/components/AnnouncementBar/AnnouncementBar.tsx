import React, { FC, ReactElement } from 'react'

import Container from '@components/Container'

import * as Styled from './styles/AnnouncementBar.style'

import { AnnouncementBarProps } from './AnnouncementBar.types'

const AnnouncementBar: FC<AnnouncementBarProps> = ({ text }: AnnouncementBarProps): ReactElement => {
  return (
    <Styled.AnnouncementBar>
      <Container width='wide'>{text}</Container>
    </Styled.AnnouncementBar>
  )
}

export default AnnouncementBar
