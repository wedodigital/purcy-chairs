import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledHeaderProps } from './Header.style.types'

type HeaderTheme = Pick<StyledHeaderProps, 'theme'>

export const Header = styled.header((props: StyledHeaderProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[2]}px 0;
  position: sticky;
  top: 0;
  width: 100%;
  z-index: 100;
  background: ${props.theme.colours.secondary};
  border-bottom: 1px solid #ccc;

  ${props.appearance === 'secondary' && css`
    border-bottom: none;
  `}

  ${props.theme.mixins.respondTo.md(css`
    background: none;

    ${props.appearance === 'primary' && css`
      background: ${props.theme.colours.secondary};
      border-bottom: 1px solid #ccc;
    `}

    ${props.appearance === 'tertiary' && css`
      background: none;
    `}
  `)}

  [class*=Container] {
    display: flex;
    justify-content: space-between;
    position: relative;
    height: 40px;

    ${props.theme.mixins.respondTo.md(css`
      height: 64px;
    `)}
  }
`)

export const ContentBlock = styled.div((props: HeaderTheme): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  gap: ${props.theme.spacing.fixed[2]}px;
`)

export const LogoBlock = styled.div(() => css`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`)


export const IconButton = styled.div((props: HeaderTheme) => css`
  all: unset;
  background: none;
  border: none;

  svg {
    width: ${props.theme.spacing.fixed[4]}px;
    height: ${props.theme.spacing.fixed[4]}px;
  }
`)
