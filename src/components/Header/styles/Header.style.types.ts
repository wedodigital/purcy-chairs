import { Theme } from '@themes/purcyTheme/purcyTheme.types'

import { HeaderProps } from '../Header.types'

export interface StyledHeaderProps {
  theme: Theme
  appearance: HeaderProps['appearance']
}
