import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledValidationProps {
  theme: Theme
  isValid: boolean
}
