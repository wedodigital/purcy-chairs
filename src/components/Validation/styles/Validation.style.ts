import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledValidationProps } from './Validation.style.types'

export const Validation = styled.div((props: StyledValidationProps): FlattenSimpleInterpolation => css`
  ${!props.isValid && css`
    border: 1px solid ${props.theme.colours.primary};
    padding: ${props.theme.spacing.fixed[2]}px;
    margin-bottom: ${props.theme.spacing.fixed[4]}px;

    & > input {
      margin-bottom: 0;
    }
  `}
`)

export const ValidationMessage = styled.div((props: StyledValidationProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  gap: ${props.theme.spacing.fixed[1]}px;
  margin-bottom: ${props.theme.spacing.fixed[1]}px;
`)
