import React, { FC, ReactElement } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExclamationCircle } from '@fortawesome/free-solid-svg-icons'

import * as Styled from './styles/Validation.style'

import { ValidationProps } from './Validation.types'

export const Validation: FC<ValidationProps> = ({ validationMessage, children }: ValidationProps): ReactElement => {
  return (
    <Styled.Validation isValid={!!!validationMessage}>
      <If condition={validationMessage}>
        <Styled.ValidationMessage>
          <FontAwesomeIcon icon={faExclamationCircle} />
          <span>{validationMessage}</span>
        </Styled.ValidationMessage>
      </If>
      {children}
    </Styled.Validation>
  )
}

export default Validation
