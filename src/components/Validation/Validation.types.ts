export interface ValidationProps {
  validationMessage?: string
  children: React.ReactNode
}
