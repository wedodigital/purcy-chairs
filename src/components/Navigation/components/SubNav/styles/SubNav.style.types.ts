import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledSubNavProps {
  theme: Theme;
}
