import styled, { css, FlattenSimpleInterpolation } from 'styled-components'
import { Link } from 'gatsby'

import { StyledSubNavProps } from './SubNav.style.types'

export const SubNav = styled.div((props: StyledSubNavProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.tertiary};
  position: absolute;
  white-space: nowrap;
  top: 100%;
  left: 0;
  color: ${props.theme.colours.primary};
  opacity: 0;
  visibility: hidden;
  transform: translateY(-10px);
  transition: 0.2s all ease 0.2s;
  z-index: 10;
  display: flex;

  ul {
    padding: ${props.theme.spacing.fixed[2]}px;
    position: relative;
  }
`)

export const NavLink = styled(Link)((props: StyledSubNavProps): FlattenSimpleInterpolation => css`
  color: ${props.theme.colours.primary};
  text-decoration: none;
  transition: 0.4s all ease;

  &::after {
    content: '';
    position: absolute;
    top: 0;
    left: 100%;
    bottom: 0;
    aspect-ratio: 16 / 9;
    background: url(${props.backgroundImage}) center center no-repeat;
    background-size: cover;
    opacity: 0;
    transition: 0.4s all ease;
    z-index: 1;
  }

  &:hover  {
    text-decoration: underline;

    &::after {
      opacity: 1;
    }
  }
`)

export const ProductImage = styled.div((props: StyledSubNavProps): FlattenSimpleInterpolation => css`
  aspect-ratio: 16 / 9;
  background: url(${props.backgroundImage}) center center no-repeat;
  background-size: cover;
  position: absolute;
  top: 0;
  left: 100%;
  bottom: 0;
  aspect-ratio: 16 / 9;
`)
