import React, { ReactElement, FC } from 'react'

import * as Styled from './styles/SubNav.style'

import { SubNavProps } from './SubNav.types'

const SubNav: FC<SubNavProps> = ({
  links,
  urlPrefix,
}: SubNavProps): ReactElement => {
  return (
    <Styled.SubNav>
      <ul>
        {
          links.map((link) => {
            return (
              <li>
                <Styled.NavLink backgroundImage={link.galleryImages.nodes[0].sourceUrl} to={link.databaseId === 11 ? urlPrefix : `${urlPrefix}/${link.slug}`}>{link.name}</Styled.NavLink>
              </li>
            )
          })
        }
      </ul>
      <Styled.ProductImage backgroundImage={links[0].galleryImages.nodes[0].sourceUrl} />
    </Styled.SubNav>
  )
}

export default SubNav
