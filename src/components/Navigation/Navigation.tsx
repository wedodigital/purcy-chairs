import React, { FC, ReactElement, useState } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { CSSTransition } from 'react-transition-group'

import Hamburger from '@assets/menu.svg'

import MatchMedia from '@utils/MatchMedia'

import Overlay from '@components/Overlay'

import MobileNav from './components/MobileNav'
import SubNav from './components/SubNav'

import * as Styled from './styles/Navigation.style'

import { NavigationProps, NavigationState, Link } from './Navigation.types'

const Navigation: FC<NavigationProps> = ({
  appearance = 'primary',
  navigation,
}): ReactElement => {
  return (
    <Styled.Navigation>
      {navigation.map((link, index: number) => {
        return (
          <li key={index}>
            <Styled.NavLink
              to={link.url}
              inverse={appearance === 'secondary'}
            >
              {link.title}
            </Styled.NavLink>
            <If condition={link.children}>
              <SubNav links={link.children} urlPrefix={link.url} />
            </If>
          </li>
        )
      })}
    </Styled.Navigation>
    )
  }

export default Navigation
