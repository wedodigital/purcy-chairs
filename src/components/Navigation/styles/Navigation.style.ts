import styled, { css, FlattenSimpleInterpolation } from 'styled-components'
import { Link } from 'gatsby'

import { StyledNavigationProps } from './Navigation.style.types'

import { SubNav } from '../components/SubNav/styles/SubNav.style'

export const Navigation = styled.ul((props: Pick<StyledNavigationProps, 'theme'>): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;

  & > li {
    margin-right: ${props.theme.spacing.fixed[4]}px;
    position: relative;

    &:last-child {
      margin-right: 0;
    }

    &:hover {
      ${SubNav} {
        opacity: 1;
        visibility: visible;
        transform: translateY(10px);
      }
    }
  }
`)

export const NavLink = styled(Link)((props: StyledNavigationProps): FlattenSimpleInterpolation => css`
  color: ${props.theme.colours.primary};
  text-decoration: none;

  ${props.inverse && css`
    color: ${props.theme.colours.tertiary};
  `}
`)

export const HamburgerIcon = styled.button((props: StyledNavigationProps): FlattenSimpleInterpolation => css`
  background: none;
  color: ${props.theme.colours.tertiary};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  text-decoration: none;
  font-weight: 700;
  border: none;
  cursor: pointer;
  font-size: 24px;
  line-height: 24px;
  font-family: ${props.theme.typography.fontFamily};
  width: ${props.theme.spacing.fixed[4]}px;
  height: ${props.theme.spacing.fixed[4]}px;


  & > svg {
    width: ${props.theme.spacing.fixed[4]}px;
    height: ${props.theme.spacing.fixed[4]}px;

    * {
      fill: ${props.theme.colours.primary};

      ${props.inverse && css`
        fill: ${props.theme.colours.tertiary};
      `}
    }
  }
`)
