import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledNavigationProps {
  theme: Theme
  inverse: boolean
}
