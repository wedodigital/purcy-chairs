export interface NavigationProps {
  appearance: 'primary' | 'secondary'
  navigation: {
    title: string
    url: string
    children?: {
      title: string
      url: string
    }[]
  }[]
}
