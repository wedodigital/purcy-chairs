import { Theme } from '@themes/purcyTheme/purcyTheme.types'
import { ProductDetails } from '@context/PageContext.types'

export interface StyledColourSwitcherProps {
  theme: Theme
  hex: ProductDetails['description']
  isSelected: boolean
}
