import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledColourSwitcherProps } from './ColourSwitcher.types'

export const ColourSwitcher = styled.div((props: Pick<StyledColourSwitcherProps, 'theme'>): FlattenSimpleInterpolation => css`
  display: flex;
  margin-right: ${props.theme.spacing.fixed[4]}px;

  &:last-child {
    margin-right: 0;
  }
`)

export const ColourSwatch = styled.button((props: StyledColourSwitcherProps): FlattenSimpleInterpolation => css`
  background: ${props.hex};
  display: block;
  width: ${props.theme.spacing.fixed[3]}px;
  height: ${props.theme.spacing.fixed[3]}px;
  border: none;
  padding: 0;
  cursor: pointer;
  text-indent: -10000px;
  border-radius: 50%;
  position: relative;
  margin-right: ${props.theme.spacing.fixed[2]}px;


  &:last-child {
    margin-right: 0;
  }

  &::after {
    content: '';
    display: block;
    width: ${props.theme.spacing.fixed[4]}px;
    height: ${props.theme.spacing.fixed[4]}px;
    border-radius: 50%;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border: 2px solid transparent;
    transition: 0.4s all ease;
  }

  ${props.isSelected && css`
    &::after {
      border-color: ${props.hex};
    }
  `}
`)
