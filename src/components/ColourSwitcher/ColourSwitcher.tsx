import React, { FC, ReactElement, useContext } from 'react'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

import * as Styled from './styles/ColourSwitcher.style'

import { ColourSwitcherProps } from './ColourSwitcher.types'

const ColourSwitcher: FC<ColourSwitcherProps> = ({
  products = [12, 13],
  onSelect,
  activeProductId,
}: ColourSwitcherProps): ReactElement => {
  const pageContext = useContext(PageContext) as PageContextType
  const productVariants = []
  products.forEach((variant) => {
    productVariants.push(pageContext.productList.find(product => product.databaseId === variant ))
  })

  return (
    <Styled.ColourSwitcher>
      {
        productVariants.map((product) => {
          return (
            <Styled.ColourSwatch
              key={product.databaseId}
              onClick={() => onSelect(product)}
              isSelected={product.databaseId === activeProductId}
              hex={product.description}
            >
              {product.title}
            </Styled.ColourSwatch>
          )
        })
      }
    </Styled.ColourSwitcher>
  )
}

export default ColourSwitcher
