export interface ColourSwitcherProps {
  products?: number[]
  activeProductId: number
  onSelect: () => void
}
