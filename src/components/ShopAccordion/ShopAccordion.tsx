import React, { FC, ReactElement } from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import Container from '@components/Container'
import CollapsibleContainer from '@components/CollapsibleContainer'
import RawHtmlWrapper from '@components/RawHtmlWrapper'

import FAQs from './components/FAQs'
import Reviews from './components/Reviews'

import * as Styled from './styles/ShopAccordion.style'

import { ShopAccordionProps } from './ShopAccordion.types'

const ShopAccordion: FC<ShopAccordionProps> = ({ showFaqs, showReviews, showDelivery}): ReactElement => {
  const accordionContent = useStaticQuery(graphql`
    query Delivery {
      shopPage: wpPage(databaseId: {eq: 7}) {
        shopPageFields {
          deliveryInfo
          reviews {
            rating
            reviewAuthor
            reviewContent
            reviewTitle
          }
          returnsPolicy
        }
      }
      faqsPage: wpPage(databaseId: {eq: 54}) {
        supportPage {
          faqs {
            answer
            question
          }
        }
      }
    }
  `)

  return (
    <Container>
      <Styled.ShopAccordion>
        <If condition={accordionContent.shopPage.shopPageFields.deliveryInfo && showDelivery}>
          <CollapsibleContainer title='Delivery'>
            <RawHtmlWrapper content={accordionContent.shopPage.shopPageFields.deliveryInfo} />
          </CollapsibleContainer>
        </If>
        <If condition={accordionContent.faqsPage.supportPage.faqs && showFaqs}>
          <CollapsibleContainer title='FAQs'>
            <FAQs />
          </CollapsibleContainer>
        </If>
        <If condition={accordionContent.shopPage.shopPageFields.reviews && showReviews}>
          <CollapsibleContainer title='Reviews'>
            <Reviews reviews={accordionContent.shopPage.shopPageFields.reviews} />
          </CollapsibleContainer>
        </If>
        <If condition={accordionContent.shopPage.shopPageFields.returnsPolicy}>
          <CollapsibleContainer title='Returns'>
            <RawHtmlWrapper content={accordionContent.shopPage.shopPageFields.returnsPolicy} />
          </CollapsibleContainer>
        </If>
      </Styled.ShopAccordion>
    </Container>
  )
}

export default ShopAccordion
