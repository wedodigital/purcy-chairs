import React, { FC, ReactElement } from 'react'

import Star from '@assets/star.svg'

import Heading from '@components/Heading'
import RawHtmlWrapper from '@components/RawHtmlWrapper'

import * as Styled from './styles/Reviews.style'

import { ReviewsProps } from './Reviews.types'

const Reviews: FC<ReviewsProps> = ({ reviews }: ReviewsProps): ReactElement => {

  const starRatings = [1, 2, 3, 4, 5]

  return (
    <>
      {
        reviews.map((review) => {
          return (
            <Styled.Review>
              <Styled.StarRating>
                {starRatings.map((starRating) => {
                  return (
                    <Styled.Star highlight={starRating <= review.rating}>
                      <Star />
                    </Styled.Star>
                  )
                })}
              </Styled.StarRating>
              <Heading size={1} weight={3} text={review.reviewTitle} />
              <RawHtmlWrapper content={review.reviewContent} />
              <Styled.ReviewAuthor>
                <Heading size={1} weight={3} text={review.reviewAuthor} />
              </Styled.ReviewAuthor>
            </Styled.Review>
          )
        })
      }
    </>
  )
}

export default Reviews
