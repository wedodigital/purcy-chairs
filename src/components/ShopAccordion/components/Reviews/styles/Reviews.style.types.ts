import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledReviewsProps {
  theme: Theme
  highlight: boolean
}
