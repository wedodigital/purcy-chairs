import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledReviewsProps } from './Reviews.style.types'

export const Review = styled.div((props: Pick<StyledReviewsProps, 'theme'>): FlattenSimpleInterpolation => css`
  padding-bottom: ${props.theme.spacing.fixed[4]}px;
  margin-bottom: ${props.theme.spacing.fixed[4]}px;
  border-bottom: 1px solid ${props.theme.colours.quartenary};

  &:last-child {
    padding-bottom: 0;
    margin-bottom: 0;
    border-bottom: none;
  }
`)

export const StarRating = styled.div((props: Pick<StyledReviewsProps, 'theme'>): FlattenSimpleInterpolation => css`
  display: flex;

  margin-bottom: ${props.theme.spacing.fixed[4]}px;
`)

export const Star = styled.div((props: StyledReviewsProps): FlattenSimpleInterpolation => css`
  width: ${props.theme.spacing.fixed[3]}px;
  height: ${props.theme.spacing.fixed[3]}px;

  svg {
    width: 100%;
    height: 100%;

    * {
      fill: ${props.theme.colours.tertiary};

      ${props.highlight && css`
        fill: #D1A364;
      `}
    }
  }
`)

export const ReviewAuthor = styled.div((props: StyledReviewsProps): FlattenSimpleInterpolation => css`
  padding-top: ${props.theme.spacing.fixed[2]}px;
`)
