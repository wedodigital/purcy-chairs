export interface ReviewsProps {
  reviews: {
    rating: 1 | 2 | 3 | 4 | 5
    reviewTitle: string
    reviewContent: string
    reviewAuthor: string
  }[]
}
