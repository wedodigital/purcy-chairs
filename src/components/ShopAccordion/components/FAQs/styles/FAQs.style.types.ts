import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledFAQsProps {
  theme: Theme
}
