import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledFAQsProps } from './FAQs.style.types'

export const Faq = styled.div((props: StyledFAQsProps): FlattenSimpleInterpolation => css`
  padding-bottom: ${props.theme.spacing.fixed[4]}px;
  margin-bottom: ${props.theme.spacing.fixed[4]}px;
  border-bottom: 1px solid ${props.theme.colours.quartenary};

  &:last-child {
    padding-bottom: 0;
    margin-bottom: 0;
    border-bottom: none;
  }
`)
