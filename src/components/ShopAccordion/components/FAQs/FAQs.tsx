import React, { FC, ReactElement } from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import Heading from '@components/Heading'
import RawHtmlWrapper from '@components/RawHtmlWrapper'

import * as Styled from './styles/FAQs.style'

const FAQs: FC = (): ReactElement => {
  const faqs = useStaticQuery(graphql`
    query Faqs {
      wpPage(databaseId: {eq: 54}) {
        supportPage {
          faqs {
            answer
            question
          }
        }
      }
    }
  `)

  return (
    <>
      {faqs.wpPage.supportPage.faqs.map((faq) => {
        return (
          <Styled.Faq>
            <Heading text={faq.question} size={2} weight={3} noMargin />
            <RawHtmlWrapper content={faq.answer} />
          </Styled.Faq>
        )
      })}
    </>
  )
}

export default FAQs
