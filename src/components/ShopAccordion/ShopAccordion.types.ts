export interface ShopAccordionProps {
  showFaqs?: boolean
  showReviews?: boolean
  showDelivery?: boolean
}
