import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledShopAccordionProps } from './ShopAccordion.style.types'

export const ShopAccordion = styled.div((props: StyledShopAccordionProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[6]}px;

  ${props.theme.mixins.respondTo.md(css`
    margin-bottom: ${props.theme.spacing.fixed[10]}px;
  `)}
`)
