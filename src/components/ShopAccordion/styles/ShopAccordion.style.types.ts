import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledShopAccordionProps {
  theme: Theme
}
