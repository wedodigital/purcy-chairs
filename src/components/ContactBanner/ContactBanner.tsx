import React, { FC, ReactElement } from 'react'

import Heading from '@components/Heading'
import Container from '@components/Container'
import Button from '@components/Button'
import RawHtmlWrapper from '@components/RawHtmlWrapper'

import { ContactBannerProps } from './ContactBanner.types'

import * as Styled from './styles/ContactBanner.style'

const ContactBanner: FC<ContactBannerProps> = ({
  title,
  content,
  email,
  telephone,
}: ContactBannerProps): ReactElement => {
  return (
    <Styled.ContactBanner>
      <Container width='narrow'>
        <Heading inverse size={5} weight={3} text={title} noMargin />
        <RawHtmlWrapper content={content} />
        <Styled.ButtonGroup>
          <Button href={`mailto:${email}`} inverse text={email} />
          <Button href={`tel:${telephone}`} inverse text={telephone} />
        </Styled.ButtonGroup>
      </Container>
    </Styled.ContactBanner>
  )
}

export default ContactBanner
