import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledContactBannerProps } from './ContactBanner.types'

export const ContactBanner = styled.div((props: StyledContactBannerProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.primary};
  color: ${props.theme.colours.tertiary};
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: ${props.theme.spacing.fixed[6]}px 0;

  ${props.theme.mixins.respondTo.md(css`
    min-height: 600px;
  `)}
`)

export const ButtonGroup = styled.div((props: StyledContactBannerProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  flex-direction: column;
  margin: ${props.theme.spacing.fixed[6]}px -${props.theme.spacing.fixed[2]}px 0;

  ${props.theme.mixins.respondTo.md(css`
    flex-direction: row;
  `)}

  & > * {
    margin: ${props.theme.spacing.fixed[2]}px ${props.theme.spacing.fixed[2]}px 0;
    flex-grow: 1;
    
    ${props.theme.mixins.respondTo.md(css`
      width: 50%;
    `)}
  }
`)
