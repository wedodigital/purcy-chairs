import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledContactBannerProps {
  theme: Theme
}
