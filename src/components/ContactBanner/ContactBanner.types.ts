export interface ContactBannerProps {
  title: string
  content: string
  email: string
  telephone: string
}
