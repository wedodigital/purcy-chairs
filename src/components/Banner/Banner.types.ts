export interface BannerProps {
  backgroundImage: string
  heading?: string
  buttonText?: string
  buttonUrl?: string
}
