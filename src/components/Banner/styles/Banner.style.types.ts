import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledBannerProps {
  theme: Theme
  backgroundImage: string
}
