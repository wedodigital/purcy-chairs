import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledBannerProps } from './Banner.style.types'

export const Banner = styled.div((props: StyledBannerProps): FlattenSimpleInterpolation => css`
  background: url(${props.backgroundImage}) center center no-repeat;
  background-size: cover;
  padding: ${props.theme.spacing.fixed[10]}px ${props.theme.spacing.fixed[4]}px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;
  min-height: 320px;
  margin-bottom: ${props.theme.spacing.fixed[6]}px;

  ${props.theme.mixins.respondTo.md(css`
    margin-bottom: ${props.theme.spacing.fixed[10]}px;
  `)}

  ${props.theme.mixins.respondTo.md(css`
    min-height: 720px;
  `)}
`)
