import React, { FC, ReactElement } from 'react'

import Heading from '@components/Heading'
import Button from '@components/Button'
import Container from '@components/Container'

import * as Styled from './styles/Banner.style'

import { BannerProps } from './Banner.types'

const Banner: FC<BannerProps> = ({
  heading,
  backgroundImage,
  buttonText,
  buttonUrl,
}: BannerProps): ReactElement => {
  return (
    <Container width='mid'>
      <Styled.Banner backgroundImage={backgroundImage}>
        <If condition={heading}>
          <Heading text={heading} weight={3} size={5} inverse />
        </If>
        <If condition={buttonUrl}>
          <Button appearance='secondary' inverse to={buttonUrl} text={buttonText} size={3} />
        </If>
      </Styled.Banner>
    </Container>
  )
}

export default Banner
