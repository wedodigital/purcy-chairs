import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledEmailSignupProps {
  theme: Theme
  inverse: boolean
  columns: 1 | 2
  size: 1 | 2
}
