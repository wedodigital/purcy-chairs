import styled, { FlattenSimpleInterpolation, css } from 'styled-components'
import { Form } from 'formik'

import { StyledEmailSignupProps } from './EmailSignup.style.types'

export const EmailSignup = styled.div((props: StyledEmailSignupProps): FlattenSimpleInterpolation => css`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  width: 100%;

  ${props.theme.mixins.respondTo.md(css`
    flex-direction: row;
    align-items: space-between;
  `)}

  & > div {
    flex-grow: 1;
    padding: ${props.theme.spacing.fixed[2]}px 0;

    &:first-child {
      padding-top: 0;
    }

    &:last-child {
      padding-bottom: 0;
    }

    ${props.theme.mixins.respondTo.md(css`
      padding: 0 ${props.theme.spacing.fixed[2]}px;

      &:first-child {
        padding-left: 0;
      }

      &:last-child {
        padding-right: 0;
      }
    `)}
  }

  ${props.columns === 1 && css`
    flex-direction: column;
    text-align: center;
    align-items: stretch;
    margin: 0;

    ${props.theme.mixins.respondTo.md(css`
      flex-direction: column;
    `)}

    ${props.size === 1 && css`
      text-align: left;
    `}

    & > div {
      flex-grow: 1;
      padding: ${props.theme.spacing.fixed[2]}px 0;

      ${props.size === 1 && css`
        &:first-child {
          padding-top: 0;
        }

        &:last-child {
          padding-bottom: 0;
        }
      `}
    }
  `}

  label {
    display: block;
    font-weight: 700;
    margin-bottom: ${props.theme.spacing.fixed[2]}px;
    text-align: left;

    ${props.inverse && css`
      color: ${props.theme.colours.tertiary};
    `}
  }

  input,
  select {
    width: 100%;
    border: 1px solid transparent;
    border-bottom: 1px solid ${props.theme.colours.primary};
    border-radius: 0;
    font-size: ${props.theme.typography.paragraph[3].fontSize};
    line-height: ${props.theme.typography.paragraph[3].lineHeight};
    font-family: aktiv-grotesk, sans-serif;
    color: ${props.theme.colours.primary};
    margin-bottom: ${props.theme.spacing.fixed[4]}px;
    padding: ${props.theme.spacing.fixed[1]}px ${props.theme.spacing.fixed[2]}px;
    background: none;
    transition: 0.4s all ease;

    ${props.inverse && css`
      border-bottom: 1px solid ${props.theme.colours.tertiary};
      color: ${props.theme.colours.tertiary};
    `}

    &:focus {
      outline: none;
      border: 1px solid ${props.theme.colours.primary};
      -webkit-appearance: none;

      ${props.inverse && css`
        border: 1px solid ${props.theme.colours.tertiary};
      `}
    }

    &:last-child {
      margin-bottom: 0;
    }
  }
`)

export const SignupForm = styled.form((props: Pick<StyledEmailSignupProps, 'theme' | 'size'>) => css`
  display: flex;
  align-items: flex-end;

  margin-bottom: ${props.theme.spacing.fixed[2]}px;

  ${props.size === 1 && css`
    flex-direction: column;
    align-items: stretch;
  `}

  & > div {
    flex-grow: 1;
    margin-right: ${props.theme.spacing.fixed[4]}px;

    ${props.size === 1 && css`
      margin: 0 0 ${props.theme.spacing.fixed[2]}px;
    `}
  }

  [class*=Button] {
    flex-shrink: 0;

    ${props.size === 1 && css`
      align-self: flex-start;
    `}
  }
`)
