import React, { FC, ReactElement, useContext } from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import Heading from '@components/Heading'
import Paragraph from '@components/Paragraph'
import Button from '@components/Button'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

import * as Styled from './styles/EmailSignup.style'

import { EmailSignupProps } from './EmailSignup.types'

const EmailSignup: FC<EmailSignupProps> = ({
  size = 2,
  columns,
  inverse,
  formId,
}): ReactElement => {
  const context = useContext(PageContext) as PageContextType
  const emailSignup = useStaticQuery(graphql`
    query EmailSignUp {
      wpPage(databaseId: {eq: 2}) {
        homepage {
          signupSmallprint
          signupSubtitle
          signupTitle
        }
      }
    }
  `)

  return (
    <Styled.EmailSignup columns={columns} size={size} inverse={inverse}>
      <div>
        <Heading text={context.journeyType === 'capture' ? 'Receive 20% Off Today' : emailSignup.wpPage.homepage.signupTitle} inverse={inverse} size={size === 1 ? 1 : 3} weight={3} noMargin />
        <Choose>
          <When condition={context.journeyType === 'capture'}>
            <Paragraph inverse={inverse}>We will send you an email shortly with instructions of how to redeem your exclusive offer</Paragraph>
          </When>
          <Otherwise>
            <If condition={emailSignup.wpPage.homepage.signupSubtitle}>
              <Paragraph inverse={inverse}>{emailSignup.wpPage.homepage.signupSubtitle}</Paragraph>
            </If>
          </Otherwise>
        </Choose>
      </div>
      <div>
        <Styled.SignupForm
          size={size!}
          data-drip-embedded-form={context.formId}
          action={`https://www.getdrip.com/forms/${formId ? formId : context.formId}/submissions`}
          method='post'
        >
          <div>
            <input type='email' id="emailAddress" name="fields[email]" placeholder='Email Address' />
          </div>
          <Button type='submit' text='Sign up' size={2} inverse={inverse} />
        </Styled.SignupForm>
        <Paragraph text={emailSignup.wpPage.homepage.signupSmallprint} size={2} inverse={inverse} />
      </div>
    </Styled.EmailSignup>
  )
}

export default EmailSignup
