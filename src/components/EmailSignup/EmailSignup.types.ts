export interface EmailSignupProps {
  columns: 1 | 2
  size?: 1 | 2
  inverse?: boolean
  endpoint: string
  formId?: string
}
