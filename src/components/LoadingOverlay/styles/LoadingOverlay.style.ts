import styled, { css, createGlobalStyle, FlattenSimpleInterpolation } from 'styled-components'

import { StyledLoadingOverlayProps } from './LoadingOverlay.style.types'

export const HideOverflow = createGlobalStyle((): FlattenSimpleInterpolation => css`
  html,
  body {
    overflow: hidden;
  }
`)

export const LoadingOverlay = styled.div((props: StyledLoadingOverlayProps): FlattenSimpleInterpolation => css`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1000;
  background: ${props.theme.colours.secondary};
  display: flex;
  align-items: center;
  justify-content: center;
`)

export const LoadingBarBackground = styled.div((props: StyledLoadingOverlayProps): FlattenSimpleInterpolation => css`
  width: 90%;
  width: 252px;
  margin: 0 auto;
  height: 128px;
  position: relative;
  overflow: hidden;

  svg {
    height: 128px;

    * {
      fill: ${props.theme.colours.tertiary};
    }
  }
`)

export const LoadingBar = styled.div((props: StyledLoadingOverlayProps): FlattenSimpleInterpolation => css`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  width: 0;
  overflow: hidden;
  animation-name: loadingState;
  animation-duration: 60s;
  animation-fill-mode: forwards;

  svg {
    height: 128px;

    * {
      fill: ${props.theme.colours.primary};
    }
  }

  ${props.loaded && css`
    animation-duration: 1s;
  `}

  @keyframes loadingState {
    from {
      width: 0;
    }
    to {
      width: 100%;
    }
  }
`)
