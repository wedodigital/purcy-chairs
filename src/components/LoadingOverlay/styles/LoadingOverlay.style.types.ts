import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledLoadingOverlayProps {
  theme: Theme
  loaded: boolean
}
