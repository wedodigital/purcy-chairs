import React, { FC, ReactElement, Fragment } from 'react'

import LogoSVG from '@assets/logo.svg'

import { LoadingOverlayProps } from './LoadingOverlay.types'

import * as Styled from './styles/LoadingOverlay.style'

const LoadingOverlay: FC<LoadingOverlayProps> = ({
  loaded,
}: LoadingOverlayProps): ReactElement => {
  return (
    <>
      <Styled.HideOverflow />
      <Styled.LoadingOverlay>
        <Styled.LoadingBarBackground>
          <LogoSVG />
          <Styled.LoadingBar loaded={loaded}>
            <LogoSVG />
          </Styled.LoadingBar>
        </Styled.LoadingBarBackground>
      </Styled.LoadingOverlay>
    </>
  )
}

export default LoadingOverlay
