import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledFeatureColumnsProps } from './FeatureColumns.style.types'

export const FeatureColumns = styled.div((props: StyledFeatureColumnsProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[6]}px 0;

  [class*=Heading] {
    margin-bottom: ${props.theme.spacing.fixed[6]}px;
  }

  ${props.theme.mixins.respondTo.md(css`
    padding: ${props.theme.spacing.fixed[10]}px 0;
  `)}
`)

export const Columns = styled.div((props: StyledFeatureColumnsProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: flex-start;
  flex-direction: column;

  ${props.theme.mixins.respondTo.md(css`
    flex-direction: row;
  `)}
`)
