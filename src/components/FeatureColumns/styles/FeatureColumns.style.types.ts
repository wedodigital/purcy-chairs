import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledFeatureColumnsProps {
  theme: Theme;
}
