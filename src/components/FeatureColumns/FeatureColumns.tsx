import React, { ReactElement, FC } from 'react'

import Heading  from '@components/Heading'
import Container from '@components/Container'

import Column from './components/Column'

import * as Styled from './styles/FeatureColumns.style'

import { FeatureColumnsProps } from './FeatureColumns.types'

const FeatureColumns: FC<FeatureColumnsProps> = ({
  title,
  columns,
}: FeatureColumnsProps): ReactElement => {
  return (
    <Styled.FeatureColumns>
      <Container>
        <If condition={title}>
          <Heading size={3} weight={2} text={title} />
        </If>
        <Styled.Columns>
          {
            columns.map((column, index) => {
              return (
                <Column
                  key={index}
                  icon={column.icon.sourceUrl}
                  content={column.content}
                />
              )
            })
          }
        </Styled.Columns>
      </Container>
    </Styled.FeatureColumns>
  )
}

export default FeatureColumns
