export interface FeatureColumnsProps {
  title?: string
  columns: {
    icon: {
      sourceUrl: string
    }
    content: string
  }[]
}
