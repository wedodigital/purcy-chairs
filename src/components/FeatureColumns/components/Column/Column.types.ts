export interface ColumnProps {
  icon: string
  content: string
}
