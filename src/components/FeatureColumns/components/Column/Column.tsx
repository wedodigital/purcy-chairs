import React, { FC, ReactElement } from 'react'

import RawHtmlWrapper from '@components/RawHtmlWrapper'

import * as Styled from './styles/Column.style'

import { ColumnProps } from './Column.types'

const Column: FC<ColumnProps> = ({ icon, content }): ReactElement => {
  return (
    <Styled.Column>
      <Styled.IconBackground>
        <img src={icon} />
      </Styled.IconBackground>
      <Styled.Content>
        <RawHtmlWrapper content={content} />
      </Styled.Content>
    </Styled.Column>
  )
}

export default Column
