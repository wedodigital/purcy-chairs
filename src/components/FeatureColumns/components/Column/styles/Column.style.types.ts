import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledColumnProps {
  theme: Theme
}
