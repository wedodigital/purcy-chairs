import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledColumnProps } from './Column.style.types'

export const Column = styled.div((props: StyledColumnProps): FlattenSimpleInterpolation => css`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  padding: ${props.theme.spacing.fixed[2]}px 0;

  &:first-child {
    padding-top: 0;
  }

  &:last-child {
    padding-bottom: 0;
  }

  ${props.theme.mixins.respondTo.md(css`
    padding: 0 ${props.theme.spacing.fixed[4]}px;

    &:first-child {
      padding-left: 0;
    }

    &:last-child {
      padding-right: 0;
    }
  `)}
`)

export const IconBackground = styled.div((props: StyledColumnProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.primary};
  padding: ${props.theme.spacing.fixed[2]}px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: ${props.theme.spacing.fixed[4]}px;

  img {
    display: block;
    max-width: 20px;
  }
`)

export const Content = styled.div((): FlattenSimpleInterpolation => css`
  width: 100%;
`)
