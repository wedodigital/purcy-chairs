import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledSimpleBasketProps } from './SimpleBasket.style.types'

export const SimpleBasketTable = styled.table((props: Pick<StyledSimpleBasketProps, 'theme'>): FlattenSimpleInterpolation => css`
  width: 100%;
  margin-bottom: ${props.theme.spacing.fixed[4]}px;
  border: 1px solid ${props.theme.colours.primary};

  tfoot {
    tr {
      td {
        background: ${props.theme.colours.primary};
        color: ${props.theme.colours.tertiary};
        text-align: right;
      }
    }
  }

  tr {
    td {
      border-bottom: 1px solid ${props.theme.colours.primary};
      padding: ${props.theme.spacing.fixed[1]}px ${props.theme.spacing.fixed[2]}px;
      font-weight: 700;

      &:last-child {
        text-align: right;
      }
    }

    &:last-child {
      td {
        border-bottom: none;
      }
    }
  }
`)
