import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledSimpleBasketProps {
  theme: Theme
}
