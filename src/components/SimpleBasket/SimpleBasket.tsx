import React, { FC, ReactElement, useContext } from 'react'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

import * as Styled from './styles/SimpleBasket.style'

import { SimpleBasketProps } from './SimpleBasket.types'

const SimpleBasket: FC<SimpleBasketProps> = (): ReactElement => {
  const pageContext = useContext(PageContext) as PageContextType
  return (
    <>
      <Styled.SimpleBasketTable>
        <tbody>
          {
            pageContext.basket.products.map((basketItem, index) => {
              if (basketItem.product.status === 'private') return
              return (
                <tr key={index}>
                  <td>{basketItem.quantity}x</td>
                  <td>{basketItem.product.name}</td>
                  <Choose>
                    <When condition={basketItem.product.onSale}>
                      <td>£{(basketItem.product.salePrice * basketItem.quantity).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                    </When>
                    <Otherwise>
                      <td>£{(basketItem.product.regularPrice * basketItem.quantity).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                    </Otherwise>
                  </Choose>
                </tr>
              )
            })
          }
          <tr>
            <td>1x</td>
            <td>{pageContext.shippingMethod.method_title}</td>
            <td>£{pageContext.shippingMethod.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
          </tr>
          {
            pageContext.basket.discounts.map((discount, index) => {
              return (
                <tr>
                  <td>1x</td>
                  <td>Voucher Code: {discount.code}</td>
                  <td>-£{discount.amount}</td>
                </tr>
              )
            })
          }
        </tbody>
        <tfoot>
          <tr>
            <td colSpan='3'>Total: £{pageContext.totalPrice.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
          </tr>
        </tfoot>
      </Styled.SimpleBasketTable>
    </>
  )
}

export default SimpleBasket
