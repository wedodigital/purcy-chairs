import React, { ReactElement, FC, useContext } from 'react'

import Heading from '@components/Heading'
import TotalPrice from '@components/TotalPrice'
import Paragraph from '@components/Paragraph'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

import * as Styled from './styles/ProductHeader.style'

import { ProductHeaderProps } from './ProductHeader.types'

const ProductHeader: FC<ProductHeaderProps> = ({
  title,
  activeProduct,
  databaseId,
}: ProductHeaderProps): ReactElement => {
  const context = useContext(PageContext) as PageContextType
  return (
    <Styled.ProductHeader>
      <Heading level={1} size={1} weight={3} noMargin text={title} />
      <TotalPrice
        regularPrice={activeProduct.regularPrice}
        salePrice={activeProduct.salePrice}
        onSale={context.journeyType !== 'capture' && activeProduct.onSale}
        quantity={1}
      />
    </Styled.ProductHeader>
  )
}

export default ProductHeader
