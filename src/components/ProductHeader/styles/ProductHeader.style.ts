import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { FormSection } from '@components/TotalPrice/styles/TotalPrice.style'

export const ProductHeader = styled.div((): FlattenSimpleInterpolation => css`
  ${FormSection} {
    padding: 0;
    border-bottom: none;
  }
`)
