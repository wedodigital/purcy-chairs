import { ReactNode } from 'react'

export interface ProductHeaderProps {
  /**
   * React children
   * */
  children: ReactNode
}
