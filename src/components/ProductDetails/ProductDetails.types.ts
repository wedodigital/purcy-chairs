export interface ProductDetailsProps {
  variations: number[]
  shortDescription?: string
  title: string
  databaseId: number
  showGallery?: boolean
  onSelectItem: (image: string) => void
}
