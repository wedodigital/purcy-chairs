import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { FormSection } from '@components/TotalPrice/styles/TotalPrice.style'

import { StyledProductDetailsProps } from './ProductDetails.style.types'

type PDTheme = Pick<StyledProductDetailsProps, 'theme'>

export const ProductDetails = styled.div((): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  width: 100%;
`)

export const Gallery = styled.div((props: StyledProductDetailsProps): FlattenSimpleInterpolation => css`
  width: 50%;
  display: block;
  width: 50%;
  margin-right: ${props.theme.spacing.fixed[3]}px;
  max-width: 440px;
  background: url('${props.backgroundImage}') center center no-repeat;
  background-size: cover;
  aspect-ratio: 1 / 1;
`)

export const Content = styled.div((props: PDTheme): FlattenSimpleInterpolation => css`
  pointer-events: all;
  display: flex;
  flex-direction: column;
  gap: ${props.theme.spacing.fixed[2]}px;
  align-items: flex-start;
  width: 50%;

  &:only-child {
    width: 100%;
  }
`)

export const Section = styled.div((props: PDTheme): FlattenSimpleInterpolation => css`
  border-bottom: 1px solid #ccc;
  padding-bottom: ${props.theme.spacing.fixed[2]}px;
  width: 100%;
  flex-grow: 1;

  ${FormSection} {
    padding: 0;
    border-bottom: none;
  }

  &:last-child {
    border-bottom: none;
    padding-bottom: 0;
  }
`)

export const FormRow = styled.div((props: PDTheme): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[2]}px;
  display: flex;
  align-items: center;
  justify-content: flex-start;

  &:last-child {
    margin-bottom: 0;
  }
`)


export const FormLabel = styled.span((props: PDTheme): FlattenSimpleInterpolation => css`
  font-weight: 700;
  margin-right: ${props.theme.spacing.fixed[3]}px;
`)

export const ButtonGroup = styled.div((props: PDTheme): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: ${props.theme.spacing.fixed[2]}px;
  flex-wrap: wrap;
`)
