import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledProductDetailsProps {
  theme: Theme;
  backgroundImage: string
}
