import React, { ReactElement, FC, useContext, useState, useEffect } from 'react'

import RawHtmlWrapper from '@components/RawHtmlWrapper'
import ColourSwitcher from '@components/ColourSwitcher'
import Button from '@components/Button'
import QuantityPicker from '@components/QuantityPicker'
import ProductHeader from '@components/ProductHeader'
import MatchMedia from '@utils/MatchMedia'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

import * as Styled from './styles/ProductDetails.style'

import { ProductDetailsProps } from './ProductDetails.types'

const ProductDetails: FC<ProductDetailsProps> = ({
  variations,
  shortDescription,
  title,
  databaseId,
  showGallery = false,
  onSelectItem,
}: ProductDetailsProps): ReactElement => {
  const pageContext = useContext(PageContext) as PageContextType
  const [quantity, setQuantity] = useState(1)
  const [activeProduct, setActiveProduct] = useState(pageContext.productList.find(product => product.databaseId === variations[0]))
  const [basketButtons, showBasketButtons] = useState(false)

  useEffect(() => {
    if (onSelectItem) {
      onSelectItem(activeProduct)
    }
  }, [activeProduct])

  return (
    <Styled.ProductDetails>
      <If condition={showGallery}>
        <Styled.Gallery backgroundImage={activeProduct.image.sourceUrl} />
      </If>
      <Styled.Content>
        <MatchMedia breakpoint='md' andAbove>
          <Styled.Section>
            <ProductHeader
              title={title}
              activeProduct={activeProduct}
              databaseId={databaseId}
            />
          </Styled.Section>
        </MatchMedia>
        <If condition={shortDescription}>
          <Styled.Section>
            <RawHtmlWrapper content={shortDescription} />
          </Styled.Section>
        </If>
        <Styled.Section>
          <If condition={variations.length > 1}>
            <Styled.FormRow>
              <Styled.FormLabel>
                Colour:
              </Styled.FormLabel>
              <ColourSwitcher
                products={variations}
                onSelect={setActiveProduct}
                activeProductId={activeProduct.databaseId}
              />
            </Styled.FormRow>
          </If>
          <Styled.FormRow>
            <If condition={pageContext.journeyType !== 'capture'}>
              <Styled.FormLabel>
                Quantity:
              </Styled.FormLabel>
              <QuantityPicker
                value={quantity}
                increaseQuantity={() => setQuantity(quantity + 1)}
                decreaseQuantity={() => setQuantity(quantity - 1)}
              />
            </If>
          </Styled.FormRow>
        </Styled.Section>
        <If condition={pageContext.journeyType !== 'capture'}>
          <Choose>
            <When condition={basketButtons}>
              <Styled.ButtonGroup>
                <Button onInteraction={() => pageContext.toggleModal(false)} to='/checkout/customer-details' text='Checkout' />
                <Button onInteraction={() => pageContext.toggleModal(false)} to='/basket' appearance='secondary' text='View Basket' />
              </Styled.ButtonGroup>
            </When>
            <Otherwise>
              <Button onInteraction={() => { showBasketButtons(!basketButtons), pageContext.updateBasket(activeProduct, quantity)}} text='Add to Basket' />
            </Otherwise>
          </Choose>
        </If>
      </Styled.Content>
    </Styled.ProductDetails>
  )
}

export default ProductDetails
