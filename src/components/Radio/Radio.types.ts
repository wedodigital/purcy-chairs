export interface RadioProps {
  name: string
  value: string
  checked: boolean
  onChange: (event) => void
  price?: number
  title: string
}
