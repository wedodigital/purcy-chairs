import React, { FC, ReactElement } from 'react'

import Heading from '@components/Heading'
import Paragraph from '@components/Paragraph'

import * as Styled from './styles/Radio.style'

import { RadioProps } from './Radio.types'

const Radio: FC<RadioProps> = ({ value, checked, name, onChange, price, title }): ReactElement => {
  return (
    <Styled.RadioWrapper>
      <Styled.Radio
        type='radio'
        name={`${name}[0]`}
        value={value}
        checked={checked}
        onChange={(event) => onChange(event)}
      />
      <span className='checkmark'></span>
      <Styled.LabelText>
        <Heading size={1} weight={3} text={title} noMargin />
        <If condition={price}>
          <Paragraph text={`£${price}`} />
        </If>
      </Styled.LabelText>
    </Styled.RadioWrapper>
  )
}

export default Radio
