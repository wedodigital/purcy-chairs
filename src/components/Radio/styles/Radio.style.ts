import styled, { FlattenSimpleInterpolation, css } from 'styled-components'
import { Field } from 'formik'

import { StyledRadioProps } from './Radio.style.types'

export const RadioWrapper = styled.label((props: StyledRadioProps): FlattenSimpleInterpolation => css`
  display: block;
  position: relative;
  padding-left: ${props.theme.spacing.fixed[6]}px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  user-select: none;

  .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: ${props.theme.spacing.fixed[4]}px;
    width: ${props.theme.spacing.fixed[4]}px;
    background: ${props.theme.colours.tertiary};
    border-radius: 50%;
    border: 2px solid ${props.theme.colours.primary};

    &::after {
      content: "";
      position: absolute;
      display: block;
      height: ${props.theme.spacing.fixed[2]}px;
      width: ${props.theme.spacing.fixed[2]}px;
      background: ${props.theme.colours.tertiary};
      top: calc(${props.theme.spacing.fixed[1]}px - 2px);
      left: calc(${props.theme.spacing.fixed[1]}px - 2px);
      border-radius: 50%;
    }
  }

  input:checked ~ .checkmark {
    &::after {
      background: ${props.theme.colours.primary};
    }
  }
`)

export const Radio = styled(Field)((): FlattenSimpleInterpolation => css`
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
`)

export const LabelText = styled.div((): FlattenSimpleInterpolation => css`
  padding-top: 3px;
`)
