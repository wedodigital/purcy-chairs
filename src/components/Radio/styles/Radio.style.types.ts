import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledRadioProps {
  theme: Theme
}
