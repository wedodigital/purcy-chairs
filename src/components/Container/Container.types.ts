import { ReactNode } from 'react';

export interface ContainerProps {
  children: ReactNode
  width?: 'default' | 'narrow' | 'wide' | 'mid';
  verticalPadding?: boolean;
}
