import { Theme } from '@themes/purcyTheme/purcyTheme.types';

import { ContainerProps } from '../Container.types';

export interface StyledContainerProps {
  theme: Theme;
  width: ContainerProps['width'];
  verticalPadding: ContainerProps['verticalPadding'];
}
