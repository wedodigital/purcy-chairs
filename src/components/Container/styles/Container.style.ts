import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledContainerProps } from './Container.style.types'

export const Container = styled.div((props: StyledContainerProps): FlattenSimpleInterpolation => css`
  width: 90%;
  margin: 0 auto;
  max-width: 1200px;

  ${props.verticalPadding && css`
    padding: ${props.theme.spacing.fixed[8]}px 0;
  `}

  ${props.theme.mixins.respondTo.md(css`
    ${props.verticalPadding && css`
      padding: ${props.theme.spacing.fixed[10] * 2}px 0;
    `}
  `)}

  ${props.width === 'wide' && css`
    max-width: 100%;
  `}

  ${props.width === 'narrow' && css`
    max-width: 600px;
  `}

  ${props.width === 'mid' && css`
    max-width: 1600px;
  `}
`)
