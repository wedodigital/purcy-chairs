import React, { FC, ReactElement } from 'react'

import { ContainerProps } from './Container.types'

import * as Styled from './styles/Container.style'

const Container: FC<ContainerProps> = ({
  children,
  verticalPadding = false,
  width = 'default',
}: ContainerProps): ReactElement => {
  return (
    <Styled.Container
      width={width}
      verticalPadding={verticalPadding}
    >
      {children}
    </Styled.Container>
  )
}

export default Container
