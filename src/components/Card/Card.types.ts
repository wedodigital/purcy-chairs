import { ReactNode } from 'react'

export interface CardProps {
  children: ReactNode
  paddingLevel?: 1 | 2
}
