import React, { ReactElement, FC } from 'react'

import * as Styled from './styles/Card.style'

import { CardProps } from './Card.types'

const Card: FC<CardProps> = ({
  children,
  paddingLevel = 2,
}: CardProps): ReactElement => {
  return (
    <Styled.Card paddingLevel={paddingLevel}>
      {children}
    </Styled.Card>
  )
}

export default Card
