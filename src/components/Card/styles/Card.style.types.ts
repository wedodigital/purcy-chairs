import { Theme } from '@themes/purcyTheme/purcyTheme.types'

import { CardProps } from '../Card.types'

export interface StyledCardProps {
  theme: Theme;
  paddingLevel: CardProps['paddingLevel']
}
