import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledCardProps } from './Card.style.types'

export const Card = styled.div((props: StyledCardProps): FlattenSimpleInterpolation => css`
  display: flex;
  background: ${props.theme.colours.tertiary};
  padding: ${props.theme.spacing.fixed[4]}px;
  border-top: 4px solid ${props.theme.colours.primary};
  border-radius: 0 0 ${props.theme.spacing.fixed[1] / 2}px ${props.theme.spacing.fixed[1] / 2}px;

  ${props.paddingLevel === 1 && css`
    padding: ${props.theme.spacing.fixed[2]}px;
  `}
`)
