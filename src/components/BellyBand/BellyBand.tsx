import React, { FC, ReactElement } from 'react'

import Container from '@components/Container'
import Heading from '@components/Heading'

import FeaturedPublications from './components/FeaturedPublications'

import { BellyBandProps } from './BellyBand.types'

import * as Styled from './styles/BellyBand.style'

const BellyBand: FC<BellyBandProps> = ({ heading, content, image, featuredPublications }: BellyBandProps): ReactElement => {
  return (
    <Styled.BellyBand>
      <Container>
        <Styled.Columns>
          <Styled.Column>
            <img src={image} alt={heading} />
          </Styled.Column>
          <Styled.Column>
            <Heading inverse weight={2} text={heading} />
            <Heading size={1} noMargin={!featuredPublications} inverse text={content} />
            <FeaturedPublications featuredPublications={featuredPublications} />
          </Styled.Column>
        </Styled.Columns>
      </Container>
    </Styled.BellyBand>
  )
}

export default BellyBand
