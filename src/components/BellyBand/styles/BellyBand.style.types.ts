import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledBellyBandProps {
  theme: Theme
}
