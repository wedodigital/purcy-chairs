export interface FeaturedPublicationsProps {
  featuredPublications: {
    logo: {
      sourceUrl: string
    }
  }[]
}
