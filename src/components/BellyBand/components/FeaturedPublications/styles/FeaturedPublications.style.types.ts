import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledFeaturedPublicationsProps {
  theme: Theme
}
