import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledFeaturedPublicationsProps } from './FeaturedPublications.style.types'

export const FeaturedPublications = styled.ul((props: StyledFeaturedPublicationsProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  margin: -${props.theme.spacing.fixed[2]}px;

  li {
    margin: ${props.theme.spacing.fixed[2]}px;
    max-width: ${props.theme.spacing.fixed[10]}px;

    img {
      display: block;
    }
  }
`)
