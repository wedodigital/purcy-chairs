import React, { FC, ReactElement } from 'react'

import Heading from '@components/Heading'

import { FeaturedPublicationsProps } from './FeaturedPublications.types'

import * as Styled from './styles/FeaturedPublications.style'

const FeaturedPublications: FC<FeaturedPublicationsProps> = ({ featuredPublications }: FeaturedPublicationsProps): ReactElement => {
  return (
    <>
      <Heading text='As seen in:' weight={2} size={1} inverse />
      <Styled.FeaturedPublications>
        {
          featuredPublications.map((logo) => {
            return (
              <li><img src={logo.logo.sourceUrl} /></li>
            )
          })
        }
      </Styled.FeaturedPublications>
    </>
  )
}

export default FeaturedPublications
