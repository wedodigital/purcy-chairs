export interface BellyBandProps {
  heading: string
  content: string
  image: string
  featuredPublications: {
    logo: {
      sourceUrl: string
    }
  }[]
}
