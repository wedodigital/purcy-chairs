import React, { FC, ReactElement } from 'react'

import CheckoutPanel from '@components/CheckoutPanel'
import Paragraph from '@components/Paragraph'
import Button from '@components/Button'

const Confirmation: FC = ({ orderStatus }): ReactElement => {
  return (
    <Choose>
      <When condition={orderStatus === 'failed'}>
      <CheckoutPanel title={`There's been a problem.`} isValid={true}>
        <Paragraph text={`Your payment method has failed. Please try again.`} />
        <Button to='/checkout/customer-details' text='Continue' appearance='secondary' size={2} />
      </CheckoutPanel>
      </When>
      <Otherwise>
        <CheckoutPanel title='Thank you for your order.' isValid={true}>
          <Paragraph text={`Your order is processing and will be with you soon, thank you for shopping with Purcy Chairs`} />
          <Button to='/' text='Continue' appearance='secondary' size={2} />
        </CheckoutPanel>
      </Otherwise>
    </Choose>
  )
}

export default Confirmation
