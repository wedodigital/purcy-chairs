import React, { ReactElement, FC, useState } from 'react'

import Container from '@components/Container'
import Card from '@components/Card'
import ProductDetails from '@components/ProductDetails'
import MatchMedia from '@utils/MatchMedia'

import * as Styled from './styles/HeroImage.style'

import { HeroImageProps } from './HeroImage.types'

const HeroImage: FC<HeroImageProps> = ({
  mobileBackgroundImage,
  gallery,
  databaseId,
  title,
  shortDescription,
  variations,
}: HeroImageProps): ReactElement => {
  const [background, setBackground] = useState(gallery[0].sourceUrl)

  return (
    <Styled.HeroImage backgroundImage={background} mobileBackground={mobileBackgroundImage}>
      <MatchMedia breakpoint='md' andAbove>
        <Container width='wide'>
          <Styled.ProductDetails>
            <Card>
              <ProductDetails
                databaseId={databaseId}
                title={title}
                shortDescription={shortDescription}
                variations={variations}
              />
            </Card>
          </Styled.ProductDetails>
          <If condition={gallery}>
            <Card paddingLevel={1}>
              <Styled.Thumbnails>
                {
                  gallery.map((thumbnail) => {
                    return (
                      <li>
                        <Styled.Thumbnail onClick={() => setBackground(thumbnail.sourceUrl)} backgroundImage={thumbnail.sourceUrl} isActive={background === thumbnail.sourceUrl} />
                      </li>
                    )
                  })
                }
              </Styled.Thumbnails>
            </Card>
          </If>
        </Container>
      </MatchMedia>
    </Styled.HeroImage>
  )
}

export default HeroImage
