import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledHeroImageProps {
  theme: Theme;
  backgroundImage: string
  mobileBackground: string
}
