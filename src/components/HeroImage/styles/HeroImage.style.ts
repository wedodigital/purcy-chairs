import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { Container } from '@components/Container/styles/Container.style'

import { StyledHeroImageProps } from './HeroImage.style.types'

export const HeroImage = styled.div((props: StyledHeroImageProps): FlattenSimpleInterpolation => css`
  width: 100%;
  min-height: calc(80vh - 147px);
  z-index: 20;
  background: url('${props.mobileBackground}') center center no-repeat ${props.theme.colours.secondary};
  background-size: cover;

  ${props.theme.mixins.respondTo.md(css`
    background: url('${props.backgroundImage}') center center no-repeat ${props.theme.colours.secondary};
    background-size: contain;
    padding: ${props.theme.spacing.fixed[8]}px 0;
  `)}

  ${Container} {
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
  }
`)

export const ProductDetails = styled.div((): FlattenSimpleInterpolation => css`
  width: 360px;
`)

export const Thumbnails = styled.ul((props: StyledHeroImageProps): FlattenSimpleInterpolation => css`
  display: flex;
  flex-direction: column;
  gap: ${props.theme.spacing.fixed[2]}px;

  li {
    display: flex;
  }
`)

export const Thumbnail = styled.button((props: StyledHeroImageProps): FlattenSimpleInterpolation => css`
  aspect-ratio: 1 / 1;
  background: url('${props.backgroundImage}') center center no-repeat;
  background-size: cover;
  border: none;
  cursor: pointer;
  width: ${props.theme.spacing.fixed[8]}px;
  border-radius: 50%;
  position: relative;

  &::after {
    content: '';
    display: block;
    width: calc(100% + ${props.theme.spacing.fixed[1]}px);
    height: calc(100% + ${props.theme.spacing.fixed[1]}px);
    border-radius: 50%;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border: 2px solid transparent;
    transition: 0.4s all ease;
  }

  ${props.isActive && css`
    &::after {
      border-color: ${props.theme.colours.primary};
    }
  `}
`)
