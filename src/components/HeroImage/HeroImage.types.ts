export interface HeroImageProps {
  backgroundImage: string
  mobileBackgroundImage: string
  gallery?: {
    sourceUrl: string
  }[]
  databaseId: number
  title: string
  variations?: any[]
  shortDescription?: string
}
