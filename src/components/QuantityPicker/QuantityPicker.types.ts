export interface QuantityPickerProps {
  value: number
  increaseQuantity: () => void
  decreaseQuantity: () => void
}
