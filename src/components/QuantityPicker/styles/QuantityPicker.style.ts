import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledQuantityPickerProps } from './QuantityPicker.style.types'

export const QuantityPicker = styled.div((props: StyledQuantityPickerProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;

  & > * {
    margin-right: ${props.theme.spacing.fixed[2]}px;

    &:last-child {
      margin-right: 0;
    }
  }
`)

export const IncrementButton = styled.button((props: StyledQuantityPickerProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.primary};
  color: ${props.theme.colours.tertiary};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  text-decoration: none;
  font-weight: 700;
  border-radius: 40px;
  border: none;
  cursor: pointer;
  font-size: 24px;
  line-height: 24px;
  font-family: ${props.theme.typography.fontFamily};
  width: ${props.theme.spacing.fixed[4]}px;
  height: ${props.theme.spacing.fixed[4]}px;

  & > svg {
    width: ${props.theme.spacing.fixed[2]}px;
    height: ${props.theme.spacing.fixed[2]}px;

    * {
      fill: ${props.theme.colours.tertiary};
    }
  }
`)

export const Quantity = styled.span((): FlattenSimpleInterpolation => css`
  font-weight: 700;
`)
