import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledQuantityPickerProps {
  theme: Theme;
}
