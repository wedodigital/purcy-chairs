import React, { FC, ReactElement } from 'react'

import Increase from '@assets/increase.svg'
import Decrease from '@assets/decrease.svg'

import * as Styled from './styles/QuantityPicker.style'

import { QuantityPickerProps } from './QuantityPicker.types'

const QuantityPicker: FC<QuantityPickerProps> = ({
  value,
  increaseQuantity,
  decreaseQuantity,
}: QuantityPickerProps): ReactElement => {
  return (
    <Styled.QuantityPicker>
      <Styled.IncrementButton onClick={value > 1 ? () => decreaseQuantity() : () => console.log()}><Decrease /></Styled.IncrementButton>
      <Styled.Quantity>{value}</Styled.Quantity>
      <Styled.IncrementButton onClick={() => increaseQuantity()}><Increase /></Styled.IncrementButton>
    </Styled.QuantityPicker>
  )
}

export default QuantityPicker
