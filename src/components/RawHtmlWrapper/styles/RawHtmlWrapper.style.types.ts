import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledRawHtmlWrapperProps {
  theme: Theme
  inverse: boolean
}
