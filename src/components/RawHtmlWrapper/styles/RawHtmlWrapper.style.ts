import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledRawHtmlWrapperProps } from './RawHtmlWrapper.style.types'

export const RawHtmlWrapper = styled.div((props: StyledRawHtmlWrapperProps): FlattenSimpleInterpolation => css`
  ${props.inverse && css`
    color: ${props.theme.colours.tertiary};
  `}

  h3 {
    font-weight: bold;
    margin-bottom: ${props.theme.spacing.fixed[2]}px;

    &:last-child {
      margin-bottom: 0;
    }
  }

  p {
    font-size: ${props.theme.typography.paragraph[3].fontSize};
    line-height: ${props.theme.typography.paragraph[3].lineHeight};
    margin-bottom: ${props.theme.spacing.fixed[2]}px;

    &:last-child {
      margin-bottom: 0;
    }
  }

  ul {
    list-style: disc;
    margin: 0 0 ${props.theme.spacing.fixed[2]}px ${props.theme.spacing.fixed[4]}px;

    li {
      margin-bottom: ${props.theme.spacing.fixed[1]}px;

      &:last-child {
        margin-bottom: 0;
      }
    }

    &:last-child {
      margin-bottom: 0;
    }
  }
`)
