export interface CheckoutPanelProps {
  title: string
  children: React.ReactNode
  loading?: boolean
  isValid?: boolean
  errorDetails?: string
}
