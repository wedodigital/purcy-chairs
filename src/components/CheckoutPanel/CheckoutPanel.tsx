import React, { FC, ReactElement } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner, faExclamationCircle } from '@fortawesome/free-solid-svg-icons'

import Heading from '@components/Heading'
import Paragraph from '@components/Paragraph'

import * as Styled from './styles/CheckoutPanel.style'

import { CheckoutPanelProps } from './CheckoutPanel.types'

const CheckoutPanel: FC<CheckoutPanelProps> = ({ title, children, loading, isValid, errorDetails }: CheckoutPanelProps): ReactElement => {
  return (
    <Styled.CheckoutPanel>
      <If condition={loading}>
        <Styled.LoadingBlock>
          <FontAwesomeIcon icon={faSpinner} spin />
        </Styled.LoadingBlock>
      </If>
      <Heading weight={3} size={3} text={title} />
      <If condition={!isValid}>
        <Styled.Error>
          <Styled.ErrorHeading>
            <FontAwesomeIcon icon={faExclamationCircle} />
            <Heading inverse text='There were errors in your form submission' size={1} weight={3} />
          </Styled.ErrorHeading>
          <If condition={errorDetails}>
            <Paragraph text={errorDetails} />
          </If>
        </Styled.Error>
      </If>
      {children}
    </Styled.CheckoutPanel>
  )
}

export default CheckoutPanel
