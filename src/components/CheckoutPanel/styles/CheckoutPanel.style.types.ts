import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledCheckoutPanelProps {
  theme: Theme
}
