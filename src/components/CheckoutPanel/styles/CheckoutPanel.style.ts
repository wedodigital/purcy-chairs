import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledCheckoutPanelProps } from './CheckoutPanel.style.types'

export const CheckoutPanel = styled.div((props: StyledCheckoutPanelProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[4]}px;
  margin: ${props.theme.spacing.fixed[4]}px 0;
  background: ${props.theme.colours.tertiary};
  border-radius: 0 0 ${props.theme.spacing.fixed[1] / 2}px ${props.theme.spacing.fixed[1] / 2}px;
  border-top: 4px solid ${props.theme.colours.primary};
  position: relative;

  ${props.theme.mixins.respondTo.md(css`
    padding: ${props.theme.spacing.fixed[4]}px ${props.theme.spacing.fixed[8]}px;
    margin: ${props.theme.spacing.fixed[8]}px -${props.theme.spacing.fixed[10]}px;
  `)}

  label {
    display: block;
    font-weight: 700;
    margin-bottom: ${props.theme.spacing.fixed[2]}px;
  }

  input,
  select {
    width: 100%;
    font-size: ${props.theme.typography.paragraph[3].fontSize};
    line-height: ${props.theme.typography.paragraph[3].lineHeight};
    font-family: aktiv-grotesk, sans-serif;
    color: ${props.theme.colours.primary};
    margin-bottom: ${props.theme.spacing.fixed[4]}px;
    padding: ${props.theme.spacing.fixed[1]}px ${props.theme.spacing.fixed[2]}px;
    border: 1px solid transparent;
    border-bottom: 1px solid ${props.theme.colours.primary};
    transition: 0.4s all ease;

    &:focus {
      outline: none;
      border: 1px solid ${props.theme.colours.primary};
    }
  }

  .copy-button {
    margin-bottom: ${props.theme.spacing.fixed[4]}px;
  }
`)

export const LoadingBlock = styled.div((props: StyledCheckoutPanelProps) => css`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background: rgba(255, 255, 255, 0.8);
  font-size: ${props.theme.typography.heading[4].fontSize};
  z-index: 10;
`)

export const ErrorHeading = styled.div((props: StyledCheckoutPanelProps) => css`
  padding: ${props.theme.spacing.fixed[1]}px ${props.theme.spacing.fixed[2]}px;
  margin: -${props.theme.spacing.fixed[1]}px -${props.theme.spacing.fixed[2]}px ${props.theme.spacing.fixed[1]}px;
  background: ${props.theme.colours.primary};
  color: ${props.theme.colours.tertiary};
  display: flex;
  gap: ${props.theme.spacing.fixed[2]}px;
  align-items: center;
`)

export const Error = styled.div((props: StyledCheckoutPanelProps) => css`
  padding: ${props.theme.spacing.fixed[1]}px ${props.theme.spacing.fixed[2]}px;
  border: 2px solid ${props.theme.colours.primary};
  margin-bottom: ${props.theme.spacing.fixed[4]}px;
`)
