import React, { FC, ReactElement } from 'react'

import Paragraph from '@components/Paragraph'

import * as Styled from './styles/Legals.style'

const Legals: FC = (): ReactElement => {
  return (
    <Styled.Legals>
      <Paragraph
        size={1}
        text='Purcy Chair Ltd | Company Number: 12595173 | PO Box 6945, Office 63024, London W1A 6US'
      />
      <Paragraph
        size={1}
        text='Purcy Chair Ltd is authorised and regulated by the Financial Conduct Authority (FCA FRN 953679) and acts as a credit intermediary and not a lender, offering credit products provided by a limited number of finance providers, including Klarna Bank AB (publ). Finance is only available to permanent UK residents aged 18+, subject to status, terms and conditions apply. Purcy Chair Co., PO BOX 6945, Office 63024, London, W1A 6US.'
      />
    </Styled.Legals>
  )
}

export default Legals
