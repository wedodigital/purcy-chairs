import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledLegalsProps {
  theme: Theme
}
