import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledLegalsProps } from './Legals.style.types'

export const Legals = styled.div((props: StyledLegalsProps): FlattenSimpleInterpolation => css`
  text-align: center;
  padding: 0 ${props.theme.spacing.fixed[2]}px ${props.theme.spacing.fixed[2]}px;
  max-width: 800px;
  margin: 0 auto;

  ${props.theme.mixins.respondTo.md(css`
    padding: 0 ${props.theme.spacing.fixed[4]}px ${props.theme.spacing.fixed[4]}px;
  `)}
`)
