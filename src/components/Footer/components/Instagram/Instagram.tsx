import React, { FC, ReactElement } from 'react'
import InstagramFeed  from 'react-ig-feed'

import Container from '@components/Container'
import MatchMedia from '@utils/MatchMedia'

import * as Styled from './styles/Instagram.style'

const Instagram: FC = (): ReactElement => {
  return (
    <Styled.Instagram>
      <Container width='mid'>
        <MatchMedia breakpoint='sm'>
          <InstagramFeed
            token='IGQVJVUnZANUmdnTENDSGJ6emJvZAlF0em9iWWpjQkZAWTGhuRkViN2NtR3hDaDl6cmJnWk1JS3pVNUQxdlJIX3dnV3V5eFZAMdDZAseHNfdklMMV9mV3p4anJmRUhpd3BWNHhERHJuSDVRbzNfRnkwN0o3TQZDZD'
            counter='4'
          />
        </MatchMedia>
        <MatchMedia breakpoint='md' andAbove>
          <InstagramFeed
            token='IGQVJVUnZANUmdnTENDSGJ6emJvZAlF0em9iWWpjQkZAWTGhuRkViN2NtR3hDaDl6cmJnWk1JS3pVNUQxdlJIX3dnV3V5eFZAMdDZAseHNfdklMMV9mV3p4anJmRUhpd3BWNHhERHJuSDVRbzNfRnkwN0o3TQZDZD'
            counter='8'
          />
        </MatchMedia>
      </Container>
    </Styled.Instagram>
  )
}

export default Instagram
