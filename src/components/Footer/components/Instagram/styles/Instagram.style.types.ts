import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledInstagramProps {
  theme: Theme
}
