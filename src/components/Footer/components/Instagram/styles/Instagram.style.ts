import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledInstagramProps } from './Instagram.style.types'

export const Instagram = styled.div((props: StyledInstagramProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[10]}px 0 0;

  [class*=Container] {
    & > div {
      display: flex;
      align-items: center;
      flex-wrap: wrap;
      margin: -${props.theme.spacing.fixed[2]}px -${props.theme.spacing.fixed[2]}px 0;

      & > div {
        height: 0;
        width: 50%;
        padding-bottom: 50%;
        position: relative;

        ${props.theme.mixins.respondTo.md(css`
          width: 25%;
          padding-bottom: 25%;
        `)}

        a {
          display: block;
          position: absolute;
          top: ${props.theme.spacing.fixed[2]}px;
          left: ${props.theme.spacing.fixed[2]}px;
          right: ${props.theme.spacing.fixed[2]}px;
          bottom: ${props.theme.spacing.fixed[2]}px;
          overflow: hidden;
        }
      }
    }
  }
`)
