import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledCopyrightProps {
  theme: Theme
}
