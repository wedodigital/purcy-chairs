import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledCopyrightProps } from './Copyright.style.types'

export const Copyright = styled.footer((props: StyledCopyrightProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[2]}px 0;

  ${props.theme.mixins.respondTo.md(css`
    padding: ${props.theme.spacing.fixed[6]}px 0;
  `)}

  [class*=Container] {
    display: flex;
    flex-direction: column;
    align-items: stretch;

    ${props.theme.mixins.respondTo.md(css`
      flex-direction: row;
      justify-content: space-between;
    `)}
  }
`)

export const CopyrightInfo = styled.div((props: StyledCopyrightProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;

  span {
    display: inline-block;
    margin-right: ${props.theme.spacing.fixed[1]}px;
  }
`)

export const SocialNav = styled.ul((props: StyledCopyrightProps): FlattenSimpleInterpolation => css`
  display: flex;
  justify-content: flex-end;
  margin-left: auto;

  li {
    margin-right: ${props.theme.spacing.fixed[4]}px;

    a {
      color: ${props.theme.colours.primary};
      text-decoration: none;
    }

    &:last-child {
      margin-right: 0;
    }
  }
`)

export const KlarnaLogo = styled.div((): FlattenSimpleInterpolation => css`
  margin-left: auto;

  svg {
    width: 72px;
    height: 16px;
  }
`)
