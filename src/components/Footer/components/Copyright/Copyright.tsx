import React, { FC, ReactElement } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import dayjs from 'dayjs'

import Container from '@components/Container'

import MatchMedia from '@utils/MatchMedia'

import * as Styled from './styles/Copyright.style'

const Copyright: FC = (): ReactElement => {
  const legals = useStaticQuery(graphql`
    query Legals {
      wpPage(databaseId: {eq: 2}) {
        homepage {
          privacyPolicy {
            mediaItemUrl
          }
          termsConditions {
            mediaItemUrl
          }
        }
      }
    }
  `)

  const privacyPolicy = legals.wpPage.homepage.privacyPolicy?.mediaItemUrl
  const termsConditions = legals.wpPage.homepage.termsConditions?.mediaItemUrl

  return (
    <Styled.Copyright>
      <Container>
        <Styled.CopyrightInfo>
          <span>&copy; {dayjs().year()} Purcy Chair Co.</span>
        </Styled.CopyrightInfo>
        <MatchMedia breakpoint='md' andAbove>
          <Styled.SocialNav>
            <If condition={privacyPolicy}>
              <li><a href={privacyPolicy} target="_blank">Privacy Policy</a></li>
            </If>
            <If condition={termsConditions}>
              <li><a href={termsConditions} target="_blank">Terms & Conditions</a></li>
            </If>
            <li><a href="https://www.instagram.com/purcy.chair" target="_blank">Instagram</a></li>
            <li><a href="http://www.facebook.com/purcychair" target="_blank">Facebook</a></li>
          </Styled.SocialNav>
        </MatchMedia>
      </Container>
    </Styled.Copyright>
  )
}

export default Copyright
