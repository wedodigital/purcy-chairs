import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledNewsletterProps {
  theme: Theme
}
