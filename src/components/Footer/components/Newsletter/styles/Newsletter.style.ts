import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledNewsletterProps } from './Newsletter.style.types'

export const Newsletter = styled.div((props: StyledNewsletterProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.primary};
  padding: ${props.theme.spacing.fixed[4]}px 0;

  ${props.theme.mixins.respondTo.md(css`
    padding: ${props.theme.spacing.fixed[8]}px 0;
  `)}
`)
