import React, { FC, ReactElement } from 'react'

import Container from '@components/Container'
import EmailSignup from '@components/EmailSignup'

import { NewsletterProps } from './Newsletter.types'

import * as Styled from './styles/Newsletter.style'

const Newsletter: FC<NewsletterProps> = ({ formEndpoint }: NewsletterProps): ReactElement => {
  return (
    <Styled.Newsletter>
      <Container>
        <EmailSignup columns={2} inverse endpoint={formEndpoint} />
      </Container>
    </Styled.Newsletter>
  )
}

export default Newsletter
