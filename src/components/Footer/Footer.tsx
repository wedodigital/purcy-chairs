import React, { FC, ReactElement } from 'react'

import Newsletter from './components/Newsletter'
import Copyright from './components/Copyright'
import Legals from './components/Legals'

import { FooterProps } from './Footer.types'

import * as Styled from './styles/Footer.style'

const Footer: FC<FooterProps> = ({ showNewsletter = true }: FooterProps): ReactElement => {
  return (
    <Styled.Footer>
      <If condition={showNewsletter}>
        <Newsletter />
      </If>
      <Copyright />
      <Legals />
    </Styled.Footer>
  )
}

export default Footer
