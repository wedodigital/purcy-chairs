import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledFooterProps } from './Footer.style.types'

export const Footer = styled.footer((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  margin-top: ${props.theme.spacing.fixed[10]}px;
`)
