import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledFooterProps {
  theme: Theme
  showTray: boolean
}
