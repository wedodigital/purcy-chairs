import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'
import Container from '@components/Container'
import Button from '@components/Button'
import Paragraph from '@components/Paragraph'

import * as Styled from './styles/ThankYou.style'

import { ThankYouProps } from './ThankYou.types'

const ThankYou: FC<ThankYouProps> = ({
  // add props
}: ThankYouProps): ReactElement => {
  return (
    <Container width='narrow'>
      <Styled.ThankYou>
        <Heading size={1} weight={3} text='Thank you, we have received your email. A member of the Purcy team will reach out to you soon!' />
        <Button text='Return to Shop' to='/shop' />
        <Paragraph text='If you did not intend to subscribe, please ignore our confirmation email.' />
      </Styled.ThankYou>
    </Container>
  )
}

export default ThankYou
