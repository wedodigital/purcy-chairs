import { ReactNode } from 'react'

export interface ThankYouProps {
  /**
   * React children
   * */
  children: ReactNode
}
