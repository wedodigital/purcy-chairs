import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledThankYouProps {
  theme: Theme;
}
