import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledThankYouProps } from './ThankYou.style.types'

export const ThankYou = styled.div((props: StyledThankYouProps): FlattenSimpleInterpolation => css`
  margin: ${props.theme.spacing.fixed[10]}px auto;
  background: ${props.theme.colours.tertiary};
  padding: ${props.theme.spacing.fixed[4]}px;
  text-align: center;
  border: 2px solid ${props.theme.colours.primary};

  * {
    margin-bottom: ${props.theme.spacing.fixed[2]}px;

    &:last-child {
      margin-bottom: 0;
    }
  }
`)
