import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledStepperProps } from './Stepper.style.types'

export const Stepper = styled.div((props: StyledStepperProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  position: relative;

  &::after {
    content: '';
    display: block;
    width: 100%;
    height: 2px;
    background: ${props.theme.colours.primary};
    position: absolute;
    top: 50%;
    margin-top: -1px;
    left: 0;
  }
`)

export const Step = styled.div((props: StyledStepperProps): FlattenSimpleInterpolation => css`
  border: 2px solid ${props.theme.colours.primary};
  font-weight: 700;
  width: ${props.theme.spacing.fixed[4]}px;
  height: ${props.theme.spacing.fixed[4]}px;
  line-height: ${props.theme.spacing.fixed[1] * 3.5}px;
  text-align: center;
  background: ${props.theme.colours.secondary};
  z-index: 1;
  transition: 0.4s all ease;
  border-radius: 50%;

  ${props.theme.mixins.respondTo.md(css`
    font-size: ${props.theme.typography.heading[3].fontSize};
    line-height: ${props.theme.spacing.fixed[6]}px;
    width: ${props.theme.spacing.fixed[7]}px;
    height: ${props.theme.spacing.fixed[7]}px;
  `)};

  ${props.isCurrentStep && css`
    background: ${props.theme.colours.primary};
    color: ${props.theme.colours.tertiary};
  `}
`)
