import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledStepperProps {
  theme: Theme
  isCurrentStep: boolean
}
