import React, { FC, ReactElement } from 'react'

import * as Styled from './styles/Stepper.style'

import { StepperProps } from './Stepper.types'

const Stepper: FC<StepperProps> = ({ currentStep, totalSteps }) => {
  const indexes = Array.from({length: totalSteps}, (_, i) => i + 1)
  return (
    <Styled.Stepper>
      {
        indexes.map((index) => {
          return (
            <Styled.Step key={index} isCurrentStep={currentStep === index}>{index}</Styled.Step>
          )
        })
      }
    </Styled.Stepper>
  )
}

export default Stepper
