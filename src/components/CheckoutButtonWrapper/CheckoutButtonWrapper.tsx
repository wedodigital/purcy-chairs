import React, { FC, ReactElement } from 'react'

import * as Styled from './styles/CheckoutButtonWrapper.style'

const CheckoutButtonWrapper: FC = ({ children }): ReactElement => {
  return (
    <Styled.CheckoutButtonWrapper>
      {children}
    </Styled.CheckoutButtonWrapper>
  )
}

export default CheckoutButtonWrapper
