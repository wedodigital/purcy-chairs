import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

export const CheckoutButtonWrapper = styled.div((): FlattenSimpleInterpolation => css`
  display: flex;
  justify-content: center;
`)
