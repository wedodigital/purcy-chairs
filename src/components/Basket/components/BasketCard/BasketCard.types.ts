import { ProductDetails } from '@context/PageContext.types'

export interface BasketCardProps {
  product: ProductDetails
  quantity: number
}
