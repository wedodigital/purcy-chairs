import React, { FC, ReactElement, useContext } from 'react'

import Heading from '@components/Heading'
import Button from '@components/Button'
import QuantityPicker from '@components/QuantityPicker'
import TotalPrice from '@components/TotalPrice'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

import * as Styled from './styles/BasketCard.style'

import { BasketCardProps } from './BasketCard.types'

const BasketCard: FC<BasketCardProps> = ({product, quantity}: BasketCardProps): ReactElement => {
  const context = useContext(PageContext) as PageContextType
  return (
    <Styled.BasketCard>
      <Styled.ImageContainer>
        <Styled.Image backgroundImage={product.image.sourceUrl} />
      </Styled.ImageContainer>
      <Styled.Details>
        <div>
          <Styled.FormSection>
            <Heading weight={3} text={product.name} />
          </Styled.FormSection>
          <Styled.FormSection>
            <Styled.FormRow>
              <Styled.FormLabel>
                Quantity:
              </Styled.FormLabel>
              <QuantityPicker value={quantity} increaseQuantity={() => context.amendQuantity(product, 'increase')} decreaseQuantity={() => context.amendQuantity(product, 'decrease')} />
            </Styled.FormRow>
            <TotalPrice
              title='Subtotal'
              regularPrice={product.regularPrice}
              salePrice={product.salePrice}
              onSale={product.onSale}
              quantity={quantity}
            />
          </Styled.FormSection>
          <Styled.FormSection>
            <Button appearance='secondary' text='Remove' onInteraction={() => context.removeFromBasket(product)} size={1} />
          </Styled.FormSection>
        </div>
      </Styled.Details>
    </Styled.BasketCard>
  )
}

export default BasketCard
