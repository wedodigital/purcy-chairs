import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledBasketCardProps {
  theme: Theme
  backgroundImage: string
}
