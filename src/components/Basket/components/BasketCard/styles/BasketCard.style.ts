import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledBasketCardProps } from './BasketCard.style.types'

export const BasketCard = styled.div((props: StyledBasketCardProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[3]}px;
  background: ${props.theme.colours.tertiary};
  display: flex;
  border-top: 4px solid ${props.theme.colours.primary};
  border-radius: 0 0 ${props.theme.spacing.fixed[1] / 2}px ${props.theme.spacing.fixed[1] / 2}px;
  flex-direction: column;
  margin: 0 0 ${props.theme.spacing.fixed[4]}px;

  ${props.theme.mixins.respondTo.md(css`
    padding: ${props.theme.spacing.fixed[3]}px 0;
    flex-direction: row;
    margin: 0 -${props.theme.spacing.fixed[10]}px ${props.theme.spacing.fixed[4]}px;
  `)}

  &:last-child {
    margin-bottom: 0;
  }
`)

export const ImageContainer = styled.div((props: StyledBasketCardProps): FlattenSimpleInterpolation => css`
  border-bottom: 1px solid ${props.theme.colours.primary};
  padding: 0 0 ${props.theme.spacing.fixed[3]}px;

  ${props.theme.mixins.respondTo.md(css`
    width: 35%;
    border-right: 1px solid ${props.theme.colours.primary};
    border-bottom: none;
    padding: 0 ${props.theme.spacing.fixed[3]}px;
  `)}
`)

export const Image = styled.div((props: StyledBasketCardProps): FlattenSimpleInterpolation => css`
  background: url('${props.backgroundImage}') center center no-repeat;
  background-size: cover;
  aspect-ratio: 1 / 1;
`)

export const Details = styled.div((props: StyledBasketCardProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[3]}px 0;

  ${props.theme.mixins.respondTo.md(css`
    width: 65%;
    padding: ${props.theme.spacing.fixed[3]}px;
    display: flex;
    align-items: center;
  `)}
`)

export const FormRow = styled.div((props: StyledBasketCardProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  justify-content: flex-start;

  &:last-child {
    margin-bottom: 0;
  }
`)

export const FormSection = styled.div((props: StyledBasketCardProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[3]}px 0;
  border-bottom: 1px solid ${props.theme.colours.secondary};

  &:first-child {
    padding-top: 0;
  }

  &:last-child {
    padding-bottom: 0;
    border-bottom: none;
  }
`)

export const FormLabel = styled.span((props: StyledBasketCardProps): FlattenSimpleInterpolation => css`
  font-weight: 700;
  margin-right: ${props.theme.spacing.fixed[3]}px;
`)
