import React, { FC, ReactElement, useContext} from 'react'

import Heading from '@components/Heading'
import Button from '@components/Button'
import Container from '@components/Container'

import BasketCard from './components/BasketCard'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

import * as Styled from './styles/Basket.style'

const Basket: FC = (): ReactElement => {
  const context = useContext(PageContext) as PageContextType
  return (
    <Styled.Basket>
      <Choose>
        <When condition={context.basket.products.length}>
          <Container width='narrow' verticalPadding>
            <Heading weight={3} size={5} text='Your Basket' />
            {
              context.basket.products.map((basketItem) => {
                if (basketItem.product.status === 'private') return
                return (
                  <BasketCard
                    key={basketItem.product.databaseId}
                    product={basketItem.product}
                    quantity={basketItem.quantity}
                  />
                )
              })
            }
            <Styled.BasketButton>
              <Button to='/checkout/customer-details' text='Continue to Checkout' size={3} />
            </Styled.BasketButton>
          </Container>
        </When>
        <Otherwise>
          <Container verticalPadding>
            <Heading weight={3} size={5} text='Your Basket is empty' />
            <Button to='/shop' text='Shop Now' />
          </Container>
        </Otherwise>
      </Choose>
    </Styled.Basket>
  )
}

export default Basket
