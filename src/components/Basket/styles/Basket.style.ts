import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

export const Basket = styled.div((): FlattenSimpleInterpolation => [])

export const BasketButton = styled.div((): FlattenSimpleInterpolation => css`
  display: flex;
  flex-direction: column;
  align-items: center;
`)
