import React, { FC, ReactElement } from 'react'

import Container from '@components/Container'
import Heading from '@components/Heading'

import Star from '@assets/star.svg'

import * as Styled from './styles/AverageRating.style'

import { AverageRatingProps } from './AverageRating.types'

const AverageRating: FC<AverageRatingProps> = ({ averageRating }: AverageRatingProps): ReactElement => {
  const ratingPercentage = ((averageRating / 5) * 100).toFixed(2)

  return (
    <Styled.AverageRating>
      <Container width='wide'>
        <Styled.RatingBackground>
          <Styled.Rating ratingPercentage={ratingPercentage}>
            <Star />
            <Star />
            <Star />
            <Star />
            <Star />
          </Styled.Rating>
          <Star />
          <Star />
          <Star />
          <Star />
          <Star />
        </Styled.RatingBackground>
        <Heading text={averageRating.toString()} size={1} noMargin weight={2} />
      </Container>
    </Styled.AverageRating>
  )
}

export default AverageRating
