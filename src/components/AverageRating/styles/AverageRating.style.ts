import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledAverageRatingProps } from './AverageRating.style.types'

export const AverageRating = styled.div((props: Pick<StyledAverageRatingProps, 'theme'>): FlattenSimpleInterpolation => css`
  position: absolute;
  top: 160px;
  left: 0;
  width: 100%;
  z-index: 10;

  [class*='Container'] {
    display: flex;
    align-items: center;
  }
`)

export const RatingBackground = styled.div((props: Pick<StyledAverageRatingProps, 'theme'>): FlattenSimpleInterpolation => css`
  position: relative;
  width: ${props.theme.spacing.fixed[3] * 5}px;
  height: ${props.theme.spacing.fixed[3]}px;
  display: flex;
  margin-right: ${props.theme.spacing.fixed[2]}px;

  svg {
    width: 100%;
    height: 100%;

    * {
      fill: ${props.theme.colours.tertiary};
    }
  }
`)

export const Rating = styled.div((props: StyledAverageRatingProps): FlattenSimpleInterpolation => css`
  width: ${props.ratingPercentage}%;
  height: 100%;
  overflow: hidden;
  position: absolute;
  top: 0;
  left: 0;
  display: flex;

  svg {
    width: ${props.theme.spacing.fixed[3]}px;
    height: ${props.theme.spacing.fixed[3]}px;
    flex-shrink: 0;

    * {
      fill: #D1A364;
    }
  }
`)
