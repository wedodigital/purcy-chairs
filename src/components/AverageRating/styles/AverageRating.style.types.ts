import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledAverageRatingProps {
  theme: Theme
  ratingPercentage: number
}
