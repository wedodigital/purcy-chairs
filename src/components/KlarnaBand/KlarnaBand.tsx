import React, { FC, ReactElement, useContext } from 'react'

import Klarna from '@assets/klarna.svg'

import Container from '@components/Container'
import RawHtmlWrapper from '@components/RawHtmlWrapper'

import PageContext from '@context/PageContext'
import { PageContextType, ProductDetails } from '@context/PageContext.types'

import { KlarnaBandProps } from './KlarnaBand.types'

import * as Styled from './styles/KlarnaBand.style'

const KlarnaBand: FC<KlarnaBandProps> = ({ klarnaBreakdown }: KlarnaBandProps): ReactElement => {
  const context = useContext(PageContext) as PageContextType
  const fullPriceBreakdown = `<h3>Pay in full at £1495 (no interest added, including VAT).</h3>
<p>Or spread the cost to 6, 12 or 36 easy instalments with Klarna</p>
<ul>
<li>6 months = £249.16 a month</li>
<li>12 months = £124.68 a month</li>
<li>36 months = £41.52 a month</li>
</ul>`

  return (
    <Styled.KlarnaBand>
      <Container>
        <Styled.Columns>
          <Styled.Column>
            <Klarna />
          </Styled.Column>
          <Styled.Column>
            <RawHtmlWrapper content={context.journeyType === 'capture' ? fullPriceBreakdown : klarnaBreakdown} inverse />
          </Styled.Column>
        </Styled.Columns>
      </Container>
    </Styled.KlarnaBand>
  )
}

export default KlarnaBand
