import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledKlarnaBandProps } from './KlarnaBand.style.types'

export const KlarnaBand = styled.div((props: StyledKlarnaBandProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.primary};
  padding: ${props.theme.spacing.fixed[6]}px 0;
  margin-bottom: ${props.theme.spacing.fixed[6]}px;

  ${props.theme.mixins.respondTo.md(css`
    padding: ${props.theme.spacing.fixed[10]}px 0;
    margin-bottom: ${props.theme.spacing.fixed[10]}px;
  `)}

  [class*=Container] {
    max-width: 800px;
  }
`)

export const Columns = styled.div((props: StyledKlarnaBandProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: stretch;
  flex-direction: column;

  ${props.theme.mixins.respondTo.md(css`
    align-items: center;
    flex-direction: row;
  `)}

  svg {
    & > * {
      fill: ${props.theme.colours.tertiary}
    }
  }
`)

export const Column = styled.div((props: StyledKlarnaBandProps): FlattenSimpleInterpolation => css`
  width: 100%;
  padding: ${props.theme.spacing.fixed[2]}px 0;

  &:first-child {
    padding-top: 0;
  }

  &:last-child {
    padding-bottom: 0;
  }

  img {
    display: block;
  }

  ${props.theme.mixins.respondTo.md(css`
    padding: 0 ${props.theme.spacing.fixed[4]}px;

    &:first-child {
      padding-left: 0;
    }

    &:last-child {
      padding-right: 0;
    }
  `)}
`)
