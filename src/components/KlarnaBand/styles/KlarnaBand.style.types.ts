import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledKlarnaBandProps {
  theme: Theme
}
