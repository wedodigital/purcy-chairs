import { Theme } from '@themes/purcyTheme/purcyTheme.types'

import { SplitContentProps } from '../SplitContent.types'

export interface StyledSplitContentProps {
  theme: Theme
  orientation: SplitContentProps['orientation']
  backgroundImage: string
}
