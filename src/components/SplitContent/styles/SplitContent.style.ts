import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledSplitContentProps } from './SplitContent.style.types'

export const SplitContent = styled.div((props: StyledSplitContentProps): FlattenSimpleInterpolation => css`
  text-align: left;

  & > [class*=Container] {
    display: flex;
    align-items: center;
    flex-direction: column-reverse;
    padding: ${props.theme.spacing.fixed[4]}px 0;
    border-bottom: 2px solid ${props.theme.colours.quartenary};

    ${props.theme.mixins.respondTo.md(css`
      flex-direction: row;
      border-bottom: 0;
      padding: ${props.theme.spacing.fixed[8]}px 0;

      ${props.orientation === 'contentRight' && css`
        flex-direction: row-reverse;
      `}
    `)}
  }
`)

export const Content = styled.div((props: StyledSplitContentProps): FlattenSimpleInterpolation => css`
  ${props.theme.mixins.respondTo.md(css`
    width: 56%;
    padding: 0 ${props.theme.spacing.fixed[8] * 2}px 0 ${props.theme.spacing.fixed[8]}px;
    flex-shrink: 0;

    ${props.orientation === 'contentRight' && css`
      padding: 0 ${props.theme.spacing.fixed[8]}px 0 ${props.theme.spacing.fixed[8] * 2}px;
    `}
  `)}
`)

export const Image = styled.div((props: StyledSplitContentProps): FlattenSimpleInterpolation => css`
  background: url(${props.backgroundImage}) center center no-repeat;
  background-size: cover;
  width: 100%;
  height: 0;
  padding-bottom: 60%;
  margin-bottom: ${props.theme.spacing.fixed[4]}px;

  ${props.theme.mixins.respondTo.md(css`
    height: 55vh;
    width: 46vw;
    flex-shrink: 0;
    padding-bottom: 0;
  `)}
`)
