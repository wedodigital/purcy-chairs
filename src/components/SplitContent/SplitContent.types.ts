export interface SplitContentProps {
  orientation: 'contentLeft' | 'contentRight'
  content: string
  title: string
  image: string
}
