import React, { FC, ReactElement } from 'react'

import Container from '@components/Container'
import Heading from '@components/Heading'
import RawHtmlWrapper from '@components/RawHtmlWrapper'

import * as Styled from './styles/SplitContent.style'

import { SplitContentProps } from './SplitContent.types'

const SplitContent: FC<SplitContentProps> = ({
  orientation,
  title,
  content,
  image,
}: SplitContentProps): ReactElement => {
  return (
    <Styled.SplitContent orientation={orientation}>
      <Container width='wide'>
        <Styled.Content orientation={orientation}>
          <Heading weight={3} text={title} />
          <RawHtmlWrapper content={content} />
        </Styled.Content>
        <Styled.Image backgroundImage={image} />
      </Container>
    </Styled.SplitContent>
  )
}

export default SplitContent
