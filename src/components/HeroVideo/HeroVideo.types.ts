type VideoFormats = 'mp4' | 'webm'

export interface HeroVideoProps {
  videoSrc: {
    [P in VideoFormats]?: string
  }
  title: string
  poster?: string 
  featureImage?: {
    sourceUrl: string
  }
}
