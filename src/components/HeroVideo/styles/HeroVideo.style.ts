import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledHeroVideoProps } from './HeroVideo.style.types'

export const HeroVideo = styled.div((props: StyledHeroVideoProps): FlattenSimpleInterpolation => css`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-image: url('${props.backgroundImage}');
  background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
  z-index: 1;

  video {
    position: fixed;
		top: 50%;
		left: 50%;
		min-width: 100%;
		min-height: 100%;
		width: auto;
		height: auto;
		z-index: -100;
		-webkit-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
		background-size: cover;
  }
`)

export const VideoOverlay = styled.div((): FlattenSimpleInterpolation => css`
  position: fixed;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
  background: rgba(0, 0, 0, 0.4);
`)

export const Title = styled.div((): FlattenSimpleInterpolation => css`
  position: fixed;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 20;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  text-align: center;

  [class*=Heading] {
    width: 100%;
  }
`)

export const Content = styled.div((props: StyledHeroVideoProps): FlattenSimpleInterpolation => css`
  position: relative;

  /* & > [class*=Button] {
    position: absolute;
    top: calc(${props.theme.typography.heading[5].lineHeightMobile} + ${props.theme.spacing.fixed[10] * 1.2}px);
    left: 50%;
    transform: translateX(-50%);
  } */
`)
