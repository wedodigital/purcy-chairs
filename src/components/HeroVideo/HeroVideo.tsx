import React, { FC, ReactElement } from 'react'

import Heading from '@components/Heading'
import Button from '@components/Button'

import * as Styled from './styles/HeroVideo.style'

import { HeroVideoProps } from './HeroVideo.types'

const HeroVideo: FC<HeroVideoProps> = ({ videoSrc, title, featureImage, poster }: HeroVideoProps): ReactElement => {
  return (
    <Styled.HeroVideo backgroundImage={featureImage?.sourceUrl}>
      <Styled.Title>
        <Styled.Content>
          <Heading weight={3} size={6} text={title} inverse />
          <Button appearance='secondary' inverse to='/shop' text='Discover Now' size={3} />
        </Styled.Content>
      </Styled.Title>
      <Styled.VideoOverlay />
      <If condition={videoSrc}>
        <video autoPlay loop muted poster={poster}>
          <source src={videoSrc.mp4} type="video/mp4" />
          <source src={videoSrc.webm} type="video/webm" />
        </video>
      </If>
    </Styled.HeroVideo>
  )
}

export default HeroVideo
