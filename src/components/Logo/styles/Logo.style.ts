import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledLogoProps } from './Logo.style.types'

export const Logo = styled.div((props: StyledLogoProps): FlattenSimpleInterpolation => css`
  height: 40px;

  ${props.theme.mixins.respondTo.md(css`
    height: 48px;
  `)}

  svg {
    height: 100%;

    * {
      fill: ${props.theme.colours.primary};

      ${props.inverse && css`
        fill: ${props.theme.colours.tertiary};
      `}
    }
  }
`)
