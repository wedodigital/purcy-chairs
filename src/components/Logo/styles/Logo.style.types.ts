import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledLogoProps {
  theme: Theme
  inverse: boolean
}
