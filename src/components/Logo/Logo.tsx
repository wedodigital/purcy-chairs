import React, { FC, ReactElement } from 'react'
import { Link } from 'gatsby'

import LogoSVG from '@assets/logo.svg'

import { LogoProps } from './Logo.types'

import * as Styled from './styles/Logo.style'

const Logo: FC<LogoProps> = ({ inverse = false }): ReactElement => {
  return (
    <Styled.Logo inverse={inverse}>
      <Link to='/'>
        <LogoSVG />
      </Link>
    </Styled.Logo>
  )
}

export default Logo
