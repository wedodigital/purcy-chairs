import React, { ReactElement, FC } from 'react'

import Container from '@components/Container'

import * as Styled from './styles/Video.style'

import { VideoProps } from './Video.types'

const Video: FC<VideoProps> = ({
  videoUrl,
}: VideoProps): ReactElement => {
  return (
    <Container>
      <Styled.VideoWrapper>
        <Styled.Video
          src={videoUrl}
          width='640'
          height='360'
          frameBorder='0'
          allow='autoplay; fullscreen; picture-in-picture'
          allowFullScreen
        />
      </Styled.VideoWrapper>
    </Container>
  )
}

export default Video
