import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledVideoProps {
  theme: Theme
}
