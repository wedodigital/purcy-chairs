import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledVideoProps } from './Video.style.types'

export const Video = styled.iframe((): FlattenSimpleInterpolation => [])

export const VideoWrapper = styled.div((props: StyledVideoProps): FlattenSimpleInterpolation => css`
  height: 0;
  padding-bottom: 56.25%;
  position: relative;
  margin-bottom: ${props.theme.spacing.fixed[6]}px;

  ${props.theme.mixins.respondTo.md(css`
    margin-bottom: ${props.theme.spacing.fixed[10]}px;
  `)}

  iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
`)
