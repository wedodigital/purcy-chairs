export interface ToggleProps {
  isOpen: boolean
  onClick: () => void
  text?: string
  toggleType: 'arrow' | 'plus'
}
