import React, { FC, ReactElement } from 'react'

import Decrease from '@assets/decrease.svg'
import Increase from '@assets/increase.svg'
import AngleDown from '@assets/angle-down.svg'

import * as Styled from './styles/Toggle.style'

import { ToggleProps } from './Toggle.types'

const Toggle: FC<ToggleProps> = ({ isOpen, onClick, toggleType = 'plus'}: ToggleProps): ReactElement => {
  return (
    <Styled.Toggle onClick={onClick} isOpen={isOpen} toggleType={toggleType}>
      <Choose>
        <When condition={toggleType === 'arrow'}>
          <AngleDown />
        </When>
        <Otherwise>
          <Choose>
            <When condition={isOpen}>
              <Decrease />
            </When>
            <Otherwise>
              <Increase />
            </Otherwise>
          </Choose>
        </Otherwise>
      </Choose>
    </Styled.Toggle>
  )
}

export default Toggle
