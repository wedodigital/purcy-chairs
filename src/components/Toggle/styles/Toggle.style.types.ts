import { Theme } from '@themes/purcyTheme/purcyTheme.types'

import { ToggleProps } from '../Toggle.types'

export interface StyledToggleProps {
  theme: Theme
  isOpen: boolean
  toggleType: ToggleProps['toggleType']
}
