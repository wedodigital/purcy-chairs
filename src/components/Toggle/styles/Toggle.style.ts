import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledToggleProps } from './Toggle.style.types'

export const Toggle = styled.button((props: StyledToggleProps): FlattenSimpleInterpolation => css`
  background: none;
  color: ${props.theme.colours.primary};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  text-decoration: none;
  font-weight: 700;
  border: none;
  cursor: pointer;
  font-family: ${props.theme.typography.fontFamily};
  width: ${props.theme.spacing.fixed[4]}px;
  height: ${props.theme.spacing.fixed[4]}px;
  transition: 0.4s all ease;
  flex-wrap: wrap;

  ${props.isOpen && css`
    transform: rotate(180deg);
  `}

  ${props.toggleType === 'arrow' && css`
    transform: rotate(180deg);

    ${props.isOpen && css`
      transform: rotate(0);
    `}
  `}

  & > svg {
    width: ${props.theme.spacing.fixed[4]}px;
    height: ${props.theme.spacing.fixed[4]}px;

    * {
      fill: ${props.theme.colours.primary};
    }
  }

  & > span {
    display: block;
  }
`)
