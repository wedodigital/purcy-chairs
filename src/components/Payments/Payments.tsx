import React, { FC, ReactElement, useContext, useState, useEffect } from 'react'
import { PaymentElement, useElements, useStripe } from '@stripe/react-stripe-js'
import { Formik, Form } from 'formik'
import { navigate } from 'gatsby'
import lookup from 'country-code-lookup'

import CheckoutButtonWrapper from '@components/CheckoutButtonWrapper'
import CheckoutPanel from '@components/CheckoutPanel'
import Button from '@components/Button'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

const Payments: FC = ({ originUrl }): ReactElement => {
  const availablePaymentMethods = [
    {
      id: 'stripe',
      title: 'Credit Card (Stripe)',
      method_title: 'Pay in full with Stripe',
    }
  ]

  const pageContext = useContext(PageContext) as PageContextType
  const [selectedPaymentMethod] = useState(availablePaymentMethods[0])
  const [loading, setLoading] = useState(true)
  const [isValid, setIsValid] = useState(true)
  const [stripeError, setStripeError] = useState()

  useEffect(() => {
    setLoading(false)
  }, [])

  // Stripe Start
  const elements = useElements()
  const stripe = useStripe()

  const [stripeResponse, setStripeResponse] = useState({})

  useEffect(() => {
    if (!!stripeResponse.paymentIntent?.client_secret) {
      switch(stripeResponse.paymentIntent.status) {
        case 'succeeded':
          navigate('/checkout/confirmation')
          break;
      }
    }
    if (stripeResponse.error) {
      setIsValid(false)
      setStripeError(stripeResponse.error!.message)
    }
  }, [stripeResponse])

  // Stripe End

  return (
    <Formik
      initialValues={{
        billing: pageContext.customerDetails.billing,
        shipping: pageContext.customerDetails.shipping,
        shipping_lines: [{
          method_id: pageContext.shippingMethod.method_id,
          method_title: pageContext.shippingMethod.method_title,
          total: `${pageContext.shippingMethod.total}`,
        }],
        line_items: [],
        coupon_lines: [],
        payment_method: selectedPaymentMethod.id,
        payment_method_title: selectedPaymentMethod.title,
        set_paid: false,
      }}
      onSubmit={(values) => {
        setLoading(true)
        pageContext.setPaymentMethod(selectedPaymentMethod)
        const { error } = stripe.confirmPayment({
          elements,
          redirect: 'if_required',
          confirmParams: {
            return_url: `${originUrl}/checkout/confirmation`,
            payment_method_data: {
              billing_details: {
                address: {
                  country: lookup.byCountry(pageContext.customerDetails.billing.country).iso2
                },
              },
            },
          },
        })
        .then(res => {
          setStripeResponse(res)
          setLoading(false)
        })
      }}
    >
      <Form>
        <CheckoutPanel title='Payment.' loading={loading} isValid={isValid} errorDetails={stripeError}>
          <PaymentElement id='payment-element' />
        </CheckoutPanel>
        <CheckoutButtonWrapper>
          <Choose>
            <When condition={loading}>
              <Button text='Complete Order' />
            </When>
            <Otherwise>
              <Button type='submit' text='Complete Order' />
            </Otherwise>
          </Choose>
        </CheckoutButtonWrapper>
      </Form>
    </Formik>
  )
}

export default Payments
