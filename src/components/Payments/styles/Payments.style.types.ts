import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledPaymentProps {
  theme: Theme
  active: boolean
}
