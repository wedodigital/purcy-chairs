import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledPaymentProps } from './Payments.style.types'

export const PaymentBlock = styled.div((props: StyledPaymentProps): FlattenSimpleInterpolation => css`
  height: 0;
  overflow: hidden;

  ${props.active && css`
    height: auto;
    border-top: 1px solid ${props.theme.colours.primary};
    margin: ${props.theme.spacing.fixed[4]}px 0 0;
    padding: ${props.theme.spacing.fixed[2]}px 0 0;
  `}

  iframe {
    max-width: 100% !important;
  }
`)

export const BasketWrapper = styled.div((props: StyledPaymentProps): FlattenSimpleInterpolation => css`
  border-bottom: 1px solid ${props.theme.colours.primary};
  margin-bottom: ${props.theme.spacing.fixed[4]}px;
  padding-bottom: ${props.theme.spacing.fixed[2]}px;
`)
