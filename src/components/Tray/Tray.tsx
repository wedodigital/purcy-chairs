import React, { ReactElement, FC, useState, useContext } from 'react'
import AnimateHeight from 'react-animate-height'
import { useStaticQuery, graphql } from 'gatsby'

import ProductDetails from '@components/ProductDetails'
import ProductHeader from '@components/ProductHeader'
import Toggle from '@components/Toggle'

import MatchMedia from '@utils/MatchMedia'

import * as Styled from './styles/Tray.style'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

import { TrayProps } from './Tray.types'

const Tray: FC<TrayProps> = ({
  databaseId,
  title,
  shortDescription,
  variations,
  onSelectItem,
}: TrayProps): ReactElement => {
  const pageContext = useContext(PageContext) as PageContextType

  const footerContent = useStaticQuery(graphql`
    query FooterContent {
      purcyChair: allWpProduct(filter: {databaseId: {eq: 11}}) {
        nodes {
          name
          databaseId
          galleryImages {
            nodes {
              sourceUrl
            }
          }
          ... on WpVariableProduct {
            variations {
              nodes {
                databaseId
              }
            }
            featuredImage {
              node {
                sourceUrl
              }
            }
          }
        }
      }
    }
  `)

  const productData = footerContent.purcyChair.nodes[0]

  const [activeProduct] = useState(databaseId ? pageContext.productList.find(product => product.databaseId === variations[0]) : null)
  const [baseProduct] = useState(pageContext.productList.find(product => product.databaseId === productData.variations.nodes[0].databaseId))

  const [showTray, toggleTray] = useState(true)

  return (
    <MatchMedia breakpoint='sm'>
      <Styled.Tray>
        <Styled.TrayHeader>
          <ProductHeader
            title={title || productData.name}
            activeProduct={activeProduct || baseProduct}
            databaseId={databaseId || productData.databaseId}
          />
          <Styled.ToggleWrapper>
            <Toggle isOpen={showTray} onClick={() => toggleTray(!showTray)} toggleType='arrow' />
          </Styled.ToggleWrapper>
        </Styled.TrayHeader>
        <AnimateHeight
          duration={ 500 }
          height={showTray ? 'auto' : 0}
        >
          <Styled.TrayBody>
            <ProductDetails
              databaseId={databaseId || productData.databaseId}
              title={title || productData.name}
              shortDescription={shortDescription}
              variations={variations || productData.variations.nodes.map(variation => variation.databaseId)}
              onSelectItem={onSelectItem}
            />
          </Styled.TrayBody>
        </AnimateHeight>
      </Styled.Tray>
    </MatchMedia>
  )
}

export default Tray
