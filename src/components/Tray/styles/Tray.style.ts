import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledTrayProps } from './Tray.style.types'

import { FormSection } from '@components/TotalPrice/styles/TotalPrice.style'

export const Tray = styled.div((props: StyledTrayProps): FlattenSimpleInterpolation => css`
  position: sticky;
  bottom: 0;
  left: 0;
  width: 100%;
  background: ${props.theme.colours.tertiary};
  padding: 0 ${props.theme.spacing.fixed[3]}px;
`)

export const TrayHeader = styled.div((props: StyledTrayProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[3]}px 0;
  display: flex;
  align-items: center;

  ${FormSection} {
    padding: 0;
    border-bottom: none;
  }
`)

export const TrayBody = styled.div((props: StyledTrayProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[3]}px 0;
  border-top: 1px solid ${props.theme.colours.secondary};
`)

export const ToggleWrapper = styled.div((props: StyledTrayProps): FlattenSimpleInterpolation => css`
  margin-left: auto;
  flex-shrink: 0;
  padding-left: ${props.theme.spacing.fixed[3]}px;
`)
