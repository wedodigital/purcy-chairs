import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledTrayProps {
  theme: Theme;
}
