import { ReactNode } from 'react'

export interface TrayProps {
  /**
   * React children
   * */
  children: ReactNode
}
