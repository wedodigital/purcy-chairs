import styled, { css, createGlobalStyle, FlattenSimpleInterpolation } from 'styled-components'

export const HideOverflow = createGlobalStyle((): FlattenSimpleInterpolation => css`
  html,
  body {
    overflow: hidden;
  }
`)

export const Overlay = styled.div((): FlattenSimpleInterpolation => css`
  background: rgba(0, 0, 0, 0.6);
  position: fixed;
  z-index: 1000;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`)
