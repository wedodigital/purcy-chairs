import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledTotalPriceProps {
  theme: Theme;
}
