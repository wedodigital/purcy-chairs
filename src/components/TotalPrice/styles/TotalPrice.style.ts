import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledTotalPriceProps } from './TotalPrice.style.types'

export const FormRow = styled.div((props: StyledTotalPriceProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[3]}px;
  display: flex;
  align-items: center;
  justify-content: flex-start;

  &:last-child {
    margin-bottom: 0;
  }
`)

export const FormSection = styled.div((props: StyledTotalPriceProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[3]}px 0;
  border-bottom: 1px solid ${props.theme.colours.secondary};

  &:first-child {
    padding-top: 0;
  }

  &:last-child {
    padding-bottom: 0;
    border-bottom: none;
  }
`)

export const FormLabel = styled.span((props: StyledTotalPriceProps): FlattenSimpleInterpolation => css`
  font-weight: 700;
  margin-right: ${props.theme.spacing.fixed[3]}px;
`)
