import React, { FC, ReactElement } from 'react'

import * as Styled from './styles/TotalPrice.style'

import { TotalPriceProps } from './TotalPrice.types'

const TotalPrice: FC<TotalPriceProps> = ({ title, regularPrice, salePrice, quantity, onSale }: TotalPriceProps): ReactElement => {
  return (
    <Styled.FormSection>
      <If condition={title}>
        <Styled.FormLabel>
          {title}:
        </Styled.FormLabel>
      </If>
      <Choose>
        <When condition={onSale}>
          <span><del>£{(regularPrice * quantity).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</del> £{(salePrice * quantity).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
        </When>
        <Otherwise>
          <span>£{(regularPrice * quantity).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
        </Otherwise>
      </Choose>
    </Styled.FormSection>
  )
}

export default TotalPrice
