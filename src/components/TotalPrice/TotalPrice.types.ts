export interface TotalPriceProps {
  title?: string
  regularPrice: number
  salePrice: number
  quantity: number
  onSale: boolean
}
