import { ReactNode } from 'react'

export interface MobileNavLinkProps {
  menuItem: {
    title: string
    url: string
    children?: {
      title: string
      url: string
    }
  }
}
