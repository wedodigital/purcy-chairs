import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledMobileNavLinkProps } from './MobileNavLink.style.types'

export const MobileNavLink = styled.div((props: StyledMobileNavLinkProps): FlattenSimpleInterpolation => css`
  all: unset;
  color: ${props.theme.colours.primary};
  text-decoration: none;
  display: flex;
  width: 100%;
  align-items: center;
`)

export const Arrow = styled.span((props: StyledMobileNavLinkProps): FlattenSimpleInterpolation => css`
  margin-left: auto;
  transition: 0.4s all ease;

  ${props.isActive && css`
    transform: rotate(180deg);
  `}

  svg {
    width: ${props.theme.spacing.fixed[3]}px;
    height: ${props.theme.spacing.fixed[3]}px;
  }
`)

export const SubNav = styled.ul((props: StyledMobileNavLinkProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[2]}px 0 0 ${props.theme.spacing.fixed[2]}px;

  li {
    font-size: ${props.theme.typography.paragraph[3].fontSizeMobile};
  }
`)
