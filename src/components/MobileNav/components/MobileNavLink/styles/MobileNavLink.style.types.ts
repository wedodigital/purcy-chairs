import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledMobileNavLinkProps {
  theme: Theme;
}
