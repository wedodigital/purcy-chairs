import React, { ReactElement, FC, useState } from 'react'
import { Link } from 'gatsby'
import AnimateHeight from 'react-animate-height'

import AngleDown from '@assets/angle-down.svg'

import * as Styled from './styles/MobileNavLink.style'

import { MobileNavLinkProps } from './MobileNavLink.types'

const MobileNavLink: FC<MobileNavLinkProps> = ({
  menuItem,
}: MobileNavLinkProps): ReactElement => {
  const [showChildren, setShowChildren] = useState(false)
  return (
    <Choose>
      <When condition={menuItem.children}>
        <Styled.MobileNavLink as='button' onClick={() => setShowChildren(!showChildren)}>
          <span>{menuItem.title}</span>
          <Styled.Arrow isActive={showChildren}><AngleDown /></Styled.Arrow>
        </Styled.MobileNavLink>
        <AnimateHeight
          duration={ 500 }
          height={showChildren ? 'auto' : 0}
        >
          <Styled.SubNav>
            {
              menuItem.children.map((child) => {
                return (
                  <li>
                    <Styled.MobileNavLink as={Link} to={child.databaseId === 11 ? menuItem.url : `${menuItem.url}/${child.slug}`}>{child.name}</Styled.MobileNavLink>
                  </li>
                )
              })
            }
          </Styled.SubNav>
        </AnimateHeight>
      </When>
      <Otherwise>
        <Styled.MobileNavLink as={Link} to={menuItem.url}>{menuItem.title}</Styled.MobileNavLink>
      </Otherwise>
    </Choose>
  )
}

export default MobileNavLink
