import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { StyledMobileNavProps } from './MobileNav.style.types'

export const MobileNav = styled.div((props: StyledMobileNavProps): FlattenSimpleInterpolation => css`
  position: fixed;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  z-index: 1000;

  a {
    color: ${props.theme.colours.primary}
  }
`)

export const NavContent = styled.div((props: StyledMobileNavProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.tertiary};
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: calc(100% - ${props.theme.spacing.fixed[8]}px);
  z-index: 1000;
  padding: 0 ${props.theme.spacing.fixed[2]}px;
  overflow: auto;
`)

export const MobileNavSection = styled.div((props: StyledMobileNavProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[4]}px 0;
  border-bottom: 1px solid ${props.theme.colours.secondary};

  &:last-child {
    border-bottom: 0;
  }

  li {
    margin-bottom: ${props.theme.spacing.fixed[1]}px;
    width: 100%;

    &:last-child {
      margin-bottom: 0;
    }
  }
`)

export const PrimaryNav = styled.ul((props: StyledMobileNavProps): FlattenSimpleInterpolation => css`
  font-size: ${props.theme.typography.heading[1].fontSize};
  line-height: ${props.theme.typography.heading[1].lineHeight};
`)

export const CloseButton = styled.button((props: StyledMobileNavProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.primary};
  color: ${props.theme.colours.tertiary};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  text-decoration: none;
  font-weight: 700;
  border: none;
  cursor: pointer;
  font-size: 24px;
  line-height: 24px;
  font-family: ${props.theme.typography.fontFamily};
  width: ${props.theme.spacing.fixed[8]}px;
  height: ${props.theme.spacing.fixed[8]}px;
  position: absolute;
  top: 0;
  right: 0;


  & > svg {
    width: ${props.theme.spacing.fixed[4]}px;
    height: ${props.theme.spacing.fixed[4]}px;

    * {
      fill: ${props.theme.colours.tertiary};
    }
  }
`)
