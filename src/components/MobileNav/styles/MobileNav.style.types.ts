import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledMobileNavProps {
  theme: Theme;
}
