import { Link } from '../../Navigation.types'

export interface MobileNavProps {
  menu: Link[]
  toggleMobileNav: () => void
  formEndpoint: string
}
