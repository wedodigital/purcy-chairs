import React, { FC, ReactElement, useState } from 'react'

import Close from '@assets/close.svg'

import EmailSignup from '@components/EmailSignup'
import Heading from '@components/Heading'

import MobileNavLink from './components/MobileNavLink'

import * as Styled from './styles/MobileNav.style'

import { MobileNavProps } from './MobileNav.types'

const MobileNav: FC<MobileNavProps> = ({ menu, toggleMobileNav, formEndpoint }: MobileNavProps): ReactElement => {
  return (
    <>
      <Styled.MobileNav>
        <Styled.CloseButton onClick={() => toggleMobileNav(false)}>
          <Close />
        </Styled.CloseButton>
        <Styled.NavContent>
          <Styled.MobileNavSection>
            <Styled.PrimaryNav>
              {
                menu.map((menuItem) => {
                  return (
                    <li>
                      <MobileNavLink menuItem={menuItem} />
                    </li>
                  )
                })
              }
            </Styled.PrimaryNav>
          </Styled.MobileNavSection>
          <Styled.MobileNavSection>
            <EmailSignup size={1} columns={1} endpoint={formEndpoint} />
          </Styled.MobileNavSection>
          <Styled.MobileNavSection>
            <Heading size={1} weight={3} text='Connect with us' />
            <ul>
              <li><a href="https://www.instagram.com/purcy.chair" target="_blank">Instagram</a></li>
              <li><a href="http://www.facebook.com/purcychair" target="_blank">Facebook</a></li>
            </ul>
          </Styled.MobileNavSection>
        </Styled.NavContent>
      </Styled.MobileNav>
    </>
  )
}

export default MobileNav
