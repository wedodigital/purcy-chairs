import React, { FC, ReactElement, useContext } from 'react'
import { CSSTransition } from 'react-transition-group'

import Close from '@assets/close.svg'

import Overlay from '@components/Overlay'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

import * as Styled from './styles/Modal.style'

import { ModalProps } from './Modal.types'

const Modal: FC<ModalProps> = ({ children, width }: ModalProps ): ReactElement => {
  const context = useContext(PageContext) as PageContextType
  return (
    <>
      <Overlay onClick={() => context.toggleModal()} />
      <CSSTransition
        in={context.showModal}
        timeout={500}
        classNames='modal'
        unmountOnExit
      >
        <Styled.ModalWrapper>
          <Styled.ModalContainer width={width}>
            <Styled.CloseButton onClick={() => context.toggleModal()}>
              <Close />
            </Styled.CloseButton>
            <Styled.ModalContent>
              {children}
            </Styled.ModalContent>
          </Styled.ModalContainer>
        </Styled.ModalWrapper>
      </CSSTransition>
    </>
  )
}

export default Modal
