import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledModalProps } from './Modal.style.types'

export const ModalWrapper = styled.div((): FlattenSimpleInterpolation => css`
  position: fixed;
  z-index: 1000;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  pointer-events: none;
`)

export const ModalContainer = styled.div((props: StyledModalProps): FlattenSimpleInterpolation => css`
  background: #fff;
  padding: ${props.theme.spacing.fixed[2]}px;
  border-radius: ${props.theme.spacing.fixed[1] / 2}px;
  width: 90%;
  max-width: 1000px;
  pointer-events: auto;
  box-shadow: 0 0 10px 0 rgba(0,0,0,0.2);
  position: relative;

  ${props.width === 'narrow' && css`
    max-width: 700px;
  `}
`)

export const ModalContent = styled.div((props: StyledModalProps): FlattenSimpleInterpolation => css`
  border: 1px solid ${props.theme.colours.primary};
  padding: ${props.theme.spacing.fixed[3]}px;
`)

export const CloseButton = styled.button((props: StyledModalProps): FlattenSimpleInterpolation => css`
  background: none;
  color: ${props.theme.colours.tertiary};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  text-decoration: none;
  font-weight: 700;
  border: none;
  cursor: pointer;
  font-size: 24px;
  line-height: 24px;
  font-family: ${props.theme.typography.fontFamily};
  width: ${props.theme.spacing.fixed[4]}px;
  height: ${props.theme.spacing.fixed[4]}px;
  position: absolute;
  bottom: 100%;
  left: 100%;
  margin: 0 0 ${props.theme.spacing.fixed[2]}px ${props.theme.spacing.fixed[2]}px;


  & > svg {
    width: ${props.theme.spacing.fixed[4]}px;
    height: ${props.theme.spacing.fixed[4]}px;

    * {
      fill: ${props.theme.colours.tertiary};
    }
  }
`)
