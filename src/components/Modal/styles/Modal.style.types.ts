import { Theme } from '@themes/purcyTheme/purcyTheme.types'

import { ModalProps } from '../Modal.types'

export interface StyledModalProps {
  theme: Theme;
  width: ModalProps['width']
}
