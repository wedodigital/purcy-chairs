export interface ShopHeroProps {
  headline: string
  icons: {
    icon: {
      sourceUrl: string
    }
    title: string
  }[]
  introContent: string[]
}
