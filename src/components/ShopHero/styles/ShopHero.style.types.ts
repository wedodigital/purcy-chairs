import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledShopHeroProps {
  theme: Theme
}
