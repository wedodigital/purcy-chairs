import styled, { FlattenSimpleInterpolation, css } from 'styled-components'

import { ProductDetails } from '@components/ProductDetails/styles/ProductDetails.style'

import { StyledShopHeroProps } from './ShopHero.style.types'

export const HeroWrapper = styled.div((props: StyledShopHeroProps): FlattenSimpleInterpolation => css`
  position: relative;

  ${ProductDetails} {
    position: absolute;
    z-index: 50;
    top: ${props.theme.spacing.fixed[4]}px;
    left: 5%;
  }
`)

export const ShopHero = styled.div((): FlattenSimpleInterpolation => css`
  text-align: center;
`)

export const Message = styled.div((props: StyledShopHeroProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.tertiary};
  position: absolute;
  bottom: 60px;
  left: 5%;
  padding: ${props.theme.spacing.fixed[1]}px ${props.theme.spacing.fixed[2]}px;
  z-index: 20;
  box-shadow: 0 0 10px 0 rgba(0,0,0,0.2);
  border-radius: ${props.theme.spacing.fixed[1] / 2}px;
`)
