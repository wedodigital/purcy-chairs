import React, { FC, ReactElement } from 'react'

import Container from '@components/Container'
import Heading from '@components/Heading'

import HeroIcons from './components/HeroIcons'
import HeroColumns from './components/HeroColumns'

import * as Styled from './styles/ShopHero.style'

import { ShopHeroProps } from './ShopHero.types'

const ShopHero: FC<ShopHeroProps> = ({ headline, icons, introContent }: ShopHeroProps): ReactElement => {
  return (
    <Styled.HeroWrapper>
      <Styled.ShopHero>
        <Container>
          <Heading weight={3} size={6} text={headline} />
          <HeroIcons icons={icons} />
          <HeroColumns heading={introContent[0]} content={introContent[1]} />
        </Container>
      </Styled.ShopHero>
    </Styled.HeroWrapper>
  )
}

export default ShopHero
