export interface HeroColumnsProps {
  heading: string
  content: string
}
