import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledHeroColumnsProps {
  theme: Theme
}
