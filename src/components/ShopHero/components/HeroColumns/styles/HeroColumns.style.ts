import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledHeroColumnsProps } from './HeroColumns.style.types'

export const HeroColumns = styled.div((props: StyledHeroColumnsProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  padding: ${props.theme.spacing.fixed[4]}px 0;
  text-align: left;

  ${props.theme.mixins.respondTo.md(css`
    flex-direction: row;
    padding: ${props.theme.spacing.fixed[8]}px 0 ${props.theme.spacing.fixed[10] * 2}px;
  `)}
`)

export const Column = styled.div((props: StyledHeroColumnsProps): FlattenSimpleInterpolation => css`
  width: 100%;
  padding: ${props.theme.spacing.fixed[2]}px 0;

  &:first-child {
    padding-top: 0;
  }

  &:last-child {
    padding-bottom: 0;
  }

  ${props.theme.mixins.respondTo.md(css`
    padding: 0 ${props.theme.spacing.fixed[4]}px;

    &:first-child {
      padding-left: 0;
    }

    &:last-child {
      padding-right: 0;
    }
  `)}
`)
