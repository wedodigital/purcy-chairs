import React, { FC, ReactElement } from 'react'

import Heading from '@components/Heading'

import { HeroColumnsProps } from './HeroColumns.types'

import * as Styled from './styles/HeroColumns.style'

const HeroColumns: FC<HeroColumnsProps> = ({ heading, content }: HeroColumnsProps): ReactElement => {
  return (
    <Styled.HeroColumns>
      <Styled.Column>
        <Heading weight={2} text={heading} />
      </Styled.Column>
      <Styled.Column>
        <Heading text={content} />
      </Styled.Column>
    </Styled.HeroColumns>
  )
}

export default HeroColumns
