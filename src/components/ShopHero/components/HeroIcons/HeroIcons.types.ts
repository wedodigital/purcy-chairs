import { ShopHeroProps } from '../../ShopHero.types'

export interface HeroIconsProps {
  icons: ShopHeroProps['icons']
}
