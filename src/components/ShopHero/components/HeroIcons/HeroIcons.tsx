import React, { FC, ReactElement } from 'react'

import Icon from './components/Icon'

import { HeroIconsProps } from './HeroIcons.types'

import * as Styled from './styles/HeroIcons.style'

const HeroIcons: FC<HeroIconsProps> = ({ icons }: HeroIconsProps): ReactElement => {
  return (
    <Styled.IconList>
      {
        icons.map((icon) => {
          return (
            <Icon title={icon.title} icon={icon.icon.sourceUrl} />
          )
        })
      }
    </Styled.IconList>
  )
}

export default HeroIcons
