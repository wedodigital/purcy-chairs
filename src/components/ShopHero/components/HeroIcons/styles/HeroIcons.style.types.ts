import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledHeroIconsProps {
  theme: Theme
}
