import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledHeroIconsProps } from './HeroIcons.style.types'

export const IconList = styled.ul((props: StyledHeroIconsProps): FlattenSimpleInterpolation => css`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  align-items: flex-start;
  border-top: 2px solid ${props.theme.colours.quartenary};
  border-bottom: 2px solid ${props.theme.colours.quartenary};
  padding: ${props.theme.spacing.fixed[3]}px 0;

  ${props.theme.mixins.respondTo.md(css`
    flex-wrap: nowrap;
    padding: ${props.theme.spacing.fixed[6]}px 0;
  `)}
`)
