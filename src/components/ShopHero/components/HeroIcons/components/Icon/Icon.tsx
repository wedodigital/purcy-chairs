import React, { FC, ReactElement } from 'react'

import Heading from '@components/Heading'

import * as Styled from './styles/Icon.style'

import { IconProps } from './Icon.types'

const Icon: FC<IconProps> = ({ icon, title }: IconProps): ReactElement => {
  return (
    <Styled.Icon>
      <Styled.IconBackground>
        <img src={icon} alt={title} />
      </Styled.IconBackground>
      <Heading size={1} text={title} noMargin />
    </Styled.Icon>
  )
}

export default Icon
