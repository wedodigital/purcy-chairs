export interface IconProps {
  icon: string
  title: string
}
