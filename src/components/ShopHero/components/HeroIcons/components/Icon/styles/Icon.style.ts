import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledIconProps } from './Icon.style.types'

export const Icon = styled.li((props: StyledIconProps): FlattenSimpleInterpolation => css`
  display: flex;
  text-align: center;
  flex-direction: column;
  align-items: center;
  width: calc(33% - (${props.theme.spacing.fixed[1]}px * 2));
  margin: ${props.theme.spacing.fixed[1]}px;

  ${props.theme.mixins.respondTo.md(css`
    width: auto;
    margin: 0 ${props.theme.spacing.fixed[4]}px;

    &:first-child {
      margin-left: 0;
    }

    &:last-child {
      margin-right: 0;
    }
  `)}
`)

export const IconBackground = styled.div((props: StyledIconProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.primary};
  padding: ${props.theme.spacing.fixed[2]}px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: ${props.theme.spacing.fixed[2]}px;

  img {
    display: block;
    max-width: 40px;
  }
`)
