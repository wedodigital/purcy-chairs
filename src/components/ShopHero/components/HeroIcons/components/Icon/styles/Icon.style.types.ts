import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledIconProps {
  theme: Theme
}
