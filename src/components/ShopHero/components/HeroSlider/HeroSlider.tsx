import React, { PureComponent, ReactNode } from 'react'

import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import LeftArrow from '@assets/left-arrow.svg'
import RightArrow from '@assets/right-arrow.svg'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

import { HeroSliderProps } from './HeroSlider.types'

import * as Styled from './styles/HeroSlider.style'

class HeroSlider extends PureComponent<HeroSliderProps> {
  public darkSlider: Slider
  public lightSlider: Slider

  nextSlide = () => {
    this.darkSlider.slickNext()
    this.lightSlider.slickNext()
  }

  prevSlide = () => {
    this.darkSlider.slickPrev()
    this.lightSlider.slickPrev()
  }

  render(): ReactNode {
    const settings = {
      dots: false,
      infinite: true,
      fade: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    }

    return (
      <Styled.HeroSlider>
        <Styled.PrevButton onClick={() => this.prevSlide()}>
          <LeftArrow />
        </Styled.PrevButton>
        <Styled.NextButton onClick={() => this.nextSlide()}>
          <RightArrow />
        </Styled.NextButton>
        <PageContext.Consumer>
          {(context: PageContextType): ReactNode => {
            return (
              <>
                <Styled.Slider activeSlider={context.activeProduct?.databaseId === 12}>
                  <Slider ref={slider => (this.darkSlider = slider)} {...settings}>
                  {
                    this.props.darkGallery.map((image) => {
                      return (
                        <Styled.Slide background={image.image.sourceUrl} />
                      )
                    })
                  }
                  </Slider>
                </Styled.Slider>
                <Styled.Slider activeSlider={context.activeProduct?.databaseId === 13}>
                  <Slider ref={slider => (this.lightSlider = slider)} {...settings}>
                  {
                    this.props.lightGallery.map((image) => {
                      return (
                        <Styled.Slide background={image.image.sourceUrl} />
                      )
                    })
                  }
                  </Slider>
                </Styled.Slider>
              </>
            )
          }}
        </PageContext.Consumer>
      </Styled.HeroSlider>
    )
}
}

export default HeroSlider
