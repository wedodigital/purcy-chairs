import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledHeroSliderProps } from './HeroSlider.style.types'

export const HeroSlider = styled.div((props: Pick<StyledHeroSliderProps, 'theme'>): FlattenSimpleInterpolation => css`
  width: 100%;
  height: calc(100vh - 147px);
  z-index: 20;
  margin-bottom: ${props.theme.spacing.fixed[8]}px;
  position: relative;
`)

export const Slider = styled.div((props: Pick<StyledHeroSliderProps, 'activeSlider'>): FlattenSimpleInterpolation => css`
  overflow: hidden;
  height: calc(100vh - 147px);

  ${!props.activeSlider && css`
    opacity: 0;
    height: 0;
  `}
`)

export const Slide = styled.div((props: Pick<StyledHeroSliderProps, 'background'>): FlattenSimpleInterpolation => css`
  height: calc(100vh - 147px);
  background: url('${props.background}') center center no-repeat;
  background-size: cover;
`)

export const PrevButton = styled.button((props: Pick<StyledHeroSliderProps, 'theme'>): FlattenSimpleInterpolation => css`
  background: none;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  border: none;
  cursor: pointer;
  width: ${props.theme.spacing.fixed[4]}px;
  height: ${props.theme.spacing.fixed[4]}px;
  position: absolute;
  top: 50%;
  left: 5%;
  z-index: 10;
  transform: translateY(-50%);

  & > svg {
    width: ${props.theme.spacing.fixed[4]}px;
    height: ${props.theme.spacing.fixed[4]}px;

    * {
      fill: ${props.theme.colours.primary};
    }
  }
`)

export const NextButton = styled.button((props: Pick<StyledHeroSliderProps, 'theme'>): FlattenSimpleInterpolation => css`
  background: none;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  border: none;
  cursor: pointer;
  width: ${props.theme.spacing.fixed[4]}px;
  height: ${props.theme.spacing.fixed[4]}px;
  position: absolute;
  top: 50%;
  right: 5%;
  z-index: 10;
  transform: translateY(-50%);

  & > svg {
    width: ${props.theme.spacing.fixed[4]}px;
    height: ${props.theme.spacing.fixed[4]}px;

    * {
      fill: ${props.theme.colours.primary};
    }
  }
`)
