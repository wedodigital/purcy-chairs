import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledHeroSliderProps {
  theme: Theme
  background: string
  activeSlider: boolean
}
