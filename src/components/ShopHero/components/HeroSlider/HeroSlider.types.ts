import { ShopHeroProps } from '../../ShopHero.types'

export interface HeroSliderProps {
  darkGallery: ShopHeroProps['darkGallery']
  lightGallery: ShopHeroProps['lightGallery']
}

export interface HeroSliderState {
  currentSlide: number
}
