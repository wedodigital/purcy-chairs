function cleanProductData(product, parentName?: string, parentId?: number) {
  return {
    databaseId: product.id,
    name: product.name || `${parentName} - ${product.attributes[0].option}`,
    onSale: product.on_sale,
    regularPrice: parseFloat(product.regular_price),
    salePrice: parseFloat(product.sale_price),
    description: product.description ? product.description.replace(/<[^>]*>?/gm, '') : '',
    stockQuantity: product.stock_quantity,
    dimensions: {
      length: parseFloat(product.dimensions.length),
      width: parseFloat(product.dimensions.width),
      height: parseFloat(product.dimensions.height),
    },
    weight: parseFloat(product.weight),
    image: {
      sourceUrl: product.image?.src || product.images[0]?.src || '',
    },
    crossSells: product.cross_sell_ids,
    status: product.status,
    parentId,
  }
}

export default cleanProductData
