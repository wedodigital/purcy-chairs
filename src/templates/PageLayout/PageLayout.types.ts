import { ProductDetails, BasketItem } from '@context/PageContext.types'

export interface PageLayoutProps {
  children: React.ReactNode
  location: {
    search: string
  }
}

export interface PageLayoutState {
  journeyType?: string
  activeProduct?: ProductDetails
  showModal: boolean
  basket: {
    products: BasketItem[],
    discounts: any[],
  }
  products: ProductDetails[]
  loaded: boolean
  showLoader: boolean
  shippingZone: number,
  customerDetails: {
    billing: {
      first_name: string,
      last_name: string,
      company: string,
      email: string,
    },
    shipping: {
      first_name: string,
      last_name: string,
      company: string,
      email: string,
    },
  },
  shippingMethod: {
    method_id: string
    method_title: string
    total: number
  } | {},
  orderId?: number
  formId?: string
  klarnaAuthCode?: string
  stripeKey?: string
  paymentMethod: {
    id: string,
    title: string,
  } | {}
}
