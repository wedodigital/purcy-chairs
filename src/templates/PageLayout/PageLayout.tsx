import React, { PureComponent, ReactNode } from 'react'
import { ThemeProvider } from 'styled-components'
import { CSSTransition } from 'react-transition-group'

import GlobalStyle from '@styles/GlobalStyle'
import { purcyTheme } from '@themes/purcyTheme'

import LoadingOverlay from '@components/LoadingOverlay'

import cleanProductData from '@helpers/cleanProductData'

import PageContext from '@context/PageContext'
import { ProductDetails, BasketItem } from '@context/PageContext.types'

import { PageLayoutProps, PageLayoutState } from './PageLayout.types'

export default class PageLayout extends PureComponent<PageLayoutProps, PageLayoutState> {

  public state: PageLayoutState = {
    journeyType: '',
    formId: '982513069',
    showModal: false,
    products: [],
    basket: {
      products: [],
      discounts: [],
    },
    loaded: false,
    showLoader: true,
    shippingZone: 0,
    customerDetails: {
      billing: {
        first_name: '',
        last_name: '',
        company: '',
        email: '',
      },
      shipping: {
        first_name: '',
        last_name: '',
        company: '',
        email: '',
      },
    },
    shippingMethod: {
      method_id: 0,
      method_title: '',
      total: '0.00',
    },
    paymentMethod: {},
  }

  componentDidMount() {
    this.getProducts()
    const searchParams = new URLSearchParams(this.props.location.search)
    const journeyType = searchParams.get('journeyType')
    const formId = searchParams.get('formId')
    this.setState({
      journeyType,
      formId,
    })
  }

  private getProducts() {
    fetch('https://purcy-api-app.herokuapp.com/purcy/products')
      .then(res => res.json())
      .then((data) => {
        this.setProductInventory(data)
      })
      .catch((error) => {
        console.log(error)
      })
  }

  private getProductVariations(product, isLastProduct: boolean) {
    fetch(`https://purcy-api-app.herokuapp.com/purcy/products/${product.id}/variations`)
      .then(res => res.json())
      .then((data) => {
        data.forEach((variation) => {
          this.setState({
            products: [...this.state.products, cleanProductData(variation, product.name, product.id)]
          })
        })
      })
      .then(() => {
        if (isLastProduct) {
          this.loadBasket()
          this.setLoadingState()
        }
      })
      .catch((error) => {
        console.log(error)
      })

  }

  private setProductInventory(data: any) {
    const productCount = data.length
    data.forEach((product: any, index: number): void => {
      if (product.variations.length) {
        this.getProductVariations(product, productCount === index + 1)
      }
      this.setState({
        products: [...this.state.products, cleanProductData(product)],
      })
    })
  }

  setLoadingState(): void {
    this.setState(
      {
        loaded: true,
      },
      () => {
        setTimeout(() => {
          this.setState({
            showLoader: false,
          })
        }, 1000)
      }
    )
  }

  getTotalPrice(): number {
    let total = 0

    this.state.basket.products.forEach((basketItem) => {
      let subTotal = 0
      if (basketItem.product?.onSale) {
        subTotal = basketItem.product.salePrice * basketItem.quantity
      } else {
        subTotal = basketItem.product?.regularPrice * basketItem.quantity
      }
      total = total + subTotal
    })

    total = total + parseFloat(this.state.shippingMethod.total)

    this.state.basket.discounts.forEach((discountItem) => {
      total = total - parseFloat(discountItem.amount)
    })

    return total
  }

  private toggleModal = (showModal?: boolean): void => {
    if (showModal !== undefined) {
      this.setState({
        showModal: showModal
      })
      return
    }
    this.setState({
      showModal: !this.state.showModal,
    })
  }

  private storeBasketProducts(basket): void {
    const stringifyBasket = basket.map((basketItem: BasketItem) => {
      return `${basketItem.product.databaseId}:${basketItem.quantity}`
    })

    localStorage.setItem('basket', stringifyBasket.join())
  }

  private loadBasket(): void {
    if (localStorage.getItem('basket')) {
      const basketItems = localStorage.getItem('basket').split(',')
      const newBasket = basketItems.map((basketItem) => {
        const splitBasketItem = basketItem.split(':')
        const product = this.state.products.find((product) => product.databaseId === parseInt(splitBasketItem[0]))
        return {
          product: product,
          quantity: parseInt(splitBasketItem[1])
        }
      })

      this.setState({
        basket: {
          products: newBasket,
          discounts: [],
        }
      })
    }
  }

  private getCrossSells = (productId: number): ProductDetails[] => {
    const crossSells = []
    const product = this.state.products.find(product => product.databaseId === productId)
    const crossSellIds = product.crossSells
    if (crossSellIds.length > 0) {
      crossSellIds.forEach((crossSellProductId) => {
        crossSells.push(this.state.products.find(product => product.databaseId === crossSellProductId))
      })
    }
    return crossSells
  }

  private updateBasket = (product: ProductDetails, quantity: number): void => {
    const newBasket = [...this.state.basket.products]

    const crossSells = this.getCrossSells(product.parentId ? product.parentId : product.databaseId)
    if (crossSells) {
      crossSells.forEach((crossSell) => {
        const basketItem = newBasket.find((basketItem: BasketItem) => basketItem.product.databaseId === crossSell.databaseId)
        if (basketItem) {
          basketItem.quantity = basketItem.quantity + quantity
        } else {
          newBasket.push({ product: crossSell, quantity })
        }
      })
    }

    const basketItem = newBasket.find((basketItem: BasketItem) => basketItem.product.databaseId === product.databaseId)
    if (basketItem) {
      basketItem.quantity = basketItem.quantity + quantity
    } else {
      newBasket.push({ product, quantity })
    }

    this.storeBasketProducts(newBasket)

    this.setState({
      basket: {
        products: newBasket,
        discounts: [],
      },
    })
  }

  private clearBasket = (): void => {
    this.storeBasketProducts([])
    this.setState({
      basket: {
        products: [],
        discounts: [],
      },
    })
  }

  private amendQuantity = (product: ProductDetails, changeType: 'increase' | 'decrease'): void => {
    const newBasket = [...this.state.basket.products]
    const basketItem = newBasket.find((basketItem: BasketItem) => basketItem.product.databaseId === product.databaseId)

    if (basketItem.quantity === 1 && changeType === 'decrease') return

    const crossSells = this.getCrossSells(product.parentId ? product.parentId : product.databaseId)
    if (crossSells) {
      crossSells.forEach((crossSell) => {
        const basketItem = newBasket.find((basketItem: BasketItem) => basketItem.product.databaseId === crossSell.databaseId)
        if (changeType === 'increase') {
          basketItem.quantity = basketItem.quantity + 1
        } else {
          basketItem.quantity = basketItem.quantity - 1
        }
      })
    }

    if (changeType === 'increase') {
      basketItem.quantity = basketItem.quantity + 1
    } else {
      basketItem.quantity = basketItem.quantity - 1
    }

    this.storeBasketProducts(newBasket)

    this.setState({
      basket: {
        products: newBasket,
        discounts: [],
      },
    })
  }

  private removeFromBasket = (product: ProductDetails): void => {
    const basket = this.removeCrossSellsFromBasket(product)
    const newBasket = basket.filter((basketItem: BasketItem) => basketItem.product.databaseId !== product.databaseId)

    this.storeBasketProducts(newBasket)

    this.setState({
      basket: {
        products: newBasket,
        discounts: [],
      },
    })
  }

  private removeCrossSellsFromBasket = (product: ProductDetails) => {
    const crossSells = this.getCrossSells(product.parentId ? product.parentId : product.databaseId)
    if (crossSells) {
      const crossSellIds = crossSells.map(crossSell => crossSell.databaseId)
      return [...this.state.basket.products].filter(basketItem => !crossSellIds.includes(basketItem.product.databaseId))
    }
    return this.state.basket
  }

  private addBasketDiscount = (discount) => {
    const basketProducts = [...this.state.basket.products]
    if (discount.free_shipping) {
      this.setState({
        basket: {
          products: basketProducts,
          discounts: [{
            id: discount.id,
            code: discount.code,
            amount: this.state.shippingMethod.total,
          }],
        },
      })
    } else {
      this.setState({
        basket: {
          products: basketProducts,
          discounts: [discount],
        },
      })
    }
  }

  private setCustomerDetails = (customerDetails: PageLayoutState['customerDetails']): void => {
    this.setState({
      customerDetails,
    })
  }

  private setShippingZone = (shippingZone: number): void => {
    this.setState({
      shippingZone,
    })
  }

  private setShippingMethod = (shippingMethod: PageLayoutState['shippingMethod']): void => {
    this.setState({
      shippingMethod,
    })
  }

  private setPaymentMethod = (paymentMethod: PageLayoutState['paymentMethod']): void => {
    this.setState({
      paymentMethod,
    })
  }

  private setOrderId = (orderId: number): void => {
    this.setState({
      orderId,
    })
  }

  private setKlarnaAuthCode = (klarnaAuthCode: string): void => {
    this.setState({
      klarnaAuthCode,
    })
  }

  private setStripeKey = (stripeKey: string): void => {
    this.setState({
      stripeKey,
    })
  }

  private clearCheckoutData = (): void => {
    this.setState({
      shippingZone: 0,
      customerDetails: {
        billing: {
          first_name: '',
          last_name: '',
          company: '',
          email: '',
        },
        shipping: {
          first_name: '',
          last_name: '',
          company: '',
          email: '',
        },
      },
      shippingMethod: {},
      paymentMethod: {},
    })
  }

  render(): ReactNode {
    return (
      <ThemeProvider theme={purcyTheme}>
        <GlobalStyle />
        <If condition={this.state.loaded}>
          <PageContext.Provider
            value={{
              productList: this.state.products,
              toggleModal: this.toggleModal,
              showModal: this.state.showModal,
              preorderStatus: false,
              basket: this.state.basket,
              updateBasket: this.updateBasket,
              clearBasket: this.clearBasket,
              amendQuantity: this.amendQuantity,
              removeFromBasket: this.removeFromBasket,
              totalPrice: this.getTotalPrice(),
              customerDetails: this.state.customerDetails,
              setCustomerDetails: this.setCustomerDetails,
              shippingZone: this.state.shippingZone,
              setShippingZone: this.setShippingZone,
              shippingMethod: this.state.shippingMethod,
              setShippingMethod: this.setShippingMethod,
              paymentMethod: this.state.paymentMethod,
              setPaymentMethod: this.setPaymentMethod,
              orderId: this.state.orderId,
              setOrderId: this.setOrderId,
              klarnaAuthCode: this.state.klarnaAuthCode,
              setKlarnaAuthCode: this.setKlarnaAuthCode,
              clearCheckoutData: this.clearCheckoutData,
              setStripeKey: this.setStripeKey,
              stripeKey: this.state.stripeKey,
              journeyType: this.state.journeyType,
              formId: this.state.formId,
              addBasketDiscount: this.addBasketDiscount,
            }}
          >
            {this.props.children}
          </PageContext.Provider>
        </If>
        <CSSTransition
          in={this.state.showLoader}
          timeout={300}
          classNames='loader'
          unmountOnExit
        >
          <LoadingOverlay loaded={this.state.loaded} />
        </CSSTransition>
      </ThemeProvider>
    )
  }
}
