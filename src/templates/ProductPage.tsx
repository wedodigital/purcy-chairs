import React, { FC, ReactElement, useState, useEffect, useContext } from 'react'
import { graphql } from 'gatsby'

import HeadTags from '@components/HeadTags'
import Header from '@components/Header'
import HeroImage from '@components/HeroImage'
import FeatureColumns from '@components/FeatureColumns'
import Footer from '@components/Footer'
import Tray from '@components/Tray'
import ShopAccordion from '@components/ShopAccordion'
import Video from '@components/Video'
import Banner from '@components/Banner'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

const ProductPage: FC = ({ data }): ReactElement => {
  const pageContext = useContext(PageContext) as PageContextType
  const productData = data.allWpProduct.nodes[0]

  const variations = productData.variations ? productData.variations.nodes.map(variation => variation.databaseId) : [productData.databaseId]
  const [activeProduct, setActiveProduct] = useState(pageContext.productList.find(product => product.databaseId === variations[0]))

  return (
    <>
      <Header />
      <HeroImage
        mobileBackgroundImage={activeProduct.image.sourceUrl}
        gallery={productData.galleryImages.nodes}
        databaseId={productData.databaseId}
        title={productData.name}
        shortDescription={productData.shortDescription}
        variations={variations}
      />
      <FeatureColumns
        title='Features & Benefits'
        columns={productData.productFeatures.features}
      />
      <If condition={productData.productFeatures.videoUrl}>
        <Video videoUrl={productData.productFeatures.videoUrl} />
      </If>
      <ShopAccordion />
      <If condition={productData.productFeatures.secondaryImage}>
        <Banner backgroundImage={productData.productFeatures.secondaryImage.sourceUrl} />
      </If>
      <Footer />
      <Tray
        databaseId={productData.databaseId}
        title={productData.name}
        shortDescription={productData.shortDescription}
        variations={productData.variations ? productData.variations.nodes.map(variation => variation.databaseId) : [productData.databaseId]}
        onSelectItem={setActiveProduct}
      />
    </>
  )
}

export default ProductPage

export const pageQuery = graphql`
  query($id: String!) {
    allWpProduct(filter: { id: { eq: $id } }) {
      nodes {
        databaseId
        name
        shortDescription
        galleryImages {
          nodes {
            sourceUrl
          }
        }
        ... on WpVariableProduct {
          databaseId
          name
          variations {
            nodes {
              databaseId
              name
              regularPrice
              salePrice
            }
          }
          featuredImage {
            node {
              sourceUrl
            }
          }
          crossSell {
            nodes {
              databaseId
            }
          }
        }
        productFeatures {
          features {
            icon {
              sourceUrl
            }
            content
          }
          videoUrl
          secondaryImage {
            sourceUrl
          }
        }
        ... on WpSimpleProduct {
          databaseId
          name
          salePrice
          regularPrice
          featuredImage {
            node {
              sourceUrl
            }
          }
        }
      }
    }
  }
`
