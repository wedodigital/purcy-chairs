import React, { FC, ReactElement } from 'react'
import { graphql } from 'gatsby'

import HeadTags from '@components/HeadTags'
import Header from '@components/Header'
import Footer from '@components/Footer'
import Container from '@components/Container'
import CheckoutPanel from '@components/CheckoutPanel'
import EmailSignup from '@components/EmailSignup'

const LandingPage: FC = ({ data }): ReactElement => {
  const pageData = data.allWpLandingPage.nodes[0]

  return (
    <>
      <HeadTags seo={pageData.seo} />
      <Header showNav={false} />
      <Container width='narrow'>
        <CheckoutPanel isValid>
          <EmailSignup columns={1} formId={pageData.landingPageContent.formId} />
        </CheckoutPanel>
      </Container>
      <Footer showNewsletter={false} />
    </>
  )
}

export default LandingPage

export const pageQuery = graphql`
  query($id: String!) {
    allWpLandingPage(filter: { id: { eq: $id } }) {
      nodes {
        landingPageContent {
          formId
        }
        seo {
          metaDesc
          metaKeywords
          canonical
          opengraphType
          opengraphUrl
          opengraphTitle
          opengraphSiteName
          opengraphPublisher
          opengraphPublishedTime
          opengraphModifiedTime
          opengraphImage {
            sourceUrl
          }
          twitterTitle
          twitterDescription
          title
          twitterImage {
            sourceUrl
          }
          schema {
            articleType
            pageType
            raw
          }
        }
      }
    }
  }
`
