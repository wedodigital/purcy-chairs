import { Theme } from '@themes/purcyTheme/purcyTheme.types'

export interface StyledPageProps {
  theme: Theme;
}
