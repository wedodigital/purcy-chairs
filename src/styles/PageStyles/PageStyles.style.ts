import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { Button } from '@components/Button/styles/Button.style'

import { StyledPageProps } from './PageStyles.style.types'

export const BasketPage = styled.div((): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: flex-start;
`)

export const BasketWrapper = styled.div((props: StyledPageProps): FlattenSimpleInterpolation => css`
  flex-grow: 1;
  margin-right: ${props.theme.spacing.fixed[4]}px;
`)

export const BasketTotal = styled.div((props: StyledPageProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.tertiary};
  padding: ${props.theme.spacing.fixed[4]}px;
  width: 300px;
  flex-grow: 0;
  flex-shrink: 0;
  border-radius: ${props.theme.spacing.fixed[1] / 2}px;
  position: sticky;
  top: 160px;
`)

export const OurStory = styled.div((props: StyledPageProps): FlattenSimpleInterpolation => css`
  padding-top: ${props.theme.spacing.fixed[8]}px;

  ${props.theme.mixins.respondTo.md(css`
    padding-top: ${props.theme.spacing.fixed[10] * 2}px;
    text-align: center;
  `)}
`)

export const SplitContentGroup = styled.div((props: StyledPageProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[8]}px 0;
`)

export const Faqs = styled.div((props: StyledPageProps): FlattenSimpleInterpolation => css`
  padding-top: ${props.theme.spacing.fixed[8]}px;
`)

export const FaqsHeader = styled.div((props: StyledPageProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[8]}px;
  text-align: center;
`)

export const PurchaseSection = styled.div((props: StyledPageProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[10]}px 0;
  background: ${props.theme.colours.tertiary};
`)

export const CouponForm = styled.div((props: StyledPageProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: flex-end;
  gap: ${props.theme.spacing.fixed[2]}px;
  margin-bottom: ${props.theme.spacing.fixed[6]}px;

  input {
    margin-bottom: 0;
  }

  & > *:first-child {
    flex-grow: 1;
  }
`)

export const ValidationMessage = styled.div((props: StyledPageProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  gap: ${props.theme.spacing.fixed[1]}px;
  margin-top: -${props.theme.spacing.fixed[4]}px;
  margin-bottom: ${props.theme.spacing.fixed[6]}px;
`)

export const SplitButtons = styled.div((props: StyledPageProps): FlattenSimpleInterpolation => css`
  width: 100%;
  display: flex;
  gap: ${props.theme.spacing.fixed[4]}px;
  flex-wrap: wrap;

  ${Button} {
    flex-grow: 1;
  }
`)
