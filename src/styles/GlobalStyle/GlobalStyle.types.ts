import { Theme } from '@themes/socialBuff.types';

export interface GlobalStyleProps {
  theme: Theme;
}
