import React from 'react'
import { graphql } from 'gatsby'

import Header from '@components/Header'
import Footer from '@components/Footer'
import Tray from '@components/Tray'
import Heading from '@components/Heading'
import Paragraph from '@components/Paragraph'
import Container from '@components/Container'
import ContactBanner from '@components/ContactBanner'
import CollapsibleContainer from '@components/CollapsibleContainer'
import RawHtmlWrapper from '@components/RawHtmlWrapper'
import HeadTags from '@components/HeadTags'

import * as Styled from '../styles/PageStyles/PageStyles.style'

const SupportPage = ({ data }) => {
  const pageFields = data.wpPage.supportPage
  return (
    <>
      <HeadTags seo={data.wpPage.seo} />
      <Header />
      <ContactBanner
        title={pageFields.heading}
        content={pageFields.content}
        email={pageFields.emailAddress}
        telephone={pageFields.telephoneNumber}
      />
      <Styled.Faqs>
        <Container>
          <Styled.FaqsHeader>
            <Heading text='FAQs' size={5} weight={3} noMargin />
            <Paragraph text={`Here are some of the questions we're regularly asked`} />
          </Styled.FaqsHeader>
          {
            pageFields.faqs.map((faq) => {
              return (
                <CollapsibleContainer title={faq.question}>
                  <RawHtmlWrapper content={faq.answer} />
                </CollapsibleContainer>
              )
            })
          }
        </Container>
      </Styled.Faqs>
      <Footer />
      <Tray />
    </>
  )
}

export default SupportPage

export const pageQuery = graphql`
  query SupportPage {
    wpPage(databaseId: {eq: 54}) {
      seo {
        metaDesc
        metaKeywords
        canonical
        opengraphType
        opengraphUrl
        opengraphTitle
        opengraphSiteName
        opengraphPublisher
        opengraphPublishedTime
        opengraphModifiedTime
        opengraphImage {
          sourceUrl
        }
        twitterTitle
        twitterDescription
        title
        twitterImage {
          sourceUrl
        }
        schema {
          articleType
          pageType
          raw
        }
      }
      supportPage {
        content
        emailAddress
        faqs {
          answer
          question
        }
        heading
        telephoneNumber
      }
    }
  }
`
