import React, { FC, ReactElement, useEffect, useContext, useState } from 'react'

import Confirmation from '@components/Confirmation'
import Container from '@components/Container'
import Stepper from '@components/Stepper'
import Header from '@components/Header'
import Footer from '@components/Footer'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

const ConfirmationPage: FC = ({ location }): ReactElement => {
  const pageContext = useContext(PageContext) as PageContextType
  const [orderStatus, setOrderStatus] = useState('')

  // https://purcychair.com/checkout/confirmation/?orderId=898&payment_intent=pi_3KdLCICiud8mXhhV0beNze2S&payment_intent_client_secret=pi_3KdLCICiud8mXhhV0beNze2S_secret_o8tJ00D40q5AHYeQCCV0PKXqQ&redirect_status=failed&source_type=klarna

  useEffect(() => {
    const searchParams = new URLSearchParams(location.search)
    const orderId = searchParams.get('orderId')
    const status = searchParams.get('redirect_status')
    setOrderStatus(status)
    if (status !== 'failed') {
      fetch('https://purcy-api-app.herokuapp.com/purcy/create-order', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          set_paid: true,
          status: 'completed',
          payment_method: pageContext.paymentMethod.id,
          payment_method_title: pageContext.paymentMethod.title,
          billing: pageContext.customerDetails.billing,
          shipping: pageContext.customerDetails.shipping,
          shipping_lines: [{
            method_id: pageContext.shippingMethod.method_id,
            method_title: pageContext.shippingMethod.method_title,
            total: `${pageContext.shippingMethod.total}`,
          }],
          line_items: pageContext.basket.products.map((basketItem) => {
            return {
              product_id: basketItem.product.databaseId,
              quantity: basketItem.quantity,
            }
          }),
          coupon_lines: pageContext.basket.discounts.map((discount) => {
            return {
              id: discount.id,
              code: discount.code,
              discount: discount.amount,
            }
          }),
        }),
      })
      if (window && window.fbq) {
        window.fbq('track', 'Purchase', {
          currency: 'GBP',
          value: pageContext.totalPrice + pageContext.shippingMethod.total,
        })
      }
      pageContext.clearBasket()
      pageContext.setKlarnaAuthCode('')
      pageContext.clearCheckoutData()
    }
  }, [])

  return (
    <>
      <Header />
      <Container width='narrow' verticalPadding>
        <Stepper totalSteps={6} currentStep={6} />
        <Confirmation orderStatus={orderStatus} />
      </Container>
      <Footer />
    </>
  )
}

export default ConfirmationPage
