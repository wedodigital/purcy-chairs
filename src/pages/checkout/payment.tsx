import React, { FC, ReactElement, useContext, useState, useEffect } from 'react'
import { loadStripe } from '@stripe/stripe-js'
import { Elements } from '@stripe/react-stripe-js'

import Payments from '@components/Payments'
import Container from '@components/Container'
import Stepper from '@components/Stepper'
import Header from '@components/Header'
import Footer from '@components/Footer'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

const PaymentPage: FC = ({ location }): ReactElement => {
  const pageContext = useContext(PageContext) as PageContextType
  const stripePromise = loadStripe('pk_live_51JzIE1Ciud8mXhhVqMMHK7tC5B1Hw4O19UpCu5nu91jGffCSndEpXfg13fhdUJvLx9VU8z5gJ3S1njyR0vgGFYGF00gCCYVduv')
  // const stripePromise = loadStripe('pk_test_51JzIE1Ciud8mXhhVQ3jotSyZjMJ7Z72Cwyrk0zQc5ufK722h6geUHxutCm5jvshQfZdyzeeAtvhaAP7t5Bcogrv200oY6KvSam')
  const [stripeKey, setStripeKey] = useState('')
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    fetch('https://purcy-api-app.herokuapp.com/purcy/stripe/create-payment-intent', {
      method: 'POST',
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        billingAddress: pageContext.customerDetails.billing,
        totalPrice: Math.round(pageContext.totalPrice * 100)
      })
    })
      .then((res) => res.json())
      .then((data) => {
        setStripeKey(data.clientSecret)
        setLoading(false)
      })
  }, [])

  return (
    <>
      <Header />
      <Container width='narrow' verticalPadding>
        <Stepper totalSteps={6} currentStep={6} />
        {stripeKey &&
          <Elements options={{ clientSecret: stripeKey }} stripe={stripePromise}>
            <Payments isLoading={loading} clientSecret={stripeKey} originUrl={location.origin} />
          </Elements>
        }
      </Container>
      <Footer />
    </>
  )
}

export default PaymentPage
