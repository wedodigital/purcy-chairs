import React, {
  FC,
  ReactElement,
  useContext,
  useEffect,
  useState,
} from "react";
import { Formik, Form } from "formik";
import { navigate } from "gatsby";

import CheckoutButtonWrapper from "@components/CheckoutButtonWrapper";
import CheckoutPanel from "@components/CheckoutPanel";
import Button from "@components/Button";
import Radio from "@components/Radio";
import Stepper from "@components/Stepper";
import Container from "@components/Container";
import Header from "@components/Header";
import Footer from "@components/Footer";
import Paragraph from "@components/Paragraph";

import PageContext from "@context/PageContext";
import { PageContextType } from "@context/PageContext.types";

const ShippingPage: FC = (): ReactElement => {
  const checkoutContext = useContext(PageContext) as PageContextType;
  const [loading, setLoading] = useState(true);

  const [shippingMethods, setShippingMethods] = useState([]);
  const [selectedShippingMethod, setSelectedShippingMethod] = useState({
    method_title: "",
  });
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    fetch("https://purcy-api-app.herokuapp.com/purcy/shipping-rates", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        shippingAddress: checkoutContext.customerDetails.shipping,
        basket: checkoutContext.basket.products,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.status === "400") {
          setErrorMessage(data.detail);
        } else {
          data.forEach((element) => {
            const orderedShippingMethods =
              element &&
              element.products.sort((a, b) => {
                if (a.totalPrice[0].price < b.totalPrice[0].price) return -1;
                if (a.totalPrice[0].price > b.totalPrice[0].price) return 1;
                return 0;
              });


            var method = "Express Shipping";
            if (orderedShippingMethods[0].productCode == "H") {
              method = "By Road Services";
            }
            shippingMethods.push({
              method_id: orderedShippingMethods[0].productCode,
              method_title: method,
              total: Math.ceil(
                orderedShippingMethods[0].totalPrice[0].price
              ).toFixed(2),
            });

            setSelectedShippingMethod({
              method_id: orderedShippingMethods[0].productCode,
              method_title: method,
              total: Math.ceil(
                orderedShippingMethods[0].totalPrice[0].price
              ).toFixed(2),
            });
          });
        }
        setLoading(false);
      })
      .catch((error) => {
        setErrorMessage(error.detail);
        setLoading(false);
      });
  }, []);

  return (
    <>
      <Header />
      <Container width="narrow" verticalPadding>
        <Stepper totalSteps={6} currentStep={4} />
        <Formik
          initialValues={{
            billing: checkoutContext.customerDetails.billing,
            shipping: checkoutContext.customerDetails.shipping,
            shipping_lines: [],
          }}
          onSubmit={(values) => {
            values.shipping_lines.push(selectedShippingMethod);
            checkoutContext.setShippingMethod(selectedShippingMethod);
            checkoutContext.setCustomerDetails(values);
            setLoading(true);
            navigate("/checkout/voucher-code");
          }}
        >
          <Form>
            <CheckoutPanel
              title="Shipping Method."
              loading={loading}
              isValid={true}
            >
              <Choose>
                <When condition={errorMessage}>
                  <Paragraph text={`Error: ${errorMessage}`} />
                  <Button
                    type="button"
                    text="Go back"
                    appearance="secondary"
                    size={1}
                    onInteraction={() => navigate(-1)}
                  />
                </When>
                <Otherwise>
                  {shippingMethods.map((shippingMethod, index) => {
                    // shippingMethod = shippingMethod[0];
                    return (
                      <Radio
                        name="shipping_lines"
                        value={shippingMethod.method_title}
                        title={shippingMethod.method_title}
                        checked={
                          shippingMethod.method_title ===
                          selectedShippingMethod.method_title
                        }
                        onChange={(event) => {
                          setSelectedShippingMethod(
                            shippingMethods.find(
                              (method) =>
                                method.method_title === event.target.value
                            )
                          );
                        }}
                        price={shippingMethod.total}
                      />
                    );
                  })}
                </Otherwise>
              </Choose>
            </CheckoutPanel>
            <CheckoutButtonWrapper>
              <Button type="submit" text="Continue" />
            </CheckoutButtonWrapper>
          </Form>
        </Formik>
      </Container>
      <Footer />
    </>
  );
};

export default ShippingPage;
