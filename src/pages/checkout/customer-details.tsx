import React, { FC, ReactElement, useContext, useState, useEffect } from 'react'
import { CSSTransition } from 'react-transition-group'
import { Formik, Form, Field } from 'formik'
import { navigate } from 'gatsby'
import * as Yup from 'yup'

import CheckoutButtonWrapper from '@components/CheckoutButtonWrapper'
import Button from '@components/Button'
import CheckoutPanel from '@components/CheckoutPanel'
import Container from '@components/Container'
import Stepper from '@components/Stepper'
import Header from '@components/Header'
import Footer from '@components/Footer'
import Paragraph from '@components/Paragraph'
import Validation from '@components/Validation'
import Modal from '@components/Modal'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

import * as Styled from '../../styles/PageStyles/PageStyles.style'

const CustomerDetailsPage: FC = (): ReactElement => {
  const checkoutContext = useContext(PageContext) as PageContextType
  const [loading, setLoading] = useState(false)
  const [checkoutType, setCheckoutType] = useState()

  const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

  const validation = Yup.object().shape({
    billing: Yup.object().shape({
      first_name: Yup.string().required('Required field'),
      last_name: Yup.string().required('Required field'),
      email: Yup.string().email('Please enter a valid email address').required('Required field'),
      phone: Yup.string().matches(phoneRegExp, 'Please enter a valid telephone number').required('Required field'),
    })
  })

  useEffect(() => {
    if (window && window.fbq) {
      window.fbq('track', 'InitiateCheckout')
    }
  }, [])

  return (
    <>
      <Header />
      <Container width='narrow' verticalPadding>
        <Stepper totalSteps={6} currentStep={1} />
        <Formik
          initialValues={{
            billing: checkoutContext.customerDetails.billing,
            shipping: checkoutContext.customerDetails.shipping,
          }}
          validateOnBlur={false}
          validateOnChange={false}
          onSubmit={(values) => {
            checkoutContext.setCustomerDetails(values)
            setLoading(true)
            navigate('/checkout/billing-address')
          }}
          validationSchema={validation}
        >
          {(props) => (

            <Form>
              {console.log(props)}
              <CheckoutPanel title={checkoutType === 'stripe' ? 'Your Details.' : 'Select Checkout Type.'} loading={loading} isValid={props.isValid}>
                <Choose>
                  <When condition={checkoutType === 'stripe'}>
                    <Validation validationMessage={props.errors.billing?.first_name}>
                      <label htmlFor="billing.first_name">First Name*</label>
                      <Field value={props.values.shipping.first_name} onChange={(event) => {props.setFieldValue('shipping.first_name', event.target.value), props.setFieldValue('billing.first_name', event.target.value)}} />
                    </Validation>
                    <Validation validationMessage={props.errors.billing?.last_name}>
                      <label htmlFor="billing.last_name">Last Name*</label>
                      <Field value={props.values.shipping.last_name} onChange={(event) => {props.setFieldValue('shipping.last_name', event.target.value), props.setFieldValue('billing.last_name', event.target.value)}} />
                    </Validation>
                    <label htmlFor="billing.company">Company Name</label>
                    <Field value={props.values.shipping.company} onChange={(event) => {props.setFieldValue('shipping.company', event.target.value), props.setFieldValue('billing.company', event.target.value)}} />
                    <Validation validationMessage={props.errors.billing?.email}>
                      <label htmlFor="billing.email">Email*</label>
                      <Field id="billing.email" name="billing.email" type="email" />
                    </Validation>
                    <Validation validationMessage={props.errors.billing?.phone}>
                      <label htmlFor="billing.phone">Telephone*</label>
                      <Field id="billing.phone" name="billing.phone" type="tel" />
                    </Validation>
                    <Paragraph size={1} text='* = Required field' />
                  </When>
                  <Otherwise>
                    <Styled.SplitButtons>
                      <Button onInteraction={() => setCheckoutType('stripe')} text='Checkout with Stripe' />
                      <Button onInteraction={() => checkoutContext.toggleModal()} text='Buy now with PayL8r' />
                    </Styled.SplitButtons>
                  </Otherwise>
                </Choose>
              </CheckoutPanel>
              <If condition={checkoutType === 'stripe'}>
                <CheckoutButtonWrapper>
                  <Button type='submit' text='Continue' />
                </CheckoutButtonWrapper>
              </If>
            </Form>
          )}
        </Formik>
      </Container>
      <CSSTransition
        in={checkoutContext.showModal}
        timeout={500}
        classNames='modal'
        unmountOnExit
      >
        <Modal>
          <iframe src='https://2purcychair.payl8r.com/checkout' frameBorder='0' width='100%' height='600'></iframe>
        </Modal>
      </CSSTransition>
      <Footer />
    </>
  )
}

export default CustomerDetailsPage
