import React, { FC, ReactElement, useContext, useState } from 'react'
import { Formik, Form, Field } from 'formik'
import { navigate } from 'gatsby'
import * as Yup from 'yup'

import CheckoutButtonWrapper from '@components/CheckoutButtonWrapper'
import CheckoutPanel from '@components/CheckoutPanel'
import Button from '@components/Button'
import Container from '@components/Container'
import Stepper from '@components/Stepper'
import Header from '@components/Header'
import Footer from '@components/Footer'
import Paragraph from '@components/Paragraph'
import Validation from '@components/Validation'
import Countries from '@components/Countries'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

const ShippingAddressPage: FC = (): ReactElement => {
  const checkoutContext = useContext(PageContext) as PageContextType
  const [loading, setLoading] = useState(false)

  const validation = Yup.object().shape({
    shipping: Yup.object().shape({
      address_1: Yup.string().required('Required field'),
      city: Yup.string().required('Required field'),
      state: Yup.string().required('Required field'),
      postcode: Yup.string().required('Required field'),
      country: Yup.string().required('Required field'),
    })
  })

  return (
    <>
      <Header />
      <Container width='narrow' verticalPadding>
        <Stepper totalSteps={6} currentStep={3} />
        <Formik
          initialValues={{
            billing: checkoutContext.customerDetails.billing,
            shipping: {
              ...checkoutContext.customerDetails.shipping,
            }
          }}
          validateOnBlur={false}
          validateOnChange={false}
          onSubmit={(values) => {
            checkoutContext.setCustomerDetails(values)
            setLoading(true)
            navigate('/checkout/shipping-method')
          }}
          validationSchema={validation}
        >
          {props => (
            <Form>
              <CheckoutPanel title='Shipping Address.' loading={loading} isValid={props.isValid}>
                <If condition={props.values.shipping.country !== 0}>
                  <div className='copy-button'>
                    <Button
                      type='button'
                      text='Copy from Billing Address'
                      appearance='secondary'
                      size={1}
                      onInteraction={() => {
                        props.setFieldValue('shipping.address_1', props.values.billing.address_1)
                        props.setFieldValue('shipping.address_2', props.values.billing.address_2)
                        props.setFieldValue('shipping.city', props.values.billing.city)
                        props.setFieldValue('shipping.state', props.values.billing.state)
                        props.setFieldValue('shipping.postcode', props.values.billing.postcode)
                        props.setFieldValue('shipping.country', props.values.billing.country)
                      }}
                    />
                  </div>
                  <Validation validationMessage={props.errors.shipping?.address_1}>
                    <label htmlFor="shipping.address_1">Address Line 1*</label>
                    <Field id="shipping.address_1" name="shipping.address_1" />
                  </Validation>
                  <label htmlFor="shipping.address_2">Address Line 2</label>
                  <Field id="shipping.address_2" name="shipping.address_2" />
                  <Validation validationMessage={props.errors.shipping?.city}>
                    <label htmlFor="shipping.city">Town*</label>
                    <Field id="shipping.city" name="shipping.city" />
                  </Validation>
                  <Validation validationMessage={props.errors.shipping?.state}>
                    <label htmlFor="shipping.state">County*</label>
                    <Field id="shipping.state" name="shipping.state" />
                  </Validation>
                  <Validation validationMessage={props.errors.shipping?.postcode}>
                    <label htmlFor="shipping.postcode">Postcode*</label>
                    <Field id="shipping.postcode" name="shipping.postcode" />
                  </Validation>
                  <Validation validationMessage={props.errors.shipping?.country}>
                    <label htmlFor="shipping.country">Country*</label>
                    <Field
                      id="shipping.country"
                      name="shipping.country"
                      as='select'
                      value={props.values.shipping.country}
                      onChange={() => {
                        props.setFieldValue('shipping.country', event.target.value)
                      }}
                    >
                      <Countries />
                    </Field>
                  </Validation>
                </If>
                <Paragraph size={1} text='* = Required field' />
              </CheckoutPanel>
              <CheckoutButtonWrapper>
                <Button type='submit' text='Continue' />
              </CheckoutButtonWrapper>
            </Form>
          )}
        </Formik>
      </Container>
      <Footer />
    </>
  )
}

export default ShippingAddressPage
