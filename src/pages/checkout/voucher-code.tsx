import React, { FC, ReactElement, useContext, useState, useEffect } from 'react'
import { Formik, Form, Field } from 'formik'
import { navigate } from 'gatsby'
import * as Yup from 'yup'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExclamationCircle } from '@fortawesome/free-solid-svg-icons'

import CheckoutButtonWrapper from '@components/CheckoutButtonWrapper'
import Button from '@components/Button'
import CheckoutPanel from '@components/CheckoutPanel'
import Container from '@components/Container'
import Stepper from '@components/Stepper'
import Header from '@components/Header'
import Footer from '@components/Footer'
import Paragraph from '@components/Paragraph'
import Validation from '@components/Validation'
import SimpleBasket from '@components/SimpleBasket'

import * as Styled from '@styles/PageStyles/PageStyles.style'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

const CustomerDetailsPage: FC = (): ReactElement => {
  const pageContext = useContext(PageContext) as PageContextType
  const [loading, setLoading] = useState(false)
  const [couponError, setCouponError] = useState('')
  const [coupon, setCoupon] = useState()

  useEffect(() => {
    if (!coupon) return
    if (coupon.free_shipping) {
      pageContext.addBasketDiscount(coupon)
      return
    }
    if (coupon.usage_limit && coupon.usage_limit === coupon.usage_count) {
      setCouponError('That coupon code has already been used. Please try another')
      return
    }
    if (coupon.product_ids.length) {
      const couponCheck = []
      coupon.product_ids.forEach((productId) => {
        const validBasketItem = pageContext.basket.products.some((basketProduct) => basketProduct.product.databaseId === productId)
        couponCheck.push(validBasketItem)
      })
      if (couponCheck.every(item => item === false)) {
        setCouponError('That coupon code can not be used with any item currently in your basket. Please try another')
        return
      }
      pageContext.addBasketDiscount(coupon)
    }
  }, [coupon])

  return (
    <>
      <Header />
      <Container width='narrow' verticalPadding>
        <Stepper totalSteps={6} currentStep={5} />
        <Formik
          initialValues={{
            billing: pageContext.customerDetails.billing,
            shipping: pageContext.customerDetails.shipping,
            couponCode: '',
          }}
          validateOnBlur={false}
          validateOnChange={false}
          onSubmit={(values) => {
            // pageContext.setCustomerDetails(values)
            setLoading(true)
            // navigate('/checkout/billing-address')
            fetch(`https://purcy-api-app.herokuapp.com/purcy/coupons/${values.couponCode}`)
              .then(res => res.json())
              .then((data) => {
                setCoupon(data)
                setLoading(false)
              })
              .catch((error) => {
                setCouponError('Voucher code not valid, please try another')
                setLoading(false)
              })
          }}
          // validationSchema={validation}
        >
          {(props) => (
            <Form>
              <CheckoutPanel title='Order Summary.' loading={loading} isValid={true}>
                <Styled.CouponForm>
                  <div>
                    <label htmlFor="couponCode">Got a voucher code? Enter it here:</label>
                    <Field id="couponCode" name="couponCode" type="text" />
                  </div>
                  <Button type='submit' text='Apply' />
                </Styled.CouponForm>
                <If condition={couponError}>
                  <Styled.ValidationMessage>
                    <FontAwesomeIcon icon={faExclamationCircle} />
                    <span>{couponError}</span>
                  </Styled.ValidationMessage>
                </If>
                <SimpleBasket basket={pageContext.basket} shippingMethod={pageContext.shippingMethod} />
              </CheckoutPanel>
            </Form>
          )}
        </Formik>
        <CheckoutButtonWrapper>
          <Button to='/checkout/payment' text='Continue' />
        </CheckoutButtonWrapper>

      </Container>
      <Footer />
    </>
  )
}

export default CustomerDetailsPage
