import React, { FC, ReactElement, useContext, useState } from 'react'
import { Formik, Form, Field } from 'formik'
import { navigate } from 'gatsby'
import * as Yup from 'yup'

import CheckoutButtonWrapper from '@components/CheckoutButtonWrapper'
import CheckoutPanel from '@components/CheckoutPanel'
import Button from '@components/Button'
import Countries from '@components/Countries'
import Container from '@components/Container'
import Stepper from '@components/Stepper'
import Header from '@components/Header'
import Footer from '@components/Footer'
import Paragraph from '@components/Paragraph'
import Validation from '@components/Validation'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

const BillingAddressPage: FC = (): ReactElement => {
  const checkoutContext = useContext(PageContext) as PageContextType
  const [loading, setLoading] = useState(false)

  const validation = Yup.object().shape({
    billing: Yup.object().shape({
      address_1: Yup.string().required('Required field'),
      city: Yup.string().required('Required field'),
      state: Yup.string().required('Required field'),
      postcode: Yup.string().required('Required field'),
      country: Yup.string().required('Required field'),
    })
  })

  return (
    <>
      <Header />
      <Container width='narrow' verticalPadding>
        <Stepper totalSteps={6} currentStep={2} />
        <Formik
          initialValues={{
            billing: checkoutContext.customerDetails.billing,
            shipping: checkoutContext.customerDetails.shipping,
          }}
          onSubmit={(values) => {
            checkoutContext.setCustomerDetails(values)
            setLoading(true)
            navigate('/checkout/shipping-address')
          }}
          validateOnBlur={false}
          validateOnChange={false}
          validationSchema={validation}
        >
          {props => (
            <Form>
              <CheckoutPanel title='Billing Address.' loading={loading} isValid={props.isValid}>
                <Validation validationMessage={props.errors.billing?.address_1}>
                  <label htmlFor="billing.address_1">Address Line 1*</label>
                  <Field id="billing.address_1" name="billing.address_1" />
                </Validation>
                <label htmlFor="billing.address_2">Address Line 2</label>
                <Field id="billing.address_2" name="billing.address_2" />
                <Validation validationMessage={props.errors.billing?.city}>
                  <label htmlFor="billing.city">Town*</label>
                  <Field id="billing.city" name="billing.city" />
                </Validation>
                <Validation validationMessage={props.errors.billing?.state}>
                  <label htmlFor="billing.state">County*</label>
                  <Field id="billing.state" name="billing.state" />
                </Validation>
                <Validation validationMessage={props.errors.billing?.postcode}>
                  <label htmlFor="billing.postcode">Postcode*</label>
                  <Field id="billing.postcode" name="billing.postcode" />
                </Validation>
                <Validation validationMessage={props.errors.billing?.country}>
                  <label htmlFor="billing.country">Country*</label>
                  <Field id="billing.country" name="billing.country" as='select'>
                    <Countries />
                  </Field>
                </Validation>
                <Paragraph size={1} text='* = Required field' />
              </CheckoutPanel>
              <CheckoutButtonWrapper>
                <Button type='submit' text='Continue' />
              </CheckoutButtonWrapper>
            </Form>
          )}
        </Formik>
      </Container>
      <Footer />
    </>
  )
}

export default BillingAddressPage
