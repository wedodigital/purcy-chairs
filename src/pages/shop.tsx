import React, { useState, useContext } from 'react'
import { graphql } from 'gatsby'

import Header from '@components/Header'
import Footer from '@components/Footer'
import ShopHero from '@components/ShopHero'
import BellyBand from '@components/BellyBand'
// import KlarnaBand from '@components/KlarnaBand'
import Video from '@components/Video'
import ShopAccordion from '@components/ShopAccordion'
import HeadTags from '@components/HeadTags'
import HeroImage from '@components/HeroImage'
import FeatureColumns from '@components/FeatureColumns'
import Tray from '@components/Tray'

import PageContext from '@context/PageContext'
import { PageContextType } from '@context/PageContext.types'

const ShopPage = ({ data }) => {
  const pageContext = useContext(PageContext) as PageContextType
  const pageFields = data.wpPage.shopPageFields
  const reviews = data.wpPage.shopPageFields.reviews
  const productData = data.allWpProduct.nodes[0]

  let totalRating = 0
  let averageRating = 0

  if (reviews) {
    reviews.forEach((review) => {
      totalRating = totalRating + parseInt(review.rating)
    })

    averageRating = (totalRating / reviews?.length)
  }

  const variations = productData.variations ? productData.variations.nodes.map(variation => variation.databaseId) : [productData.databaseId]
  const [activeProduct, setActiveProduct] = useState(pageContext.productList.find(product => product.databaseId === variations[0]))

  console.log(activeProduct)

  return (
    <>
      <HeadTags seo={data.wpPage.seo} />
      <Header formEndpoint='https://www.getdrip.com/forms/982513069/submissions' />
      <HeroImage
        mobileBackgroundImage={activeProduct.image.sourceUrl}
        gallery={productData.galleryImages.nodes}
        databaseId={productData.databaseId}
        title={productData.name}
        variations={productData.variations.nodes.map(variation => variation.databaseId)}
      />
      <ShopHero
        headline={pageFields.headline}
        icons={pageFields.icons}
        introContent={[pageFields.introHeading, pageFields.introText]}
      />
      <BellyBand
        heading={pageFields.secondaryHeading}
        content={pageFields.secondaryContent}
        image={pageFields.image.sourceUrl}
        featuredPublications={pageFields.featuredPublications}
      />
      <FeatureColumns
        title='Features & Benefits'
        columns={pageFields.features}
      />
      <Video videoUrl={pageFields.videoUrl} />
      <ShopAccordion showFaqs showReviews showDelivery />
      <Footer />
      <Tray onSelectItem={setActiveProduct} />
    </>
  )
}

export default ShopPage

export const shopQuery = graphql`
  query ShopPage {
    allWpProduct(filter: {databaseId: {eq: 11}}) {
      nodes {
        name
        databaseId
        galleryImages {
          nodes {
            sourceUrl
          }
        }
        ... on WpVariableProduct {
          variations {
            nodes {
              databaseId
            }
          }
          featuredImage {
            node {
              sourceUrl
            }
          }
        }
      }
    }
    wpPage(databaseId: {eq: 7}) {
      seo {
        metaDesc
        metaKeywords
        canonical
        opengraphType
        opengraphUrl
        opengraphTitle
        opengraphSiteName
        opengraphPublisher
        opengraphPublishedTime
        opengraphModifiedTime
        opengraphImage {
          sourceUrl
        }
        twitterTitle
        twitterDescription
        title
        twitterImage {
          sourceUrl
        }
        schema {
          articleType
          pageType
          raw
        }
      }
      shopPageFields {
        darkGallery {
          image {
            sourceUrl
          }
        }
        lightGallery {
          image {
            sourceUrl
          }
        }
        additionalMessaging
        headline
        icons {
          icon {
            sourceUrl
          }
          title
        }
        introHeading
        introText
        secondaryHeading
        secondaryContent
        featuredPublications {
          logo {
            sourceUrl
          }
        }
        image {
          sourceUrl
        }
        features {
          content
          icon {
            sourceUrl
          }
        }
        videoUrl
        reviews {
          rating
        }
        klarnaBreakdown
      }
    }
  }
`
