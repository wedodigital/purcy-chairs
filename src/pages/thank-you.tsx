import React from 'react'

import Header from '@components/Header'
import Footer from '@components/Footer'
import ThankYou from '@components/ThankYou'

const ThankyouPage = () => {
  return (
    <>
      <Header formEndpoint='https://www.getdrip.com/forms/982513069/submissions' />
      <ThankYou />
      <Footer formEndpoint='https://www.getdrip.com/forms/982513069/submissions' />
    </>
  )
}

export default ThankyouPage
