import React from 'react'
import { graphql } from 'gatsby'

import Header from '@components/Header'
import HeroVideo from '@components/HeroVideo'
import HeadTags from '@components/HeadTags'

const IndexPage = ({ data, location }) => {
  const videoFormats = {
    webm: data.wpPage.homepage.webm?.mediaItemUrl,
    mp4: data.wpPage.homepage.mp4?.mediaItemUrl,
  }

  return (
    <>
      <HeadTags seo={data.wpPage.seo} />
      <meta name="google-site-verification" content="zTvh1AkYhRMt1lZQSIvzQTCS-58IK15MYmtCGXxUqK8" />
      <Header appearance='secondary' />
     
      <HeroVideo
        videoSrc={videoFormats}
        title={data.wpPage.homepage.mainTitle}
        poster={data.wpPage.homepage.poster.sourceUrl}
        featureImage={data.wpPage.homepage.featureImage}
      />
    </>
  )
}

export default IndexPage

export const pageQuery = graphql`
  query Homepage {
    wpPage(databaseId: {eq: 2}) {
      seo {
        metaDesc
        metaKeywords
        canonical
        opengraphType
        opengraphUrl
        opengraphTitle
        opengraphSiteName
        opengraphPublisher
        opengraphPublishedTime
        opengraphModifiedTime
        opengraphImage {
          sourceUrl
        }
        twitterTitle
        twitterDescription
        title
        twitterImage {
          sourceUrl
        }
        schema {
          articleType
          pageType
          raw
        }
      }
      homepage {
        mainTitle
        mp4 {
          mediaItemUrl
        }
        webm {
          mediaItemUrl
        }
        poster {
          sourceUrl
        }
        featureImage {
          sourceUrl
        }
      }
    }
  }
`
