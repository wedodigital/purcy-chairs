import React, { FC, ReactElement, useContext } from 'react'

import Basket from '@components/Basket'
import Header from '@components/Header'
import Footer from '@components/Footer'
import HeadTags from '@components/HeadTags'

const BasketPage: FC = (): ReactElement => {
  return (
    <>
      <Header />
      <Basket />
      <Footer />
    </>
  )
}

export default BasketPage
