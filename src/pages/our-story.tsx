import React, { ReactElement} from 'react'
import { graphql } from 'gatsby'

import Header from '@components/Header'
import Footer from '@components/Footer'
import Tray from '@components/Tray'
import Container from '@components/Container'
import Heading from '@components/Heading'
import SplitContent from '@components/SplitContent'
import Banner from '@components/Banner'
import HeadTags from '@components/HeadTags'

import { SplitContentProps } from '@components/SplitContent/SplitContent.types'

import * as Styled from '../styles/PageStyles/PageStyles.style'

const OurStoryPage = ({ data }) => {
  const pageFields = data.wpPage.ourStory
  return (
    <>
      <HeadTags seo={data.wpPage.seo} />
      <Header />
      <Styled.OurStory>
        <Container width='mid'>
          <Heading size={6} weight={3} text={pageFields.primaryHeading} />
          <Banner
            backgroundImage={pageFields.primaryImage?.sourceUrl}
          />
        </Container>
        <If condition={pageFields.splitContentSections}>
          <Styled.SplitContentGroup>
          {
            pageFields.splitContentSections?.map((splitContentSection: SplitContentProps): ReactElement => {
              return (
                <SplitContent
                  orientation={splitContentSection.orientation}
                  content={splitContentSection.content}
                  title={splitContentSection.title}
                  image={splitContentSection.image?.sourceUrl}
                />
              )
            })
          }
          </Styled.SplitContentGroup>
        </If>
      </Styled.OurStory>
      <Banner
        backgroundImage={pageFields.secondaryImage?.sourceUrl}
        heading={pageFields.secondaryHeading}
        buttonUrl='/shop'
        buttonText='Shop Now'
      />
      <Footer />
      <Tray />
    </>
  )
}

export default OurStoryPage

export const pageQuery = graphql`
  query OurStoryPage {
    wpPage(databaseId: {eq: 51}) {
      seo {
        metaDesc
        metaKeywords
        canonical
        opengraphType
        opengraphUrl
        opengraphTitle
        opengraphSiteName
        opengraphPublisher
        opengraphPublishedTime
        opengraphModifiedTime
        opengraphImage {
          sourceUrl
        }
        twitterTitle
        twitterDescription
        title
        twitterImage {
          sourceUrl
        }
        schema {
          articleType
          pageType
          raw
        }
      }
      ourStory {
        primaryHeading
        primaryImage {
          sourceUrl
        }
        splitContentSections {
          content
          orientation
          title
          image {
            sourceUrl
          }
        }
        secondaryHeading
        secondaryImage {
          sourceUrl
        }
      }
    }
  }
`
