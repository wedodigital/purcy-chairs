import React from 'react'

import { PageContextType } from './PageContext.types'

export default React.createContext<Partial<PageContextType>>({})
