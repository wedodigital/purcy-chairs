export interface ProductDetails {
  databaseId: number
  name: string
  regularPrice: number
  salePrice: number
  description: string
  stockQuantity: number
  image: {
    sourceUrl: string
  }
  dimensions: {
    length: number
    width: number
    height: number
  }
  weight: number
  onSale?: boolean
  packages?: {
    weight: number
    dimensions: {
      length: number
      width: number
      height: number
    }
  }[]
}

export interface BasketItem {
  product: ProductDetails
  quantity: number
}

export interface PageContextType {
  setActiveProduct: (product: ProductDetails) => void
  productList: ProductDetails[]
  activeProduct: ProductDetails
  toggleModal: (showModal?: boolean) => void
  showModal: boolean
  preorderStatus: boolean
  basket: BasketItem[]
  totalPrice: number
  updateBasket: (product: ProductDetails, quantity: number) => void
  amendQuantity: (product: ProductDetails, changeType: 'increase' | 'decrease') => void
  removeFromBasket: (product: ProductDetails) => void
  clearBasket: () => void
  setCheckoutStep: (index: number) => void
  setCustomerDetails: ({}: any) => void
  shippingZone: number
  setShippingZone: (shippingZone: number) => void
  customerDetails: {
    billing: {}
    shipping: {}
  }
  shippingMethod: {
    method_id: string
    method_title: string
    total: number
  } | {}
  paymentMethod: {
    id: string,
    title: string,
  } | {}
  setShippingMethod: (shippingMethod: any) => void
  orderId: number
  setOrderId: (orderId: number) => void
  klarnaAuthCode?: string
  setKlarnaAuthCode: (klarnaAuthCode: string) => void
  clearCheckoutData: () => void
  stripeKey?: string
  setStripeKey: (stripeKey: string) => void
  setPaymentMethod: (paymentMethod: any) => void
  journeyType: string
}
