/* eslint-disable */
const path = require(`path`)
const { slash } = require(`gatsby-core-utils`)
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin')

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      plugins: [new TsconfigPathsPlugin()],
    },
  })
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const {
    data: {
      allWpProduct: { nodes: allProducts },
      allWpLandingPage: { nodes: allLandingPages },
    },
  } = await graphql(`
    {
      allWpProduct {
        nodes {
          slug
          id
          databaseId
        }
      }
      allWpLandingPage {
        nodes {
          slug
          id
          databaseId
        }
      }
    }
  `)

  allProducts.forEach(post => {
    if (post.databaseId !== 11) {
      createPage({
        path: `/shop/${post.slug}`,
        component: slash(path.resolve(`./src/templates/ProductPage.tsx`)),
        context: {
          id: post.id,
        },
      })
    }
  })

  allLandingPages.forEach(post => {
    createPage({
      path: `/${post.slug}`,
      component: slash(path.resolve(`./src/templates/LandingPage.tsx`)),
      context: {
        id: post.id,
      },
    })
  })
}
